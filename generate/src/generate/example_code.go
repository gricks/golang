package generate

// NOTE: THIS FILE WAS PRODUCED BY
// CODE GENERATION TOOL
// DO NOT EDIT

func (self *Message) Info() string {
	var info string
	info += "name:ID, tag:id, type:int32"
	info += "name:Msg, tag:msg, type:string"
	return info
}

func (self *Protocol) Info() string {
	var info string
	info += "name:Desc, tag:desc, type:string"
	info += "name:ID, tag:id, type:int32"
	return info
}
