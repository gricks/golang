package main

import (
	"errors"
	"fmt"

	"github.com/cenkalti/backoff/v3"
)

func main() {
	cnt := 0
	success := 3
	operation := func() error {
		cnt++
		if cnt >= success {
			return nil
		}
		return errors.New("aha")
	}

	err := backoff.Retry(operation, backoff.NewExponentialBackOff())
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("success")
	}
}
