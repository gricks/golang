package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

// Register System Signal
func InitSignal() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGHUP, syscall.SIGUSR1, syscall.SIGUSR2)
	for {
		s := <-c
		switch s {
		case syscall.SIGHUP:
			fmt.Println("Recv SIGHUP")
		case syscall.SIGUSR1:
			fmt.Println("Recv SIGUSR1")
		case syscall.SIGUSR2:
			fmt.Println("Recv SIGUSR2")
		}
	}
}

func main() {
	InitSignal()
}
