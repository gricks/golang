package main

import "fmt"

func GetMap(m map[string]interface{}, k string) interface{} {
	return m[k]
}

func SetMap(m map[string]interface{}, k string, v interface{}) {
	m[k] = v
}

func DelMap(m map[string]interface{}, k string) {
	delete(m, k)
}

func AppendSlice(s []int, i int) []int {
	s = append(s, i)
	return s
}

func main() {
	// map is a reference type
	m := make(map[string]interface{})
	SetMap(m, "1", 1)
	SetMap(m, "2", 2)
	fmt.Println(m)
	fmt.Println(GetMap(m, "1"))
	fmt.Println(GetMap(m, "2"))
	DelMap(m, "1")
	DelMap(m, "1")
	fmt.Println(m)
	fmt.Println(GetMap(m, "1"))
	fmt.Println(GetMap(m, "2"))

	// slice is a reference type of array
	// but, not a reference of vector/slice when change length by append
	s := make([]int, 0, 2)
	fmt.Println(AppendSlice(s, 1))
	fmt.Println(s)
}
