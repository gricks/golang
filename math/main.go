package main

import (
	"fmt"
	"math"
)

func main() {
	var x float64
	x = 1.333
	fmt.Println("value:", x, "ceil:", math.Ceil(x), "floor:", math.Floor(x), "rounding:", math.Floor(x+0.5))
	x = 1.666
	fmt.Println("value:", x, "ceil:", math.Ceil(x), "floor:", math.Floor(x), "rounding:", math.Floor(x+0.5))

	for i := 0; i < math.MaxInt32; i++ {
		if i/32 != i>>5 {
			panic("Unexpected")
		}
		if i%32 != i&31 {
			panic("Unexpected")
		}
	}
}
