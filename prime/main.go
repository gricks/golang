package main

import "fmt"

func generate(ch chan<- int) {
	for i := 2; i < 100; i++ {
		ch <- i
	}
	close(ch)
}

func filter(src chan int, dst chan int, p int) {
	for i := range src {
		if i%p != 0 {
			dst <- i
		}
	}
	close(dst)
}

func prime(r uint64) uint64 {
	max := uint64(0)
	status := make([]bool, r)
	for i := uint64(2); i < r; i++ {
		if !status[i] {
			max = i
			for j := i; j < r; j += i {
				status[j] = true
			}
		}
	}
	return max
}

func main() {
	src := make(chan int)
	go generate(src)
	for {
		p, ok := <-src
		if !ok {
			break
		}
		fmt.Println("Prime:", p)
		dst := make(chan int)
		go filter(src, dst, p)
		src = dst
	}

	fmt.Println("Prime:", prime(100000))
}
