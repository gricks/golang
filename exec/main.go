package main

import (
	"bytes"
	"flag"
	"fmt"
	"os/exec"
	"strings"
)

func init() {
	flag.Parse()
}

func main() {
	if len(flag.Args()) == 0 {
		a := []string{
			"p1",
			"p2",
			"p3=0",
		}
		var b bytes.Buffer
		ex := exec.Command("bash", "-c", "./exec "+strings.Join(a, " "))
		ex.Stdout = &b
		ex.Stderr = &b
		err := ex.Run()
		if err != nil {
			fmt.Println(string(b.Bytes()))
			panic(err)
		}
		fmt.Println(string(b.Bytes()))
	} else {
		for _, v := range flag.Args() {
			fmt.Printf("%s ", v)
		}
		fmt.Println()
	}
}
