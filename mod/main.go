package main

import (
	"fmt"
	"math/rand"
)

func main() {
	// power 2 quick mod
	{
		total := 5000000
		sets := make([]int, 256)
		for i := 0; i < 256; i++ {
			sets[i] = -(total / 256)
		}
		for i := 0; i < 5000000; i++ {
			sets[rand.Uint32()&(256-1)]++
		}
		fmt.Println(sets)
	}

	// https://lemire.me/blog/2016/06/27/a-fast-alternative-to-the-modulo-reduction/
	{
		total := 5000000
		sets := make([]int, 256)
		for i := 0; i < 256; i++ {
			sets[i] = -(total / 256)
		}
		for i := 0; i < 5000000; i++ {
			p := int((uint64(rand.Uint32()) * 256) >> 32)
			sets[p]++
		}
		fmt.Println(sets)
	}
}
