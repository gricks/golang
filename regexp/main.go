package main

import (
	"fmt"
	"regexp"
)

var exp1 = regexp.MustCompile(`AND \((.*)\)`)  // 贪婪
var exp2 = regexp.MustCompile(`AND \((.*?)\)`) // 非贪婪

func main() {
	str := "  AND (VIPLevel > 2000 AND VIPLevel <= 50000) AND (CAST(GameSvrId AS bigint) = 33209) AND (PlatID > 2) AND (CAST(vGameAppid AS bigint) = 1008) AND"
	/*
		{
			val := exp1.FindAllStringSubmatch(str, -1)
			for i := 0; i < len(val); i++ {
				fmt.Println(val[i][1])
			}
		}
	*/
	{
		val := exp2.FindAllStringSubmatch(str, -1)
		for i := 0; i < len(val); i++ {
			fmt.Println(val[i][1])
		}
	}
}
