package main

import (
	"testing"

	"github.com/google/cel-go/cel"
	"github.com/google/cel-go/checker/decls"
)

func BenchmarkCel2(b *testing.B) {
	object := map[string]interface{}{
		"object": map[string]interface{}{
			"values": []map[string]interface{}{
				{"age": 1, "score": 1},
				{"age": 2, "score": 2},
				{"age": 3, "score": 3},
				{"age": 4, "score": 4},
				{"age": 5, "score": 5},
			},
		},
	}

	env, err := cel.NewEnv(cel.Declarations(decls.NewVar("object", decls.NewMapType(decls.String, decls.Dyn))))
	if err != nil {
		panic(err)
	}

	ast, iss := env.Compile(`object.values.filter(x, x.age > 1)`)
	if iss.Err() != nil {
		panic(iss.Err())
	}

	prg, err := env.Program(ast)
	if err != nil {
		panic(err)
	}

	b.Run("map", func(tb *testing.B) {
		for i := 0; i < tb.N; i++ {
			_, _, _ = prg.Eval(object)
		}
	})
}
