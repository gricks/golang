package main

import (
	"fmt"
	"reflect"

	"github.com/google/cel-go/cel"
	"github.com/google/cel-go/checker/decls"
	"github.com/google/cel-go/common/types/ref"
)

func eval(expr string, value map[string]interface{}) {
	env, err := cel.NewEnv(cel.Declarations(decls.NewVar("object", decls.NewMapType(decls.String, decls.Dyn))))
	if err != nil {
		panic(err)
	}

	ast, iss := env.Compile(expr)
	if iss.Err() != nil {
		panic(iss.Err())
	}

	prg, err := env.Program(ast)
	if err != nil {
		panic(err)
	}

	v, _, err := prg.Eval(map[string]interface{}{"object": value})
	if err != nil {
		panic(err)
	}

	fmt.Println("----------------")
	fmt.Println("value.type:", v.Type())
	fmt.Println("value.reftype:", reflect.TypeOf(v.Value()))
	if typ := reflect.TypeOf(v.Value()).Elem(); typ.Kind() == reflect.Interface {
		if reflect.ValueOf(v.Value()).Len() != 0 {
			fmt.Println("value.[]reftype:", reflect.ValueOf(v.Value()).Index(0).Elem().Type())
		}
		if reflect.ValueOf(v.Value()).Len() == 1 {
			fmt.Println("value.[]reftype2:", reflect.ValueOf(v.Value()).Index(0).Interface().(ref.Val).Value())
		}
	}
	fmt.Println("value.value:", v.Value())
}

func main() {
	object := map[string]interface{}{
		"values": []map[string]interface{}{
			{"age": 1, "score": 1},
			{"age": 2, "score": 2},
			{"age": 3, "score": 3},
			{"age": 4, "score": 4},
			{"age": 5, "score": 5},
		},
	}

	eval(`object.values.filter(x, x.age > 1)`, object)
	eval(`object.values.filter(x, x.age > 2)`, object)
	eval(`object.values.filter(x, x.age > 3)`, object)
	eval(`object.values.filter(x, x.age > 4)`, object)
	eval(`object.values.filter(x, x.age > 5)`, object)
}
