package main

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/google/cel-go/common/types"
	"github.com/google/cel-go/interpreter"
	jsoniter "github.com/json-iterator/go"
)

type mapActivation struct {
	bindings map[string]interface{}
}

// Parent implements the Activation interface method.
func (a *mapActivation) Parent() interpreter.Activation {
	return nil
}

// ResolveName implements the Activation interface method.
func (a *mapActivation) ResolveName(name string) (interface{}, bool) {
	obj, found := a.bindings[name]
	if !found {
		return nil, false
	}
	return obj, found
}

func BenchmarkCel1(b *testing.B) {
	object1 := &mapActivation{
		bindings: map[string]interface{}{
			"object": map[string]interface{}{
				"id":    1,
				"stime": 1060,
				"etime": 2060,
				"vtype": "int",
			},
		},
	}

	object2 := &mapActivation{
		bindings: map[string]interface{}{
			"object": map[string]interface{}{
				"id":    types.Int(1),
				"stime": types.Int(1060),
				"etime": types.Int(2060),
				"vtype": types.String("int"),
			},
		},
	}

	object3 := &mapActivation{
		bindings: map[string]interface{}{
			"object": types.NewStringInterfaceMap(types.DefaultTypeAdapter, map[string]interface{}{
				"id":    1,
				"stime": 1060,
				"etime": 2060,
				"vtype": "int",
			}),
		},
	}

	object4 := &mapActivation{
		bindings: map[string]interface{}{
			"object": types.NewStringInterfaceMap(types.DefaultTypeAdapter, map[string]interface{}{
				"id":    types.Int(1),
				"stime": types.Int(1060),
				"etime": types.Int(2060),
				"vtype": types.String("int"),
			}),
		},
	}

	objects := []*mapActivation{}
	objects = append(objects, object1, object2, object3, object4)
	//objects = []*mapActivation{object2}

	//prg := cel_compile(`object.stime > 1 && object.etime != 0`)
	//prg := cel_compile(`object.stime > 1 && object.etime != 0 && object.vtype == "int"`)
	prg := cel_compile(`object.vtype == "int" && object.etime > 2050 && object.stime > 1050`)
	v, _, err := prg.Eval(objects[0])
	if err != nil {
		panic(err)
	} else if !v.Value().(bool) {
		panic("invalid result")
	}

	for i, object := range objects {
		b.Run(fmt.Sprintf("act-%d", i), func(tb *testing.B) {
			for i := 0; i < tb.N; i++ {
				_, _, _ = prg.Eval(object)
			}
		})
	}

	for i, object := range objects {
		b.Run(fmt.Sprintf("map-%d", i), func(tb *testing.B) {
			for i := 0; i < tb.N; i++ {
				_, _, _ = prg.Eval(object.bindings)
			}
		})
	}
}

func BenchmarkCel2(b *testing.B) {
	object1 := &mapActivation{
		bindings: map[string]interface{}{
			"object": []interface{}{
				1,
				1060,
				2060,
				"int",
			},
		},
	}

	object2 := &mapActivation{
		bindings: map[string]interface{}{
			"object": []interface{}{
				types.Int(1),
				types.Int(1060),
				types.Int(2060),
				types.String("int"),
			},
		},
	}

	objects := []*mapActivation{}
	objects = append(objects, object1, object2)

	//prg := cel_compile(`object.stime > 1 && object.etime != 0`)
	//prg := cel_compile(`object.stime > 1 && object.etime != 0 && object.vtype == "int"`)
	prg := cel_compile(`object[3] == "int" && object[2] > 2050 && object[1] > 1050`, true)
	v, _, err := prg.Eval(objects[0])
	if err != nil {
		panic(err)
	} else if !v.Value().(bool) {
		panic("invalid result")
	}

	for i, object := range objects {
		b.Run(fmt.Sprintf("act-%d", i), func(tb *testing.B) {
			for i := 0; i < tb.N; i++ {
				_, _, _ = prg.Eval(object)
			}
		})
	}
}

func BenchmarkCel3(b *testing.B) {
	object := datas()
	prg := cel_compile(`object.values.filter(x, x.vtype == "int" && x.etime > 2050 && x.stime > 1050)`)
	_, _, err := prg.Eval(object)
	if err != nil {
		panic(err)
	}

	object2 := datas2()

	b.Run("map", func(tb *testing.B) {
		for i := 0; i < tb.N; i++ {
			_, _, _ = prg.Eval(object)
		}
	})

	b.Run("map2", func(tb *testing.B) {
		for i := 0; i < tb.N; i++ {
			_, _, _ = prg.Eval(object2)
		}
	})

	b.Run("json", func(tb *testing.B) {
		for i := 0; i < tb.N; i++ {
			_, _ = json.Marshal(object)
		}
	})

	b.Run("jsoniter", func(tb *testing.B) {
		for i := 0; i < tb.N; i++ {
			_, _ = jsoniter.Marshal(object)
		}
	})
}
