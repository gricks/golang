module golang/cel/benchmark

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/google/cel-go v0.9.0
	github.com/json-iterator/go v1.1.12
)
