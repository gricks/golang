package main

import (
	"fmt"

	"github.com/google/cel-go/cel"
	"github.com/google/cel-go/checker/decls"
	"github.com/google/cel-go/common/types"
	"github.com/google/cel-go/common/types/ref"

	"github.com/davecgh/go-spew/spew"
)

func cel_compile(expr string, opt ...bool) cel.Program {
	env, err := cel.NewEnv(cel.Declarations(
		decls.NewVar("object",
			decls.NewMapType(decls.String, decls.Dyn))))
	if err != nil {
		panic(err)
	}

	if len(opt) > 0 {
		env, err = cel.NewEnv(cel.Declarations(
			decls.NewVar("object",
				decls.NewListType(decls.Dyn))))
		if err != nil {
			panic(err)
		}
	}

	ast, iss := env.Compile(expr)
	// Check iss for compilation errors.
	if iss.Err() != nil {
		panic(iss.Err())
	}

	prg, err := env.Program(ast)
	//prg, err := env.Program(ast, cel.EvalOptions(cel.OptOptimize))
	if err != nil {
		panic(err)
	}

	return prg
}

func datas() interface{} {
	datas := []map[string]interface{}{}
	for i := 1; i <= 100; i++ {
		datas = append(datas, map[string]interface{}{
			"id":    i,
			"stime": 1000 + i,
			"etime": 2000 + i,
			"vtype": func(v int) string {
				if v%2 == 0 {
					return "int"
				} else {
					return "str"
				}
			}(i),
		})
	}
	return map[string]interface{}{"object": map[string]interface{}{"values": datas}}
}

func datas2() interface{} {
	datas := []ref.Val{}
	for i := 1; i <= 100; i++ {
		datas = append(datas, types.NewStringInterfaceMap(types.DefaultTypeAdapter, map[string]interface{}{
			"id":    i,
			"stime": 1000 + i,
			"etime": 2000 + i,
			"vtype": func(v int) string {
				if v%2 == 0 {
					return "int"
				} else {
					return "str"
				}
			}(i),
		}))
	}
	return map[string]interface{}{"object": map[string]interface{}{"values": datas}}
}

func main() {
	v1 := map[string]interface{}{
		"object": map[string]interface{}{
			"id":    1,
			"stime": 1060,
			"etime": 2060,
			"vtype": "int",
		},
	}

	v2 := map[string]interface{}{
		"object": map[string]interface{}{
			"id":    types.Int(1),
			"stime": types.Int(1060),
			"etime": types.Int(2060),
			"vtype": types.String("int"),
		},
	}

	fmt.Println(spew.Sdump(v1))
	fmt.Println(spew.Sdump(v2))

	MapIfaceToValue(v1["object"])
	fmt.Println(spew.Sdump(v1))
}

func MapIfaceToValue(m interface{}) interface{} {
	vm := m.(map[string]interface{})
	for k, v := range vm {
		vm[k] = types.DefaultTypeAdapter.NativeToValue(v)
	}
	return vm
}
