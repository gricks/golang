package main

import (
	"encoding/json"
	"fmt"
	"os"
	"reflect"

	"gopkg.in/yaml.v3"

	"github.com/google/cel-go/cel"
	"github.com/google/cel-go/checker/decls"
)

func pretty(v any) string {
	b, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		panic(err)
	}
	return string(b)
}

func eval(expr string, object any) {
	env, err := cel.NewEnv(cel.Declarations(
		decls.NewVar("object",
			decls.NewMapType(
				decls.String, decls.Dyn))))
	if err != nil {
		panic(err)
	}

	ast, iss := env.Compile(expr)
	// Check iss for compilation errors.
	if iss.Err() != nil {
		panic(iss.Err())
	}

	prg, err := env.Program(ast)
	if err != nil {
		panic(err)
	}

	fmt.Println("exprssion:", expr)
	fmt.Println("object:", pretty(object))
	val, _, err := prg.Eval(map[string]any{"object": object})
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println()
	rtype := reflect.TypeOf(map[string]any{})
	v, err := val.ConvertToNative(rtype)
	if err != nil {
		panic(err)
	}
	fmt.Println("return:", pretty(v))
}

type Input struct {
	Expr   string
	Object any
}

func main() {
	b, err := os.ReadFile("input.yaml")
	if err != nil {
		panic(err)
	}

	var input Input
	err = yaml.Unmarshal(b, &input)
	if err != nil {
		panic(err)
	}

	eval(input.Expr, input.Object)
}
