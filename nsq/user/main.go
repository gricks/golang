package main

import (
	"flag"
	"fmt"
	"math/rand"
	"net"
	"time"
)

var (
	host = flag.String("host", "", "gate host")
)

func init() {
	flag.Parse()
}

func main() {
	rand.Seed(time.Now().Unix())

	conn, err := net.Dial("tcp", *host)
	if err != nil {
		panic(err)
	}

	_, err = conn.Write([]byte("Astone" + fmt.Sprintf("%d", rand.Intn(9999))))
	if err != nil {
		panic(err)
	}

	buffer := make([]byte, 1024)
	for {
		n, err := conn.Read(buffer[:])
		if err != nil {
			panic(err)
		}
		fmt.Println(string(buffer[:n]))
	}
}
