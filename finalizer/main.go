package main

import (
	"fmt"
	"runtime"
	"strconv"
	"sync/atomic"
	"time"
)

type Publisher struct {
	Name string
}

func NewPublisher(i int) *Publisher {
	publisher := &Publisher{
		Name: strconv.Itoa(i),
	}
	// keep alive void gc
	//publisher.keepalive()
	return publisher
}

func (p *Publisher) Send() {
	fmt.Println("Send", p.Name, "begin")
	defer fmt.Println("Send", p.Name, "end")

	time.Sleep(time.Second)
}

func (p *Publisher) keepalive() {
	go func() {
		time.Sleep(time.Hour)
		fmt.Println(p)
	}()
}

func (p *Publisher) Close() {
	fmt.Println("Close", p.Name)
}

var mgr atomic.Pointer[map[int]*Publisher]

func Reload(n int) {
	publishers := map[int]*Publisher{}
	for i := 0; i < 10; i++ {
		publisher := NewPublisher(n*100000 + i)
		runtime.SetFinalizer(publisher,
			func(v any) {
				publisher, ok := v.(*Publisher)
				if ok {
					publisher.Close()
				}
			})
		publishers[i] = publisher
	}
	mgr.Store(&publishers)
}

func main() {
	Reload(0)

	go func() {
		tick := time.NewTicker(3 * time.Second)
		defer tick.Stop()
		i := 1
		for range tick.C {
			i++
			Reload(i)
		}
	}()

	// trigger GC
	go func() {
		for {
			runtime.GC()
			time.Sleep(time.Second)
		}
	}()

	for i := 0; i < 10; i++ {
		v := i
		go func() {
			for {
				(*mgr.Load())[v].Send()
			}
		}()
	}

	select {}
}
