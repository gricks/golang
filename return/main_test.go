package main

import "testing"

//go:oninline
func fn1(p *int) {
	for i := 0; i < 10; i++ {
		_fn1(p)
	}
}

//go:oninline
func _fn1(i *int) (data interface{}, code int) {
	if i != nil {
		data = i
	}
	code = 1
	return
}

//go:oninline
func fn2(p *int) {
	for i := 0; i < 10; i++ {
		_fn2(p)
	}
}

//go:oninline
func _fn2(i *int) (interface{}, int) {
	return i, 1
}

func BenchmarkReturn(b *testing.B) {
	p := 0
	b.Run("fn1-nil", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			fn1(nil)
		}
	})

	b.Run("fn1-point", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			fn1(&p)
		}
	})

	b.Run("fn2-nil", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			fn2(nil)
		}
	})

	b.Run("fn2-point", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			fn2(&p)
		}
	})
}
