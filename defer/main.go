package main

import "fmt"

type Int int

func (i Int) Value() string {
	return fmt.Sprint(i)
}

func main() {
	{
		fn := func() (err error) {
			defer func() {
				if rerr := recover(); rerr != nil {
					err = fmt.Errorf("%v", rerr)
				}
			}()
			panic("wow")
			return
		}
		fmt.Println(fn())
	}

	{
		fn := func() {
			succ := true
			defer func() {
				fmt.Println(succ)
			}()
			succ = false
		}
		fn()
	}

	for i := 0; i < 3; i++ {
		v := Int(i)
		defer fmt.Println(v.Value())
	}
}
