package main

import "testing"

type Object struct {
	i int
}

//go:noinline
func (o *Object) Incr() {
	o.i++
}

func RawDefer(os []*Object) {
	defer func() {
		for _, o := range os {
			o.Incr()
		}
	}()
	os[0].Incr()
}

func ForDefer(os []*Object) {
	for _, o := range os {
		defer o.Incr()
	}
	os[0].Incr()
}

func BenchmarkDefer(b *testing.B) {
	os := make([]*Object, 10)
	for i := range os {
		os[i] = new(Object)
	}

	b.Run("RawDefer", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			RawDefer(os)
		}
	})

	b.Run("ForDefer", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			ForDefer(os)
		}
	})
}
