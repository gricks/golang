package main

import "fmt"

func div(a int, b int) int {
	if b == 0 {
		panic("division by 0")
	} else {
		return a / b
	}
	return 0 // unreachable code
}

func main() {
	str := "vet"
	fmt.Printf("%d\n", str) // arg str for printf verb %d of wrong type: string
	fmt.Printf("%d\n")      // missing argument for Printf("%d"): format reads arg 1, have only 0 args

	i := 10
	fmt.Println(i != 0 || i != 1) // suspect or: i != 0 || i != 1
	fmt.Println(i == 0 && i == 1) // suspect and: i == 0 && i == 1
	fmt.Println(i == 0 && i == 0) // redundant and: i == 0 && i == 0

	words := []string{"alice", "bob"}
	for _, word := range words {
		go func() {
			fmt.Println(word) // range variable word captured by func literal
		}()
	}

	i = i                   // self-assignment of i to i
	fmt.Println(div == nil) // comparison of function div == nil is always false
	fmt.Println(i >> 64)    // i (64 bits) too small for shift of 64
}
