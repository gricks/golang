package main

import (
	"context"
	"fmt"
	"log"
	"time"

	badger "github.com/dgraph-io/badger/v4"
	"github.com/dgraph-io/badger/v4/pb"
)

// https://dgraph.io/docs/badger/get-started/
func main() {
	// Open the Badger database located in the ./data directory.
	// It will be created if it doesn't exist.
	db, err := badger.Open(badger.DefaultOptions("./data"))
	if err != nil {
		log.Fatal(err)

	}
	defer db.Close()

	// Stream
	go func() {
		stream := db.NewStream()

		stream.NumGo = 2
		stream.LogPrefix = "stream"
		stream.KeyToList = func(key []byte, itr *badger.Iterator) (*pb.KVList, error) {
			kv, err := stream.ToList(key, itr)
			if err != nil {
				return nil, err
			}
			for _, kv := range kv.Kv {
				log.Printf("stream kv: %s,%s\n",
					kv.Key, kv.Value)
			}
			return nil, nil
		}

		if err := stream.Orchestrate(context.Background()); err != nil {
			log.Println("Done streaming", err)
		} else {
			log.Println("Done streaming")
		}
	}()

	// View
	db.View(func(txn *badger.Txn) error {
		it, err := txn.Get([]byte("answer"))
		if err != nil {
			log.Printf("txn.Get with error: %s", err)
			return err
		}
		return it.Value(func(val []byte) error {
			log.Printf("The key info: %s, val:%s\n", it.String(), val)
			return nil
		})
	})

	// Update
	db.Update(func(txn *badger.Txn) error {
		entry := badger.NewEntry([]byte("answer"), []byte("42"))
		return txn.SetEntry(entry)
	})

	// Merge
	key := []byte("merge")
	db.View(func(txn *badger.Txn) error {
		it, err := txn.Get(key)
		if err != nil {
			log.Printf("txn.Get with error: %s", err)
			return err
		}
		return it.Value(func(val []byte) error {
			log.Printf("The key info: %s, val:%s\n", it.String(), val)
			return nil
		})
	})

	add := func(src, dst []byte) []byte {
		fmt.Println("exec add")
		return append(src, dst...)
	}

	m := db.GetMergeOperator(key, add, time.Second)
	m.Add([]byte("A"))

	db.View(func(txn *badger.Txn) error {
		it, err := txn.Get(key)
		if err != nil {
			log.Printf("txn.Get with error: %s", err)
			return err
		}
		return it.Value(func(val []byte) error {
			log.Printf("The key info: %s, val:%s\n", it.String(), val)
			return nil
		})
	})

	m.Add([]byte("B"))
	m.Add([]byte("C"))

	m.Stop()

	value, err := m.Get()
	if err != nil {
		panic(err)
	}

	log.Printf("kv: %s,%s\n", key, value)

	db.RunValueLogGC(0.7)

	time.Sleep(3 * time.Second)
}
