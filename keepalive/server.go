package main

import (
	"fmt"
	"net/http"

	"github.com/spf13/cobra"
)

var addr = "127.0.0.1:23333"

var serverCmd = &cobra.Command{
	Use: "server",
	Run: runServer,
}

func init() {
	rootCmd.AddCommand(serverCmd)
}

func runServer(cmd *cobra.Command, args []string) {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("hello"))
		fmt.Println("request complete")
	})
	http.ListenAndServe(addr, nil)
}
