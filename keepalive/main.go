package main

import (
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use: "keepalive",
}

func main() {
	rootCmd.Execute()
}
