package main

import (
	"fmt"
	"runtime"
	"time"
)

func New(fns ...func()) {
	var garbage struct{ refer *int }
	garbage.refer = new(int)
	var gc interface{}
	gc = func(obj interface{}) {
		for _, fn := range fns {
			fn()
		}
		runtime.SetFinalizer(
			obj, gc)
	}
	runtime.SetFinalizer(&garbage, gc)
}

func main() {
	New(func() { fmt.Println("GC Happend") })

	for {
		runtime.GC()
		time.Sleep(time.Second)
	}
}
