package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
)

func Eval(src string) ([]byte, error) {
	cmd, err := EvalCommand(src)
	if err != nil {
		return nil, err
	}
	return cmd.CombinedOutput()
}

func EvalCommand(src string) (*exec.Cmd, error) {
	dir, err := ioutil.TempDir("", "eval-*")
	if err != nil {
		return nil, err
	}
	defer os.Remove(dir)
	err = ioutil.WriteFile(filepath.Join(dir, "main.go"), []byte(src), 0644)
	if err != nil {
		return nil, err
	}
	script := "cd " + dir + "\n"
	script += "go mod init eval > /dev/null 2>&1\n"
	script += "go mod tidy > /dev/null 2>&1\n"
	script += "go run main.go\n"
	return exec.Command("/bin/bash", "-c", script), nil
}

func f() {
}

func main() {
	src := `
package main

import (
  "os"
  "fmt"

  "golang.org/x/sync/errgroup"
)

func main() {
	eg := new(errgroup.Group)
	eg.Go(func() error {
		fmt.Println("eval run")
		return nil
	})
	eg.Wait()
	fmt.Fprintf(os.Stderr, "stderr")
}
`
	str, err := Eval(src)
	fmt.Println(string(str))
	fmt.Println(err)
}
