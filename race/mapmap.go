package main

import "sync"

type Chatroom struct {
	Rooms sync.Map // Rid  ID
}

func (this *Chatroom) Join(Rids []uint64, ID uint64) {
	for _, Rid := range Rids {
		r, _ := this.Rooms.LoadOrStore(Rid, &sync.Map{})
		rmap := r.(*sync.Map)
		(*rmap).Store(ID, true)
	}
}

func (this *Chatroom) Quit(Rids []uint64, ID uint64) {
	for _, Rid := range Rids {
		if r, ok := this.Rooms.Load(Rid); ok {
			rmap := r.(*sync.Map)
			(*rmap).Delete(ID)
		}
	}
}

func (this *Chatroom) Range(Rid uint64, call func(ID uint64)) {
	if r, ok := this.Rooms.Load(Rid); ok {
		rmap := r.(*sync.Map)
		(*rmap).Range(func(k, v interface{}) bool {
			kv := k.(uint64)
			call(kv)
			return true
		})
	}
}
