package main

import (
	"log"
	"os"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	"github.com/davecgh/go-spew/spew"
)

type User struct {
	Id   int    `gorm:"primaryKey"`
	Name string `gorm:"type:varchar(64)"`

	// belongs to
	CompanyId int
	Company   *Company `gorm:"foreignKey:CompanyId"`

	// has one
	Card struct {
		Id     int    `gorm:"primaryKey"`
		Name   string `gorm:"type:varchar(64)"`
		UserId int
	} `gorm:"foreignKey:UserId"`

	// has many
	CreditCards []*CreditCard

	// many2many
	Languages []*Language `gorm:"many2many:user_language;"`
}

type Company struct {
	Id   int    `gorm:"primaryKey"`
	Name string `gorm:"type:varchar(64)"`
}

type Card struct {
	Id     int    `gorm:"primaryKey"`
	Name   string `gorm:"type:varchar(64)"`
	UserId int
}

type CreditCard struct {
	Id     int    `gorm:"primaryKey"`
	UserId int    `gorm:"index"`
	Number string `gorm:"type:varchar(64)"`
}

type Language struct {
	Id   int    `gorm:"primaryKey"`
	Name string `gorm:"type:varchar(64)"`
}

var (
	usr1 = &User{
		Name: "a",
		Company: &Company{
			Name: "a_com",
		},
		CreditCards: []*CreditCard{
			{Number: "1_a"},
			{Number: "1_b"},
			{Number: "1_c"},
		},
	}

	usr2 = &User{
		Name: "b",
		Company: &Company{
			Name: "b_com",
		},
		CreditCards: []*CreditCard{
			{Number: "2_a"},
			{Number: "2_b"},
			{Number: "2_c"},
		},
	}

	card1 = &Card{
		Name: "card1",
	}

	card2 = &Card{
		Name: "card2",
	}

	language1 = &Language{
		Name: "zh",
	}

	language2 = &Language{
		Name: "en",
	}

	language3 = &Language{
		Name: "fr",
	}
)

var db *gorm.DB

func main() {
	var err error
	db, err = gorm.Open(
		mysql.Open("root:nikki@(127.0.0.1:3306)/test_association?timeout=5s&parseTime=true&loc=Local&charset=utf8"), &gorm.Config{
			Logger: logger.New(
				log.New(os.Stdout, "\r\n", log.LstdFlags), logger.Config{
					SlowThreshold:             200 * time.Millisecond,
					LogLevel:                  logger.Info,
					IgnoreRecordNotFoundError: false,
					Colorful:                  true,
				}),
			DisableForeignKeyConstraintWhenMigrating: true,
		})
	if err != nil {
		panic(err)
	}

	Migrate()

	defer Cleanup()

	err = db.Table("languages").CreateInBatches([]*Language{
		language1,
		language2,
		language3,
	}, 3).Error
	if err != nil {
		panic(err)
	}

	usr1.Languages = []*Language{
		&Language{Id: language1.Id},
		&Language{Id: language2.Id},
	}
	err = db.Omit("Languages.*").Create(usr1).Error
	if err != nil {
		panic(err)
	}

	usr2.Languages = []*Language{
		&Language{Id: language2.Id},
		&Language{Id: language3.Id},
	}
	err = db.Omit("Languages.*").Create(usr2).Error
	if err != nil {
		panic(err)
	}

	card1.UserId = usr1.Id
	err = db.Create(card1).Error
	if err != nil {
		panic(err)
	}

	card2.UserId = usr2.Id
	err = db.Create(card2).Error
	if err != nil {
		panic(err)
	}

	usrs := []*User{}
	if err := db.
		Preload("Company").
		Preload("Card", TableName("cards")).
		//Joins("Card", db.Table("cards")).
		Preload("CreditCards").
		Preload("Languages").
		Find(&usrs).Error; err != nil {
		panic(err)
	}

	spew.Dump(usrs)

	db.Model(usr1).Omit("Languages.*").
		Association("Languages").
		Replace([]*Language{
			&Language{Id: language1.Id},
			&Language{Id: language3.Id},
		})

	if err := db.
		Preload("Company").
		Preload("Card", TableName("cards")).
		Preload("CreditCards").
		Preload("Languages").
		Find(&usrs).Error; err != nil {
		panic(err)
	}

	spew.Dump(usrs)
}

func TableName(name string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		return db.Table(name).Select("id", "user_id")
	}
}

func Migrate() {
	err := db.AutoMigrate(
		new(User),
		new(Company),
		new(Card),
		new(CreditCard),
	)
	if err != nil {
		panic(err)
	}
}

func Cleanup() {
	var err error
	err = db.Where("1=1").Delete(new(Language)).Error
	if err != nil {
		panic(err)
	}
	err = db.Where("1=1").Delete(new(User)).Error
	if err != nil {
		panic(err)
	}
	err = db.Where("1=1").Delete(new(Company)).Error
	if err != nil {
		panic(err)
	}
	err = db.Where("1=1").Delete(new(Card)).Error
	if err != nil {
		panic(err)
	}
	err = db.Where("1=1").Delete(new(CreditCard)).Error
	if err != nil {
		panic(err)
	}
}
