package main

import (
	"errors"
	"fmt"
	_ "runtime/debug"
)

var UserError = errors.New("User Error")

type PathError struct {
	Err  error
	Path string
}

func (self *PathError) Error() string {
	return self.Path + ": " + self.Err.Error()
}

func PanicCall() {
	panic("call panic")
}

func InvalidCall() {
	a := []int{}
	a[10] = 100
}

func ProtectedCall(call func()) {
	defer func() {
		// recover must write in defer func
		if err := recover(); err != nil {
			//debug.PrintStack()
			fmt.Println("what, i didn't want die!")
		}
	}()
	call()
}

func main() {
	ProtectedCall(PanicCall)
	ProtectedCall(InvalidCall)
	fmt.Println("never coredump!")

	PanicCall()
	fmt.Println("rip")
}
