package main

import (
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func main() {
	{
		logger, err := zap.NewProduction()
		if err != nil {
			panic(err)
		}

		sugar := logger.Sugar()
		sugar.Infow("asldkfjasldfj", "url", "go.uber.org", "attempt", 3, "backoff", time.Second, sugar)
		sugar.Infow("asldkfjasldfj", "url", "go.uber.org", "attempt", 3, "backoff", time.Second, "cc", sugar)
	}

	{
		cfg := zap.NewProductionConfig()
		cfg.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
		cfg.OutputPaths = []string{"log"}
		logger, err := cfg.Build()
		if err != nil {
			panic(err)
		}

		sugar := logger.Sugar()
		sugar.Infow("asldkfjasldfj", "url", "go.uber.org", "attempt", 3, "backoff", time.Second, sugar)
		sugar.Infow("asldkfjasldfj", "url", "go.uber.org", "attempt", 3, "backoff", time.Second, "cc", sugar)
	}
}
