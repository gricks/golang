package main

import (
	"fmt"
	"strings"

	"golang.org/x/text/language"
)

// https://www.w3.org/International/articles/language-tags/
func main() {
	tags := []language.Tag{
		language.English,
		language.AmericanEnglish,    // en-US
		language.SimplifiedChinese,  // zh-Hans
		language.MustParse("zh-CN"), // zh-CN
		language.MustParse("zh-TW"), // zh-TW
	}

	matcher := language.NewMatcher(tags)

	fmt.Println("language.English", language.English)
	fmt.Println("language.AmericanEnglish", language.AmericanEnglish)
	fmt.Println("language.Chinese", language.Chinese)
	fmt.Println("language.SimplifiedChinese", language.SimplifiedChinese)

	fmt.Println("-----------")

	langs := []string{
		"en",
		"en-US",
		"en-GB",
		"zh",
		"zh-CN",
		"zh-TW",
	}

	for _, lang := range langs {
		tag, index := language.MatchStrings(matcher, lang)
		fmt.Printf("lang:%s tag:%s index:%v\n", lang, tag, index)
	}

	// iso-3166 country-code
	codes := map[string]string{
		"EE": "EST",
		"ET": "ETH",
		"FK": "FLK",
		"FO": "FRO",
		"FJ": "FJI",
		"FI": "FIN",
		"cn": "CHN",
	}

	for code2, code3 := range codes {
		region2, err := language.ParseRegion(code2)
		if err != nil {
			panic(err)
		}
		if region2.String() != strings.ToUpper(code2) {
			panic(fmt.Sprint("err2:", region2, code2, code3))
		}
		region3, err := language.ParseRegion(code3)
		if err != nil {
			panic(err)
		}
		if region3.ISO3() != strings.ToUpper(code3) {
			panic(fmt.Sprint("err3:", region3, code2, code3))
		}
		fmt.Println("code2:", code2, "code3:", code3)
	}
}
