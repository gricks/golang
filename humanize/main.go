package main

import (
	"fmt"
	"strconv"

	"github.com/dustin/go-humanize"
)

func main() {
	fmt.Printf("%.0f\n", 12.3456)
	fmt.Printf("%s\n", strconv.FormatFloat(12.345, 'f', 0, 64))
	fmt.Printf("%.1f\n", 12.3456)
	fmt.Printf("%s\n", strconv.FormatFloat(12.345, 'f', 1, 64))
	fmt.Printf("That size file is %s.\n", humanize.Bytes(82854982))
	fmt.Printf("That size file is %s.\n", humanize.IBytes(82854982))
	fmt.Printf("You owe $%s.\n", humanize.Comma(6582491))
}
