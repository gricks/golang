package main

import "fmt"

func main() {
	str1 := "你好"
	str2 := "Привет"

	s1 := []rune(str1)
	s1[1] = '*'
	fmt.Println(string(s1))
	s2 := []rune(str2)

	for i := 1; i < len(s2); i++ {
		s2[i] = '*'
	}
	fmt.Println(string(s2))
}
