package main

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
)

var (
	host = flag.String("host", "", "host")
	file = flag.String("file", "crt.pem", "certificate file")
)

func init() {
	flag.Parse()
}

func formatCertificate(crt *x509.Certificate) string {
	b := new(bytes.Buffer)
	fmt.Fprintln(b, "Certificate")
	fmt.Fprintln(b, "Version:\t\t", crt.Version)
	fmt.Fprintln(b, "SerialNumber:\t\t", crt.SerialNumber)
	fmt.Fprintln(b, "Issuer:\t\t\t", crt.Issuer)
	fmt.Fprintln(b, "Subject:\t\t", crt.Subject)
	fmt.Fprintln(b, "NotBefore:\t\t", crt.NotBefore)
	fmt.Fprintln(b, "NotAfter:\t\t", crt.NotAfter)
	fmt.Fprintln(b, "IsCA:\t\t\t", crt.IsCA)
	fmt.Fprintln(b, "SignatureAlgorithm:\t", crt.SignatureAlgorithm)
	fmt.Fprintln(b, "Signature:\t\t", fmt.Sprintf("%x", crt.Signature))

	pubKey := crt.PublicKey
	bs, err := x509.MarshalPKIXPublicKey(pubKey)
	if err == nil {
		pubKey = base64.StdEncoding.EncodeToString(bs)
	}

	fmt.Fprintln(b, "PublicKeyAlgorithm:\t", crt.PublicKeyAlgorithm)
	fmt.Fprintln(b, "PublicKey:\t\t", pubKey)

	var keyUsageMap = map[x509.KeyUsage]string{
		x509.KeyUsageDigitalSignature:  "KeyUsageDigitalSignature",
		x509.KeyUsageContentCommitment: "KeyUsageContentCommitment",
		x509.KeyUsageKeyEncipherment:   "KeyUsageKeyEncipherment",
		x509.KeyUsageDataEncipherment:  "KeyUsageDataEncipherment",
		x509.KeyUsageKeyAgreement:      "KeyUsageKeyAgreement",
		x509.KeyUsageCertSign:          "KeyUsageCertSign",
		x509.KeyUsageCRLSign:           "KeyUsageCRLSign",
		x509.KeyUsageEncipherOnly:      "KeyUsageEncipherOnly",
		x509.KeyUsageDecipherOnly:      "KeyUsageDecipherOnly",
	}

	var keyUsage []string
	for k, v := range keyUsageMap {
		if k&crt.KeyUsage != 0 {
			keyUsage = append(keyUsage, v)
		}
	}

	fmt.Fprint(b, "KeyUsage:\t\t", "(", strings.Join(keyUsage, "|"), ")")
	return b.String()
}

func main() {
	if *host != "" {
		c, err := tls.Dial("tcp", *host, nil)
		if err != nil {
			fmt.Printf("dial host with error: %s\n", err)
			return
		}
		state := c.ConnectionState()
		for _, crt := range state.PeerCertificates {
			fmt.Println(formatCertificate(crt), "\n")
		}
	} else {
		b, err := ioutil.ReadFile(*file)
		if err != nil {
			fmt.Printf("open certificate file with error: %s\n", err)
			return
		}
		var block *pem.Block
		for len(bytes.TrimSpace(b)) != 0 {
			block, b = pem.Decode(bytes.TrimSpace(b))
			if block != nil {
				crt, err := x509.ParseCertificate(block.Bytes)
				if err != nil {
					fmt.Printf("parse certificate file with error: %s\n", err)
				} else {
					fmt.Println(formatCertificate(crt), "\n")
				}
			} else {
				fmt.Printf("parse certificate file with empty\n")
			}
		}
	}
}
