# standard

```
name
parent
display_name
title
full_name
author
page_size
page_token
next_page_token
total_size
filter
order_by
show_deleted
reconciling
retrieve
update_mask
allow_missing
etag
force
shared
proxy
settings
error
reason
detail
enable
enabled
disable
disabled
require
required
origin
bytes
miles
units
nanos
mime_type
region_code
country_code
language_code
currency_code
start_page
end_page
obfuscated_email
uid
start_time
end_time
read_time
create_time
update_time
delete_time
expire_time
time_zone
description
query
progress_percent
ttl
rating
credit_rating
async
request_id
quota
revision_id
revision_create_time
validate_only
purge
resume_token
labels
utm_source
utm_medium
utm_campaign
utm_content
utm_term
```

# commom custom methods

```
:archive
:batchGet
:batchCreate
:batchUpdate
:batchDelete
:cancel
:retrieve
:sort
:move
:rename
:merge
:search
:translate
:undelete
:add\*
:remove\*
:remove
:import
:export
:undelete
:purge
:publish
:signin
:signup
:signout
```

# commom state

"Resting states" are lifecycle states that, absent user action, are expected to remain indefinitely. However, the user can initiate an action to move a resource in a resting state into certain other states (resting or active).

```
ACCEPTED
ACTIVE
CANCELLED
DELETED
FAILED
SUCCEEDED
SUSPENDED
VERIFIED
```

"Active states" are lifecycle states that typically resolve on their own into a single expected resting state.
Note: Remember only to expose states that are useful to customers. Active states are valuable only if the resource will be in that state for a sufficient period of time. If state changes are immediate, active states are not necessary.

```
CREATING (usually becomes ACTIVE)
DELETING (usually becomes DELETED)
PENDING (usually becomes RUNNING)
REPAIRING (usually becomes ACTIVE)
RUNNING (usually becomes SUCCEEDED)
SUSPENDING (usually becomes SUSPENDED)
```

# Naming
https://cloud.google.com/apis/design/naming_convention

| 名称 | 示例 |
| :--: | :--: |
|产品名称|Google Calendar API|
|服务名称|calendar.googleapis.com|
|软件包名称|google.calendar.v3|
|接口名称|google.calendar.v3.CalendarService|
|来源目录|//google/calendar/v3|
|API 名称|calendar|
