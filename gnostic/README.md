# Install
```bash
wget https://github.com/protocolbuffers/protobuf/releases/download/v21.4/protoc-21.4-linux-x86_64.zip
unzip protoc-21.4-linux-x86_64.zip
mv bin/protoc /usr/local/bin/
mv include/google /usr/local/include/

go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28.1
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2.0
go install github.com/google/gnostic/cmd/protoc-gen-openapi@latest
go install github.com/envoyproxy/protoc-gen-validate@latest

git clone https://github.com/googleapis/api-common-protos.git --depth=1
```

# Reference
https://cloud.google.com/apis/design
https://developers.google.com/protocol-buffers/docs/reference/overview
https://oai.github.io/Documentation/start-here.html
https://google.aip.dev/general

# How to write document
https://google.aip.dev/192

# How to return error
https://google.aip.dev/193

# How to retry request
https://google.aip.dev/194
