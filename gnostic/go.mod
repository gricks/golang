module gnostic

go 1.17

require (
	github.com/envoyproxy/protoc-gen-validate v0.6.7
	google.golang.org/genproto v0.0.0-20220803205849-8f55acc8769f
	google.golang.org/grpc v1.48.0
	google.golang.org/protobuf v1.28.1
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
	golang.org/x/sys v0.0.0-20210816183151-1e6c022a8912 // indirect
	golang.org/x/text v0.3.7 // indirect
)
