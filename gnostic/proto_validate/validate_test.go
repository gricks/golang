package proto_validate

import "testing"

func TestProtoValidate(t *testing.T) {
	p := new(Person)
	err := p.Validate() // err: Id must be greater than 999
	t.Log(err)
	p.Id = 1000
	err = p.Validate() // err: Email must be a valid email address
	t.Log(err)
	p.Email = "example@lyft.com"
	err = p.Validate() // err: Phone must match pattern '^1[0-9]{12}$'
	t.Log(err)
	p.Phone = "13012341234"
	err = p.Validate() // err: Home is required
	t.Log(err)
	p.Home = &Location{Lat: 37.7, Lng: 999}
	err = p.Validate() // err: Home.Lng must be within [-180, 180]
	t.Log(err)
	p.Home.Lng = -122.4
	err = p.Validate() // err: nil
	t.Log(err)
}
