package main

import (
	"container/list"
)

type CloseCallback func(h *Handle)
type ReadCallback func(s *Stream, n int, b []byte)
type ConnectionCallback func(s *Stream, err error)

type Handle struct {
	Data       interface{}
	Loop       *Loop
	HandleType int
	HandleLink *list.Element
	Fd         int
	Flags      uint32
}

func (this *Handle) Init(l *Loop, t int) {
	this.Loop = l
	this.HandleType = t
	this.HandleLink = this.Loop.handleQueue.PushBack(this)
}

func (this *Handle) start() {
	if ISet(this.Flags, HandleStatusActive) {
		return
	}
	this.Flags |= HandleStatusActive
}

func (this *Handle) stop() {
	if !ISet(this.Flags, HandleStatusActive) {
		return
	}
	this.Flags &= ^uint32(HandleStatusActive)
}
