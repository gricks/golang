package main

import (
	"net/http"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql-go-handler"
)

// https://wehavefaces.net/learn-golang-graphql-relay-1-e59ea174a902
// https://wehavefaces.net/learn-golang-graphql-relay-2-a56cbcc3e341

var queryType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Query",
	Fields: graphql.Fields{
		"latestPost": &graphql.Field{
			Type: graphql.String,
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				return "Hello World!", nil
			},
		},
	},
})

var Schema, _ = graphql.NewSchema(graphql.SchemaConfig{
	Query: queryType,
})

func main() {
	// serve a GraphQL endpoint at `/graphql`
	http.Handle("/graphql", handler.New(&handler.Config{
		Schema: &Schema,
		Pretty: true,
	}))

	// and serve!
	http.ListenAndServe(":8080", nil)
}
