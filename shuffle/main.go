package main

import (
	"fmt"
	"math/rand"
)

func main() {
	{
		a := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
		fmt.Println("Original:\t", a)
		as := a[0 : len(a)-1]
		fmt.Println("After Sub:\t", as)
		rand.Shuffle(len(as), func(i, j int) { as[i], as[j] = as[j], as[i] })
		fmt.Println("After Shuffle:\t", as)
		fmt.Println("Original:\t", a)
	}

	fmt.Println("-------")

	{
		a := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
		fmt.Println("Original:\t", a)
		as := a[1 : len(a)-1]
		fmt.Println("After Sub:\t", as)
		rand.Shuffle(len(as), func(i, j int) { as[i], as[j] = as[j], as[i] })
		fmt.Println("After Shuffle:\t", as)
		fmt.Println("Original:\t", a)
	}
}
