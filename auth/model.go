package main

type AccountInfo struct {
	Uid      int
	Code     string
	Platform int
	CTime    int
	MTime    int
}
