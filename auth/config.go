package main

import (
	"flag"

	"github.com/Terry-Mao/goconf"
)

var (
	conf *Config
)

type Config struct {
	Address         string `goconf:"base:addr"`
	DatabaseAddr    string `goconf:"database:addr"`
	DatabaseMaxConn int    `goconf:"database:max"`
	LogName         string `goconf:"log:name"`
	LogLevel        int    `goconf:"log:level"`
}

func InitConfig() (err error) {
	var file string
	flag.StringVar(&file, "conf", "./conf/auth.conf", "auth config path")
	flag.Parse()

	conf = &Config{
		Address:         ":18080",
		DatabaseAddr:    "root:nikki@(127.0.0.1:3306)/auth_test?timeout=30s&parseTime=true&loc=Local&charset=utf8",
		DatabaseMaxConn: 10,
		LogName:         "auth",
		LogLevel:        DEBUG,
	}

	gconf := goconf.New()
	if err = gconf.Parse(file); err == nil {
		if err = gconf.Unmarshal(conf); err == nil {
			return nil
		}
	}

	return err
}
