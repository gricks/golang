HOST=""
PORT="3306"
USER="root"
PASSWORD="nikki"
DATABASE="auth_test"
MYSQL_AUTH="mysql -u${USER} -p${PASSWORD}"

sql="DROP DATABASE IF EXISTS ${DATABASE}; CREATE DATABASE \`${DATABASE}\`;"
echo ${sql} | ${MYSQL_AUTH}
if [ $? -ne 0 ]
then
    echo "Create Database ${DATABASE} Failed!"
    exit 1
fi
echo "Create Database ${DATABASE} Success!"

TABLE_NAME="account"
sql="
CREATE TABLE IF NOT EXISTS ${TABLE_NAME}
(
    uid int(10) NOT NULL AUTO_INCREMENT,
    code varchar(64) NOT NULL DEFAULT '',
    platform int(10) NOT NULL DEFAULT 0,
    ctime int(10) NOT NULL DEFAULT 0,
    mtime int(10) NOT NULL DEFAULT 0,
    PRIMARY KEY(uid), UNIQUE KEY(code)
) ENGINE=InnoDB, CHARSET=utf8;
"

echo ${sql} | ${MYSQL_AUTH} ${DATABASE}
if [ $? -ne 0 ]
then
    echo "Create Table ${TABLE_NAME} Failed!"
    exit 1
fi
echo "Create Table ${TABLE_NAME} Success!"
