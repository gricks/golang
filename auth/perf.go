package main

import (
	"net/http"
	_ "net/http/pprof"
)

func InitPerf() {
	addr := ":18081"
	go func() {
		if err := http.ListenAndServe(addr, nil); err != nil {
			logger.Error("Start Perf Serve Failed: %s", err.Error())
		}
	}()
}
