package main

import "errors"

const (
	Err_Success = iota
	Err_ServerError
	Err_InvalidParameter
	Err_VerifyFailure
)

var ErrorInfo = []string{
	"Success",
	"Server Error",
	"Invalid Parameter",
	"Verify Failure",
}

var (
	ErrServerError      = errors.New(ErrorInfo[Err_ServerError])
	ErrInvalidParameter = errors.New(ErrorInfo[Err_InvalidParameter])
	ErrVerifyFailure    = errors.New(ErrorInfo[Err_VerifyFailure])
)
