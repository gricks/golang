package main

import (
	"log"
	"net/http"
)

var (
	logger *Glogger
)

func main() {
	// Init Conf
	if err := InitConfig(); err != nil {
		log.Fatal("Init Config Failed: ", err)
	}

	// Init Logger
	logger = NewGlogger(conf.LogName, conf.LogLevel)

	// Init Perf
	InitPerf()

	// Init Mysql
	if err := InitMysql(conf.DatabaseAddr, conf.DatabaseMaxConn); err != nil {
		logger.Fatal("Init Mysql Failed: %s", err.Error())
	}
	defer CloseMysql()

	// Start Server
	logger.Info("Start Auth Server ...")

	mtx := http.NewServeMux()
	mtx.HandleFunc("/x/v1/auth", Auth)
	if err := http.ListenAndServe(conf.Address, mtx); err != nil {
		logger.Fatal("ListenAndServe: %s", err.Error())
	}

	logger.Info("Stop Auth Server ...")
}
