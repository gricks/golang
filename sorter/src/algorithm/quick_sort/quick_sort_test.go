package quick_sort

import "testing"

func TestQuickSort1(t *testing.T) {
	vals := []int{4, 2, 1, 3, 1, 5}
	rets := []int{1, 1, 2, 3, 4, 5}
	QuickSort(vals)
	for i := 0; i < len(vals); i++ {
		if vals[i] != rets[i] {
			t.Error("BubbleSort() failed, Got", vals, "Expect", rets)
		}
	}
}

func TestQuickSort2(t *testing.T) {
	vals := []int{4, 2, 4, 3, 3, 5}
	rets := []int{2, 3, 3, 4, 4, 5}
	QuickSort(vals)
	for i := 0; i < len(vals); i++ {
		if vals[i] != rets[i] {
			t.Error("BubbleSort() failed, Got", vals, "Expect", rets)
		}
	}
}
