package bubble_sort

import "testing"

func TestBubbleSort1(t *testing.T) {
	vals := []int{4, 2, 1, 3, 5}
	rets := []int{1, 2, 3, 4, 5}
	BubbleSort(vals)
	for i := 0; i < len(vals); i++ {
		if vals[i] != rets[i] {
			t.Error("BubbleSort() failed, Got", vals, "Expect", rets)
		}
	}
}

func TestBubbleSort2(t *testing.T) {
	vals := []int{4, 4, 3, 3, 5}
	rets := []int{3, 3, 4, 4, 5}
	BubbleSort(vals)
	for i := 0; i < len(vals); i++ {
		if vals[i] != rets[i] {
			t.Error("BubbleSort() failed, Got", vals, "Expect", rets)
		}
	}
}
