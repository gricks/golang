package bubble_sort

func BubbleSort(vals []int) {
	for i := 0; i < len(vals)-1; i++ {
		for j := 0; j < len(vals)-i-1; j++ {
			if vals[j] > vals[j+1] {
				vals[j], vals[j+1] = vals[j+1], vals[j]
			}
		}
	}
}
