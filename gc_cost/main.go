package main

import (
	"fmt"
	"runtime"
	"strconv"
	"time"
)

type DataIntV struct{ V int }
type DataIntP struct{ V *int }

var (
	msi  = map[string]int{}
	mis  = map[int]string{}
	mii  = map[int]int{}
	miv  = map[int]DataIntV{}
	mip  = map[int]DataIntP{}
	mivp = map[int]*DataIntV{}
	mipp = map[int]*DataIntP{}
	mivv = map[int]interface{}{}
)

func main() {
	n, m := 10, 1_000_000
	for i := 0; i < 7; i++ {
		clean()
		for k := 0; k < m; k++ {
			fill(i, k, n)
		}
		fmt.Printf("init %d complete. ", i)
		runtime.GC()
		runtime.GC()
		t := time.Now()
		for k := 0; k < n; k++ {
			runtime.GC()
		}
		fmt.Printf("gc cost: %v\n", time.Since(t))
	}
}

func clean() {
	msi = nil
	mis = nil
	mii = nil
	miv = nil
	mip = nil
	mivp = nil
	mipp = nil
}

func fill(i, k, n int) {
	switch i {
	case 0:
		if k == 0 {
			msi = make(map[string]int, n)
		}
		msi[strconv.Itoa(k)] = k
	case 1:
		if k == 0 {
			mis = make(map[int]string, n)
		}
		mis[k] = strconv.Itoa(k)
	case 2:
		if k == 0 {
			mii = make(map[int]int, n)
		}
		mii[k] = k
	case 3:
		if k == 0 {
			miv = make(map[int]DataIntV, n)
		}
		miv[k] = DataIntV{V: k}
	case 4:
		if k == 0 {
			mip = make(map[int]DataIntP, n)
		}
		mip[k] = DataIntP{V: &k}
	case 5:
		if k == 0 {
			mivp = make(map[int]*DataIntV, n)
		}
		mivp[k] = &DataIntV{V: k}
	case 6:
		if k == 0 {
			mipp = make(map[int]*DataIntP, n)
		}
		mipp[k] = &DataIntP{V: &k}
	case 7:
		if k == 0 {
			mivv = make(map[int]interface{})
		}
		mivv[k] = &DataIntP{V: &k}
	}
}
