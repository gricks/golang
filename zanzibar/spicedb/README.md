# Install
```
go install github.com/authzed/zed/cmd/zed@latest
docker run --rm -p 50051:50051 authzed/spicedb serve --grpc-preshared-key "testpresharedkey"
```

# Command

```
export ZED_KEYRING_PASSWORD=123
zed context set dev localhost:50051 testpresharedkey --insecure
zed context list
zed validate schema.yaml
zed import schema.yaml
zed schema read
zed permission check task:org_team_1_task1 rescue role:org_team_1_task_manager --explain
zed permission check task:org_team_1_task1 rescue role:org_team_1_task_viewer --explain
zed permission check task:org_team_1_task1 detail role:org_team_1_task_viewer --explain
zed permission check task:org_team_1_task1 detail role:org_team_2_task_viewer --explain // shared_team
zed permission check task:org_team_2_task1 detail role:org_team_1_task_viewer --explain
```

# Playground
https://play.authzed.com/schema
