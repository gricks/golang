package main

import "fmt"

type Bird struct{}

func (b *Bird) talk() {
	fmt.Println("jijiji")
}

type Duck struct{}

func (d *Duck) talk() {
	fmt.Println("gagaga")
}

type Dog struct{}

func (d *Dog) talk() {
	fmt.Println("wawawa")
}

type ITalk interface {
	talk()
}

func main() {
	array := []ITalk{}
	dog := Dog{}
	bird := Bird{}
	duck := Duck{}

	array = append(array, &dog)
	array = append(array, &bird)
	array = append(array, &duck)

	for _, val := range array {
		val.talk()
	}
}
