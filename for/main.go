package main

import "fmt"

func all() []int {
	fmt.Println("all running")
	return []int{1, 2, 3}
}

func main() {
	for {
		fmt.Printf("for\n")
		break
	}

	for i := 0; i < 10; i++ {
		fmt.Println(i)
	}

	vec := [5]int{4, 3, 2, 1}
	for k, v := range vec {
		fmt.Println(k, v)
	}

	str := "hello"
	for k, v := range str {
		fmt.Println(k, v)
	}

	for i := 1; i < len(str); i += 2 {
		fmt.Printf("%c%c", str[i-1], str[i])
	}

	fmt.Println()
	fmt.Printf("%d, %d\n", ('A'&0xdf-'A')+10, ('a'&0xdf-'A')+10)

	for _, v := range all() {
		fmt.Println(v)
	}
}
