package main

import (
	"fmt"
	"runtime"
	"runtime/debug"
)

func apply() uint64 {
	var s *[100]int
	for i := 0; i < 10000; i++ {
		s = &[100]int{}
		s[0] = 1
	}
	var stat runtime.MemStats
	runtime.ReadMemStats(&stat)
	return stat.HeapInuse
}

func main() {
	debug.SetGCPercent(-1)

	var stat runtime.MemStats
	runtime.ReadMemStats(&stat)
	fmt.Println("heap:", stat.HeapInuse)

	fmt.Println("heap:", apply())

	debug.SetGCPercent(100)
	runtime.GC()

	runtime.ReadMemStats(&stat)
	fmt.Println("heap:", stat.HeapInuse)
}
