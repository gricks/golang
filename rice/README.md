# Install
```bash
go get github.com/GeertJohan/go.rice/rice
```

# Tool Usage

## Embed resources by generating Go source code
```bash
rice embed-go
go build
```

## Append resources to executable as zip file
```bash
go build -o main
rice append --exec main
```
