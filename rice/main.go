package main

import (
	"net/http"

	rice "github.com/GeertJohan/go.rice"
)

func main() {
	http.Handle("/", http.FileServer(rice.MustFindBox("static").HTTPBox()))
	http.ListenAndServe(":8080", nil)
}
