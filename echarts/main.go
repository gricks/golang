package main

import (
	"math/rand"
	"net/http"

	"github.com/go-echarts/go-echarts/v2/charts"
	"github.com/go-echarts/go-echarts/v2/opts"
)

var weeks = []string{"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"}

func items() []opts.BarData {
	items := make([]opts.BarData, 0)
	for i := 0; i < len(weeks); i++ {
		items = append(items,
			opts.BarData{Value: rand.Intn(300)})
	}
	return items
}

func basicBar() *charts.Bar {
	bar := charts.NewBar()
	bar.SetGlobalOptions(
		charts.WithTitleOpts(opts.Title{
			Title:    "Title",
			Subtitle: "Subtitle",
		}),
	)

	bar.SetXAxis(weeks).
		AddSeries("Category A", items()).
		AddSeries("Category B", items())
	return bar
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		bar := basicBar()
		bar.Render(w)
	})
	http.ListenAndServe(":9988", nil)
}
