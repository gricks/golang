package main

import (
	"fmt"
	"strings"
)

func main() {
	file := "game_20230101.003.log"
	file = strings.Replace(file, ".", " ", -1)

	var serial int
	var prefix string
	n, err := fmt.Fscanf(strings.NewReader(file),
		"%s%03d", &prefix, &serial)

	fmt.Println(prefix, serial)
	fmt.Println(n, err)
}
