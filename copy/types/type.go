package types

// +k8s:deepcopy-gen=true

type Struct struct {
	V1 int32
	V2 string
	V3 []int
	V4 []SubStruct
	V5 []*SubStruct
	V6 map[string]SubStruct
	V7 map[string]*SubStruct
	V8 Bits
}

// +k8s:deepcopy-gen=true

type Bits []int

// +k8s:deepcopy-gen=true

type SubStruct struct {
	SV1 int32
	SV2 []byte
	SV3 string
}
