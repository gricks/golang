package main

import (
	"fmt"
	"html/template"
	"os"
	"path/filepath"
	"strings"

	"github.com/Masterminds/sprig/v3"
)

func HTMLRender(name string, data any) {
	ext := filepath.Ext(name)
	fullname, _ := filepath.Abs(name)
	pattern := strings.TrimSuffix(fullname, ext) + "*" + ext
	tpl, err := template.New(filepath.Base(name)).
		Funcs(sprig.GenericFuncMap()).
		ParseGlob(pattern)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(-1)
	}
	err = tpl.Execute(os.Stdout, data)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(-1)
	}
}
