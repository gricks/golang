package main

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	html    = false
	file    = ""
	RootCmd = &cobra.Command{
		Use:   "template",
		Short: "A toolkit for template",
		Run:   runRoot,
	}
)

func init() {
	RootCmd.Flags().BoolVar(&html, "html", html, "html")
	RootCmd.Flags().StringVarP(&file, "file", "f", file, "file")
}

func runRoot(cmd *cobra.Command, args []string) {
	if len(file) == 0 {
		fmt.Fprintln(os.Stderr, "Error: file must not empty")
		cmd.Usage()
		os.Exit(-1)
	}
	data := read(file)
	if html {
		HTMLRender(file, data)
	} else {
		TEXTRender(file, data)
	}
}

func read(name string) any {
	name, _ = filepath.Abs(name)
	ext := filepath.Ext(name)
	dir, file := filepath.Split(name)
	in := strings.TrimSuffix(file, ext) + ".data"
	viper.AddConfigPath(dir)
	viper.SetConfigName(in)
	err := viper.ReadInConfig()
	if err != nil {
		if !errors.As(err, new(viper.ConfigFileNotFoundError)) {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(-1)
		}
	}
	var data any
	err = viper.Unmarshal(&data)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(-1)
	}
	return data
}

func main() {
	RootCmd.Execute()
}
