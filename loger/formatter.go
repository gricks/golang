package loger

const timeformat = "2006-01-02 15:04:05"

type Formatter interface {
	Format(*Entry) ([]byte, error)
}
