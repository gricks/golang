package loger

import (
	"os"
	"testing"
)

func Test_print(t *testing.T) {

}

func TestExport(t *testing.T) {
	SetLevel(Debug)
	l1 := WithFields(map[string]interface{}{"Uid": "10000"})
	l1.Info("cc is coco")
	l1.Debug("vv is vivi")

	l2 := WithFields(map[string]interface{}{"Uid": "10001"})
	l2.Info("cc is coco")
	l2.Debug("vv is vivi")
}

func TestNewLogger(t *testing.T) {
	logger := New()
	logger.SetLevel(Debug)
	l1 := logger.WithFields(map[string]interface{}{"Uid": "10002"})
	l1.Info("cc is coco")
	l1.Debug("vv is vivi")

	l2 := logger.WithFields(map[string]interface{}{"Uid": "10003"})
	l2.Info("cc is coco")
	l2.Debug("vv is vivi")
}

func TestWrapper(t *testing.T) {
	wrapper := NewWrapper("loger_wrapper", Debug)
	hook, err := NewElasticHook("http://localhost:9200", "logrus")
	if err == nil {
		wrapper.AddHook(hook)
	}

	l1 := wrapper.WithField("10004", "1", "GET_USER_INFO")
	l1.Info("cc is coco")
	l1.Info("I'm so happy today")
	l1.Debug("Not too bad")
	l1.Debug("Not|too|bad")
	l1.Debug("that's OK")

	l2 := wrapper.WithField("10005", "1", "GET_USER_INFO")
	l2.Info("cc is coco")
	l2.Info("I'm so happy today")
	l2.Debug("Not too bad")
	l2.Debug("Not|too|bad")
	l2.Debug("that's OK")
}

func BenchmarkDummyLogger(b *testing.B) {
	var tfs = map[string]interface{}{
		"foo":   "bar",
		"baz":   "qux",
		"one":   "two",
		"three": "four",
	}
	f, err := os.OpenFile("loger.log", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		b.Fatalf("%v", err)
	}
	defer f.Close()
	logger := Logger{
		level:     Infos,
		writer:    f,
		formatter: &TextFormatter{},
	}
	entry := logger.WithFields(tfs)
	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			entry.Info("aaa")
		}
	})
}

func BenchmarkWrapperLogger(b *testing.B) {
	wrapper := NewWrapper("loger_bench", Debug)
	entry := wrapper.WithField("10004", "1", "GET_USER_INFO")
	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			entry.Info("aaa")
		}
	})
}

func BenchmarkWrapperLogger2(b *testing.B) {
	wrapper := NewWrapper("loger_bench", Debug)
	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			entry := wrapper.WithoutField()
			entry.Info("aaa")
		}
	})
}

func BenchmarkWrapperLogger3(b *testing.B) {
	wrapper := NewWrapper("loger_bench", Debug)
	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			entry := wrapper.WithField("10004", "1", "GET_USER_INFO")
			entry.Info("aaa")
		}
	})
}
