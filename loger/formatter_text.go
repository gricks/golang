package loger

import "strings"

type TextFormatter struct{}

func (this *TextFormatter) Format(entry *Entry) ([]byte, error) {
	data := []string{}

	data = append(data, entry.Time.Format(timeformat))
	data = append(data, entry.Level.String())
	for k, v := range entry.Data {
		data = append(data, k+":"+v.(string))
	}
	data = append(data, entry.Message+"\n")

	return []byte(strings.Join(data, "|")), nil
}
