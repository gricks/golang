package loger

import "io"

var (
	std = New()
)

func StandardLogger() *Logger {
	return std
}

func SetOutput(writer io.Writer)                      { std.SetOutput(writer) }
func SetFormatter(formatter Formatter)                { std.SetFormatter(formatter) }
func SetLevel(level Level)                            { std.SetLevel(level) }
func GetLevel() Level                                 { return std.GetLevel() }
func AddHook(hook Hook)                               { std.AddHook(hook) }
func WithFields(fields map[string]interface{}) *Entry { return std.WithFields(fields) }
