package main

import "fmt"

type Server interface {
	MakeRequest() interface{}
	Handle(interface{}) interface{}
}

var registerServer = map[string]Server{}

func RegisterServer(m string, s Server) {
	if _, ok := registerServer[m]; ok {
		panic(fmt.Sprintf("duplicate method:%s", m))
	}
	registerServer[m] = s
}

/////////////////////////////

func init() {
	RegisterServer("login", &loginServer{})
}

type LoginRequest struct {
	Account  string `schema:"acc"`
	Password string `schema:"pas"`
}

type LoginResponse struct {
	Account  string `json:"acc"`
	Password string `json:"pas"`
}

type loginServer struct{}

func (this *loginServer) MakeRequest() interface{} {
	return new(LoginRequest)
}

func (this *loginServer) Handle(r interface{}) interface{} {
	req := r.(*LoginRequest)
	fmt.Println("Request", req)
	return &LoginResponse{req.Account, req.Password}
}
