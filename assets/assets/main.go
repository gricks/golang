package main

import (
	"fmt"
	"net/http"
)

func main() {
	AssetsHandler := http.StripPrefix("/assets/", http.FileServer(http.Dir("assets")))
	http.HandleFunc("/assets/", AssetsHandler.ServeHTTP)

	if err := http.ListenAndServe(":18088", nil); err != nil {
		fmt.Println("Start Faild:", err)
	}
}
