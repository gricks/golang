package main

import (
	"bytes"
	"flag"
	"fmt"
	"net"
)

var (
	addr   = flag.String("addr", "127.0.0.1:19889", "addr")
	client = flag.Bool("client", false, "client")
	server = flag.Bool("server", false, "server")
)

func init() {
	flag.Parse()
}

func main() {
	switch {
	case *server:
		Server()
	case *client:
		Client()
	}
}

func Client() {
	vconn, err := net.Dial("tcp", *addr)
	if err != nil {
		panic(err)
	}
	conn := vconn.(*net.TCPConn)
	conn.SetWriteBuffer(1010)

	for i := 0; i < 1000000; i++ {
		n, err := conn.Write(bytes.Repeat([]byte("a"), 100))
		if err != nil {
			panic(err)
		}
		fmt.Println("write success:", n)
	}
}

func Server() {
	lis, err := net.Listen("tcp", ":19889")
	if err != nil {
		panic(err)
	}
	defer lis.Close()

	for {
		_, err := lis.Accept()
		if err != nil {
			panic(err)
		}
	}
}
