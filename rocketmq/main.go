package main

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/apache/rocketmq-client-go/v2"
	"github.com/apache/rocketmq-client-go/v2/consumer"
	"github.com/apache/rocketmq-client-go/v2/primitive"
	"github.com/apache/rocketmq-client-go/v2/producer"
)

var (
	topic   = "test_topic"
	group   = "testGroup"
	namesrv = []string{"127.0.0.1:9876"}
)

func syncProducer() rocketmq.Producer {
	p, err := rocketmq.NewProducer(
		producer.WithNsResolver(primitive.NewPassthroughResolver(namesrv)),
		producer.WithRetry(2),
	)
	if err != nil {
		panic(err)
	}
	err = p.Start()
	if err != nil {
		panic(err)
	}
	return p
}

func consumerGroup(n int) {
	for i := 0; i < n; i++ {
		go func(k int) {
			c, err := rocketmq.NewPushConsumer(
				consumer.WithGroupName(group),
				consumer.WithInstance(group+"_"+fmt.Sprintf("%d", k)),
				consumer.WithNsResolver(primitive.NewPassthroughResolver(namesrv)),
			)
			if err != nil {
				panic(err)
			}
			err = c.Subscribe(topic, consumer.MessageSelector{}, func(ctx context.Context,
				msgs ...*primitive.MessageExt) (consumer.ConsumeResult, error) {
				for i := range msgs {
					fmt.Printf("index: %v, receive: %v, %v\n", k, msgs[i].Topic, string(msgs[i].Body))
				}
				return consumer.ConsumeSuccess, nil
			})
			if err != nil {
				panic(err)
			}

			// Note: start after subscribe
			err = c.Start()
			if err != nil {
				panic(err)
			}

			time.Sleep(time.Hour)
			err = c.Shutdown()
			if err != nil {
				fmt.Printf("shutdown Consumer error: %s", err.Error())
			}
		}(i)
	}
}

// Package main implements a simple producer to send message.
func main() {
	consumerGroup(4)

	p := syncProducer()
	for i := 0; i < 20; i++ {
		msg := &primitive.Message{
			Topic: topic,
			Body:  []byte("Hello RocketMQ Go Client! " + strconv.Itoa(i)),
		}
		res, err := p.SendSync(context.Background(), msg)
		if err != nil {
			fmt.Printf("send message error: %s\n", err)
		} else {
			fmt.Printf("send message success: result=%s\n", res.String())
		}
		time.Sleep(time.Second)
	}
	err := p.Shutdown()
	if err != nil {
		fmt.Printf("shutdown producer error: %s", err.Error())
	}
}
