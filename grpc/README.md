# nignx proxy grpc

[nginx doc](http://nginx.org/en/docs/http/ngx_http_grpc_module.html#grpc_set_header)

```
server {
    listen  9099  http2;
    access_log    /var/log/nginx/access-grpc.log;
    location / {
        grpc_pass grpc://127.0.0.1:9091;
        grpc_set_header X-Real-IP $remote_addr;
    }
}
```

# read from grpc context

```
md, ok := metadata.FromIncomingContext(ctx)
if ok {
    md.Get("x-real-ip")
}
```
