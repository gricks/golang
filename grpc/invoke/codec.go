package main

import "encoding/json"

type JSON struct{}

func (JSON) Marshal(v any) ([]byte, error) {
	return json.Marshal(v)
}

func (JSON) Unmarshal(data []byte, v any) error {
	return json.Unmarshal(data, v)
}

func (JSON) Name() string {
	return "json"
}
