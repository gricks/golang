package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"reflect"
	"strings"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/encoding"
	"google.golang.org/grpc/reflection"
	"google.golang.org/protobuf/types/known/durationpb"

	"github.com/fullstorydev/grpcurl"
)

type Calc struct {
	UnimplementedCalcServer
}

func (Calc) Multiple(ctx context.Context, in *MultipleRequest) (*MultipleResponse, error) {
	return &MultipleResponse{
		V: in.A * in.B,
		D: durationpb.New(time.Second),
	}, nil
}

func server() {
	lis, err := net.Listen("tcp", ":9900")
	if err != nil {
		log.Fatalf("failed to listen: %v",
			err)
	}
	encoding.RegisterCodec(new(JSON))
	s := grpc.NewServer(grpc.UnaryInterceptor(
		func(ctx context.Context, request interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
			fmt.Println("request:", request)
			resp, err = handler(ctx, request)
			fmt.Println("response:", resp)
			if err != nil {
				panic(err)
			}
			return resp, nil
		}))
	RegisterCalcServer(s, &Calc{})
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v",
			err)
	}
}

func main() {
	go server()
	time.Sleep(time.Second)

	cc, err := grpc.Dial("127.0.0.1:9900",
		grpc.WithTransportCredentials(
			insecure.NewCredentials()))
	if err != nil {
		panic(err)
	}

	ctx := context.Background()
	symbol := Calc_Multiple_FullMethodName

	fmt.Println("dial from grpc client")
	{
		c := NewCalcClient(cc)
		c.Multiple(ctx,
			&MultipleRequest{
				A: 10, B: 5,
			})
	}

	fmt.Println("\ndial from invoke")
	{
		var v any
		cc.Invoke(ctx, symbol,
			&MultipleRequest{
				A: 5, B: 30,
			}, &v)
		fmt.Printf("%+v %+v\n", reflect.ValueOf(v), reflect.TypeOf(v))
		resp := new(MultipleResponse)
		cc.Invoke(ctx, symbol,
			&MultipleRequest{
				A: 5, B: 30,
			}, resp)
		fmt.Println("resp invoke:", resp)
	}

	fmt.Println("\ndial from invoke json")
	{
		var r = struct {
			A int `json:"a"`
			B int `json:"b"`
		}{
			A: 5, B: 100,
		}
		var v any
		cc.Invoke(ctx, symbol, &r, &v, grpc.CallContentSubtype("json"))
		fmt.Println("resp invoke json:", v)
	}

	fmt.Println("\ndial from protoset")
	{
		ctx := context.Background()
		ds, err := grpcurl.DescriptorSourceFromProtoSets("calc.protoset")
		if err != nil {
			panic(err)
		}
		in := strings.NewReader(`{"a":10,"b":"3"}`)
		options := grpcurl.FormatOptions{}
		format := grpcurl.Format("json")
		rf, formatter, err := grpcurl.RequestParserAndFormatter(format, ds, in, options)
		if err != nil {
			panic(err)
		}
		h := &grpcurl.DefaultEventHandler{
			Out:       os.Stdout,
			Formatter: formatter,
		}
		err = grpcurl.InvokeRPC(ctx, ds, cc, "main.Calc/Multiple", nil, h, rf.Next)
		if err != nil {
			panic(err)
		}
	}
}
