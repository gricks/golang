package main

import (
	"flag"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"os"
	"path/filepath"
	"strings"
	"unicode"
)

var (
	fpath = flag.String("f", ".", "file path")
)

func init() {
	flag.Parse()
}

func main() {
	store := map[string]map[string][]string{}
	for _, file := range walk(*fpath) {
		af, err := parser.ParseFile(token.NewFileSet(), file, nil, 0)
		if err != nil {
			panic(err)
		}

		packageStore, ok := store[af.Name.String()]
		if !ok {
			packageStore = make(map[string][]string)
			store[af.Name.String()] = packageStore
		}

		for i := range af.Decls {
			if funcDecl, ok := af.Decls[i].(*ast.FuncDecl); ok {
				if funcDecl.Name.Obj != nil && isExported(funcDecl.Name.Obj.Name) {
					packageStore["func"] = append(packageStore["func"], funcDecl.Name.Obj.Name)
				}
			} else if genDecl, ok := af.Decls[i].(*ast.GenDecl); ok {
				switch genDecl.Tok {
				case token.CONST:
					for _, spec := range genDecl.Specs {
						v := spec.(*ast.ValueSpec)
						if isExported(v.Names[0].String()) {
							packageStore["const"] = append(packageStore["const"], v.Names[0].String())
						}
					}
				case token.TYPE:
					for _, spec := range genDecl.Specs {
						v := spec.(*ast.TypeSpec)
						if isExported(v.Name.String()) {
							packageStore["type"] = append(packageStore["type"], v.Name.String())
						}
					}
				case token.VAR:
					for _, spec := range genDecl.Specs {
						v := spec.(*ast.ValueSpec)
						if isExported(v.Names[0].String()) {
							packageStore["var"] = append(packageStore["var"], v.Names[0].String())
						}
					}
				}
			}
		}
	}

	for k, packageStore := range store {
		fmt.Println("package", k)

		if vars, ok := packageStore["var"]; ok {
			fmt.Println("var (")
			for _, v := range vars {
				fmt.Printf("    %s = %s.%s\n", v, k, v)
			}
			fmt.Println(")")
		}

		if consts, ok := packageStore["const"]; ok {
			fmt.Println("const (")
			for _, v := range consts {
				fmt.Printf("    %s = %s.%s\n", v, k, v)
			}
			fmt.Println(")")
		}

		if funcs, ok := packageStore["func"]; ok {
			fmt.Println("var (")
			for _, v := range funcs {
				fmt.Printf("    %s = %s.%s\n", v, k, v)
			}
			fmt.Println(")")
		}

		if types, ok := packageStore["type"]; ok {
			fmt.Println("type (")
			for _, v := range types {
				fmt.Printf("    %s = %s.%s\n", v, k, v)
			}
			fmt.Println(")")
		}

		fmt.Println()
	}
}

func walk(fpath string) []string {
	files := []string{}
	if err := filepath.Walk(fpath,
		func(f string, fi os.FileInfo, err error) error {
			if fi != nil &&
				fi.Mode().IsRegular() &&
				strings.HasSuffix(fi.Name(), ".go") &&
				!strings.HasSuffix(fi.Name(), "_test.go") {
				files = append(files, f)
			}
			return nil
		}); err != nil {
		panic(err)
	}
	return files
}

func isExported(s string) bool {
	if len(s) > 0 && unicode.IsUpper(rune(s[0])) {
		return true
	}
	return false
}
