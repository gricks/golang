package main

// #include "main.h"
// CFLAGS: -lm
import "C"

import (
	"fmt"
	"io/ioutil"
	"time"
)

var n = 0

//export AtExit
func AtExit() {
	ioutil.WriteFile(fmt.Sprintf("exit%d", n), []byte("yes, exit success\n"), 0644)
	n++
}

func main() {
	C.set_atexit()
	fmt.Println("call set_atexit")

	C.srand(C.uint(time.Now().Unix()))
	fmt.Println(C.rand() % 1000)

	// try kill -9
	for {
		time.Sleep(time.Second)
	}
}
