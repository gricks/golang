#include <stdlib.h>

extern void AtExit();

static inline set_atexit() {
	atexit(AtExit);
	AtExit();
}
