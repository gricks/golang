package main

import (
	"bufio"
	"fmt"
	"os"
	"sync"
)

var (
	Waiter = &sync.WaitGroup{}

	LoadQueue  = make(chan *string, 100)
	ParseQueue = make(chan *Info, 100)
)

type Info struct {
	data *string
}

func Load() error {
	file, err := os.OpenFile("main.go", os.O_RDONLY, 0660)
	if err != nil {
		return err
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		LoadQueue <- &line
	}
	close(LoadQueue)
	Waiter.Done()
	return scanner.Err()
}

func Parse() {
	for {
		line, ok := <-LoadQueue
		if !ok {
			break
		}
		// Parse Raw Data To Struct
		d := &Info{}
		d.data = line
		ParseQueue <- d
	}
	close(ParseQueue)
	Waiter.Done()
}

func Handle() {
	for {
		info, ok := <-ParseQueue
		if !ok {
			break
		}
		// Handle Struct Data
		fmt.Println(*info.data)
	}
	Waiter.Done()
}

func main() {
	Waiter.Add(3)
	go Load()
	go Parse()
	go Handle()
	Waiter.Wait()
}
