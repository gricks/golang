package circle

import "testing"

func TestCircle1(t *testing.T) {
	c := New(10) // capacity 2^10

	if c.Capacity() != 1024 {
		t.Error("invalid capacity", c.Capacity())
	}

	if c.Size() != 0 {
		t.Error("invalid size")
	}

	if false == c.Push(10) {
		t.Error("push failed")
	}

	if false == c.Push(3) {
		t.Error("push failed")
	}

	if c.Size() != 2 {
		t.Error("invalid size")
	}

	val1 := c.Pop().(int)
	if val1 != 10 {
		t.Error("pop error data")
	}

	if c.Size() != 1 {
		t.Error("invalid size")
	}

	val2 := c.Pop().(int)
	if val2 != 3 {
		t.Error("pop error data")
	}
}
