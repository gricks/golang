package main

import (
	"fmt"
	"unicode"
)

func ExistChinese(s string) bool {
	for _, r := range s {
		if unicode.Is(unicode.Scripts["Han"], r) {
			return true
		}
	}
	return false
}

func main() {
	fmt.Println(ExistChinese("vim-go"))
	fmt.Println(ExistChinese("vim-go 你好"))
}
