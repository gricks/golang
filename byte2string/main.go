package main

import "fmt"

func main() {
	b := []byte{}
	var v byte
	for {
		_, err := fmt.Scan(&v)
		if err != nil {
			break
		}
		b = append(b, v)
	}
	fmt.Println(string(b))
}
