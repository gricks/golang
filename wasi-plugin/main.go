package main

import (
	"strings"

	"github.com/tidwall/gjson"

	"github.com/tetratelabs/proxy-wasm-go-sdk/proxywasm"
	"github.com/tetratelabs/proxy-wasm-go-sdk/proxywasm/types"
)

func main() {
	proxywasm.SetVMContext(&vmContext{})
}

type vmContext struct {
	// Embed the default VM context here,
	// so that we don't need to reimplement all the methods.
	types.DefaultVMContext
}

// Override types.DefaultVMContext.
func (*vmContext) NewPluginContext(contextID uint32) types.PluginContext {
	return &pluginContext{}
}

type pluginContext struct {
	// Embed the default plugin context here,
	// so that we don't need to reimplement all the methods.
	types.DefaultPluginContext

	headerKey   string
	headerValue string
}

// Override types.DefaultPluginContext.
func (ctx *pluginContext) OnPluginStart(pluginConfigurationSize int) types.OnPluginStartStatus {
	data, err := proxywasm.GetPluginConfiguration()
	if data == nil {
		return types.OnPluginStartStatusOK
	}

	if err != nil {
		proxywasm.LogCriticalf("pluginContext: error reading plugin configuration: %v",
			err)
		return types.OnPluginStartStatusFailed
	}

	if !gjson.Valid(string(data)) {
		proxywasm.LogCritical("pluginContext: invalid configuration format")
		return types.OnPluginStartStatusFailed
	}

	ctx.headerKey, ctx.headerValue = strings.TrimSpace(gjson.Get(string(data), "key").Str),
		strings.TrimSpace(gjson.Get(string(data), "value").Str)

	if ctx.headerKey == "" || ctx.headerValue == "" {
		proxywasm.LogCritical("pluginContext: invalid configuration format")
		return types.OnPluginStartStatusFailed
	}

	proxywasm.LogInfof("pluginContext: header from config: %s = %s",
		ctx.headerKey, ctx.headerValue)

	return types.OnPluginStartStatusOK
}

// Override types.DefaultPluginContext.
func (ctx *pluginContext) NewHttpContext(contextID uint32) types.HttpContext {
	return &httpContext{
		contextID:   contextID,
		headerKey:   ctx.headerKey,
		headerValue: ctx.headerValue,
	}
}

type httpContext struct {
	// Embed the default http context here,
	// so that we don't need to reimplement all the methods.
	types.DefaultHttpContext

	contextID uint32

	headerKey   string
	headerValue string
}

// Override types.DefaultHttpContext.
func (ctx *httpContext) OnHttpResponseHeaders(_ int, _ bool) types.Action {
	// Add the header passed by arguments
	if ctx.headerKey != "" {
		if err := proxywasm.AddHttpResponseHeader(ctx.headerKey, ctx.headerValue); err != nil {
			proxywasm.LogCriticalf("httpContext: failed to set response headers(%s,%s): %v",
				ctx.headerKey, ctx.headerValue, err)
		}
	}
	return types.ActionContinue
}

// Override types.DefaultHttpContext.
func (ctx *httpContext) OnHttpStreamDone() {
	proxywasm.LogInfof("httpContext: %d finished", ctx.contextID)
}
