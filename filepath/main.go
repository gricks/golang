package main

import (
	"fmt"
	"path/filepath"
)

func main() {
	pathname := "../../golang/filepath"
	fmt.Println(filepath.Dir(pathname))
	fmt.Println(filepath.Base(pathname))
	fmt.Println(filepath.IsAbs(pathname))
	fmt.Println(filepath.Abs(pathname))
	fmt.Println(filepath.Split(pathname))
	fmt.Println(filepath.Clean(pathname))
	fmt.Println(filepath.Glob("../../golang/*path"))
}
