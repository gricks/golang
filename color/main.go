package main

import (
	"fmt"
	"log"
)

const (
	info = "\x1b[94mINFO\x1b[0m"
	trac = "\x1b[92mTRAC\x1b[0m"
	warn = "\x1b[1;93mWARN\x1b[0m"
	erro = "\x1b[1;91mERRO\x1b[0m"
)

func Info(format string, a ...interface{}) {
	log.Printf("%s | %s", info, fmt.Sprintf(format, a...))
}

func Trace(format string, a ...interface{}) {
	log.Printf("%s | %s", trac, fmt.Sprintf(format, a...))
}

func Warn(format string, a ...interface{}) {
	log.Printf("%s | %s", warn, fmt.Sprintf(format, a...))
}

func Error(format string, a ...interface{}) {
	log.Printf("%s | %s", erro, fmt.Sprintf(format, a...))
}

func init() {
	log.SetFlags(log.Flags() | log.Lshortfile)
}

func main() {
	Info("%s", "this is a info log")
	Warn("%s", "this is a warn log")
	Trace("%s", "this is a trace log")
	Error("%s", "this is a error log")
}
