package main

import (
	"flag"
	"fmt"
	"os"
)

/*
./flag -name Astone -age 18 -info "play game"
./flag -name=Astone -age=18 -info="play game" -F
*/

var (
	age   string
	name  string
	info  = flag.String("info", "nop", "auth info")
	flags = flag.Bool("F", false, "flags")
)

func init() {
	flag.StringVar(&age, "age", "21", "auth age")
	flag.StringVar(&name, "name", "Astone", "auth name")
}

func main() {
	flag.Parse()
	if flag.NFlag() < 3 {
		flag.Usage()
		os.Exit(-1)
	}

	fmt.Println("name:", name)
	fmt.Println("age:", age)
	fmt.Println("info:", *info)
	fmt.Println("F:", *flags)
}
