package main_test

import "testing"

var (
	stu *Student
	val int
)

type Student struct {
	Name  string
	Age   int
	Score int
}

func (s *Student) Study() int {
	for i := 0; i < 100; i++ {
		s.Score++
	}
	return s.Score
}

func fn1() int {
	s := new(Student)
	return s.Study()
}

func fn2() int {
	ss := make([]Student, 1)
	return ss[0].Study()
}

func fn3() int {
	ss := make([]Student, 10000)
	return ss[0].Study()
}

func fn4() *Student {
	s := new(Student)
	s.Study()
	return s
}

func fn5() int {
	s := fn4()
	return s.Study()
}

func BenchmarkEscape(b *testing.B) {
	b.Run("fn1", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			val = fn1()
		}
	})
	b.Run("fn2", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			val = fn2()
		}
	})
	b.Run("fn3", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			val = fn3()
		}
	})
	b.Run("fn4", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			stu = fn4()
		}
	})
	b.Run("fn4-0", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_ = fn4()
		}
	})
	var stu4_1 *Student
	b.Run("fn4-1", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			stu4_1 = fn4()
		}
		_ = stu4_1
	})
	b.Run("fn4-2", func(b *testing.B) {
		var stu4_2 *Student
		for i := 0; i < b.N; i++ {
			stu4_2 = fn4()
		}
		_ = stu4_2
	})
	b.Run("fn5", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_ = fn5()
		}
	})
}
