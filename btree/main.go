package main

import (
	"fmt"
	"math/rand"

	"github.com/google/btree"
)

// perm returns a random permutation of n Int items in the range [0, n).
func perm(n int) (out []btree.Item) {
	for _, v := range rand.Perm(n) {
		out = append(out, btree.Int(v))
	}
	return
}

func main() {
	treeSize := 10000
	tr := btree.New(2)

	if tr.Len() != 0 {
		panic("invalid length")
	}

	// replace or insert
	for _, item := range perm(treeSize) {
		if x := tr.ReplaceOrInsert(item); x != nil {
			panic("insert found item")
		}
	}
	for _, item := range perm(treeSize) {
		if x := tr.ReplaceOrInsert(item); x == nil {
			panic("insert didn't find item")
		}
	}

	if tr.Len() != treeSize {
		panic("invalid length")
	}

	// min max
	if min, want := tr.Min(), btree.Item(btree.Int(0)); min != want {
		panic(fmt.Sprintf("min: want %+v, got %+v", want, min))
	}
	if max, want := tr.Max(), btree.Item(btree.Int(treeSize-1)); max != want {
		panic(fmt.Sprintf("min: want %+v, got %+v", want, max))
	}

	// ascend
	pre := -1
	tr.Ascend(func(val btree.Item) bool {
		if int(val.(btree.Int)) < pre {
			panic("ascend: val < pre")
		}
		pre = int(val.(btree.Int))
		return true
	})

	pre = treeSize + 1
	// descend
	tr.Descend(func(val btree.Item) bool {
		if int(val.(btree.Int)) > pre {
			panic("descend: val > pre")
		}
		pre = int(val.(btree.Int))
		return true
	})

	// delete
	for _, item := range perm(treeSize) {
		if x := tr.Delete(item); x == nil {
			panic(fmt.Sprintf("didn't find %v", item))
		}
	}

	if tr.Len() != 0 {
		panic("invalid length")
	}
}
