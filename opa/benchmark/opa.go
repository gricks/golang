package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"strings"

	"github.com/open-policy-agent/opa/ast"
	"github.com/open-policy-agent/opa/rego"
	"github.com/open-policy-agent/opa/storage/inmem"
)

func opa_compile(name string) rego.PreparedEvalQuery {
	ctx := context.Background()

	module, err := ast.ParseModule(name, loadfile(name+".rego"))
	if err != nil {
		panic(err)
	}

	compiler := ast.NewCompiler()
	if compiler.Compile(map[string]*ast.Module{name: module}); compiler.Failed() {
		panic(compiler.Errors)
	}

	store := inmem.NewFromReader(strings.NewReader(loadfile(name + ".json")))
	query, err := rego.New(
		rego.Query("data."+name),
		rego.Store(store),
		rego.Compiler(compiler)).PrepareForEval(ctx)
	if err != nil {
		panic(err)
	}
	return query
}

func loadfile(f string) string {
	b, err := ioutil.ReadFile(f)
	if err != nil {
		panic(err)
	}
	return string(b)
}

func main() {
	data := map[string][]map[string]interface{}{}
	for i := 1; i <= 100; i++ {
		data["values"] = append(data["values"], map[string]interface{}{
			"id":    i,
			"stime": 1000 + i,
			"etime": 2000 + i,
			"vtype": func(v int) string {
				if v%2 == 0 {
					return "int"
				} else {
					return "str"
				}
			}(i),
		})
	}

	b, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile("info.json", b, 0644)
	if err != nil {
		panic(err)
	}
}
