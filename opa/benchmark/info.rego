package info

import future.keywords.in

ids[id] {
    some value in data.values
    value.vtype == "int"
    value.etime > 2050
    value.stime > 1050
    id := value.id
}
