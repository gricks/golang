package main

import (
	"context"
	"encoding/json"
	"testing"

	"github.com/open-policy-agent/opa/rego"
)

func BenchmarkOpa(b *testing.B) {
	ctx := context.Background()
	q := opa_compile("info")
	q.Eval(ctx)

	input := map[string]interface{}{}
	_, err := q.Eval(ctx, rego.EvalInput(input))
	if err != nil {
		panic(err)
	}
	//fmt.Println(rst[0].Expressions[0].Value, err)

	var values map[string][]map[string]interface{}
	err = json.Unmarshal([]byte(loadfile("info.json")), &values)
	if err != nil {
		panic(err)
	}

	b.Run("opa", func(tb *testing.B) {
		for i := 0; i < tb.N; i++ {
			q.Eval(ctx, rego.EvalInput(input))
		}
	})

	b.Run("raw", func(tb *testing.B) {
		for i := 0; i < tb.N; i++ {
			ids := make([]int, 0)
			for _, m := range values["values"] {
				if m["vtype"].(string) == "int" &&
					m["etime"].(float64) > 2050 &&
					m["stime"].(float64) > 1050 {
					ids = append(ids, int(m["id"].(float64)))
				}
			}
		}
	})
}
