package main

import (
	"fmt"

	"github.com/klauspost/reedsolomon"
)

func main() {
	data := make([][]byte, 13)
	for k := range data {
		data[k] = []byte(fmt.Sprintf("%02d", k)) // same size
	}

	for _, v := range data {
		fmt.Printf("%02s ", string(v))
	}
	fmt.Println("before")

	enc, err := reedsolomon.New(10, 3)
	if err != nil {
		panic(err)
	}

	if err := enc.Encode(data); err != nil {
		panic(err)
	}

	if ok, err := enc.Verify(data); !ok || err != nil {
		panic(err)
	}

	fmt.Println("reconstruct:")
	{
		// missing packet
		data[1] = nil
		data[2] = nil
		// missing parity packet
		data[11] = nil
		//data[3] = []byte("xx") // cann't reconstruct missing data

		for _, v := range data {
			fmt.Printf("%02s ", string(v))
		}
		fmt.Println("before")

		// rebuild
		if err := enc.Reconstruct(data); err != nil {
			panic(err)
		}

		for _, v := range data {
			fmt.Printf("%02s ", string(v))
		}
		fmt.Println("after")
	}

	fmt.Println("reconstruct data:")
	{
		// missing packet
		data[1] = nil
		data[2] = nil
		// missing parity packet
		data[11] = nil
		//data[3] = []byte("xx") // cann't reconstruct missing data

		for _, v := range data {
			fmt.Printf("%02s ", string(v))
		}
		fmt.Println("before")

		// rebuild; expect parity packet
		if err := enc.ReconstructData(data); err != nil {
			panic(err)
		}

		for _, v := range data {
			fmt.Printf("%02s ", string(v))
		}
		fmt.Println("after")
	}
}
