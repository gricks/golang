package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func GetFilelist(path string, suffix string) ([]string, error) {
	var files []string
	return files, filepath.Walk(path, func(f string, fi os.FileInfo, err error) error {
		if fi == nil || !fi.Mode().IsRegular() {
			return err
		}

		if suffix == "" || strings.HasSuffix(fi.Name(), suffix) {
			files = append(files, f)
		}

		return nil
	})
}

func main() {
	flag.Parse()

	path := flag.Arg(0)
	files, err := GetFilelist(path, ".go")
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		fmt.Printf("%s\n", file)
	}
}
