package main

import (
	"context"
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	"github.com/Shopify/sarama"
)

var (
	topic = "text"
	addrs = []string{"127.0.0.1:9092"}
)

func newClient(applys ...func(*sarama.Config)) sarama.Client {
	conf := sarama.NewConfig()

	version, err := sarama.ParseKafkaVersion("2.8.0")
	if err != nil {
		log.Panicf("Error parsing Kafka version: %v", err)
	}

	conf.Version = version       // kafka 版本
	conf.ClientID = "sarama"     // 客户端名称
	conf.ChannelBufferSize = 256 // 消息缓存队列大小

	// 生产者
	conf.Producer.MaxMessageBytes = 1 * 1024 * 1024       // 单个消息最大长度
	conf.Producer.RequiredAcks = sarama.WaitForLocal      // ACK 级别 - NoResponse, WaitForLocal, WaitForAll
	conf.Producer.Timeout = 10 * time.Second              // ACK 超时时间, WaitForAll 时有效
	conf.Producer.Compression = sarama.CompressionNone    // 压缩算法, 默认不压缩
	conf.Producer.Partitioner = sarama.NewHashPartitioner // 分区算法, 默认 Hash 分区
	conf.Producer.Retry.Max = 3                           // 最大重试次数
	conf.Producer.Return.Successes = true                 // 返回成功消息
	conf.Producer.Flush.Bytes = 0                         // 同步策略: 最大字节数
	conf.Producer.Flush.Messages = 0                      // 同步策略: 最大消息数
	conf.Producer.Flush.Frequency = 0                     // 同步策略: 刷新间隔
	conf.Producer.Flush.MaxMessages = 0                   // 同步策略: 单个请求最大消息数量

	// 消费者
	conf.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategyRoundRobin
	conf.Consumer.Group.Session.Timeout = 10 * time.Second      // 心跳超时时间
	conf.Consumer.Group.Heartbeat.Interval = 3 * time.Second    // 心跳间隔时间
	conf.Consumer.Fetch.Default = 1 * 1024 * 1024               // 单次从 broker 获取数据大小
	conf.Consumer.Offsets.AutoCommit.Enable = true              // 自动 commit 消息
	conf.Consumer.Offsets.AutoCommit.Interval = 1 * time.Second // 自动 commit 消息
	conf.Consumer.Offsets.Initial = sarama.OffsetNewest         // ConsumerGroup 新创建时的 Offset

	for _, apply := range applys {
		apply(conf)
	}

	c, err := sarama.NewClient(addrs, conf)
	if err != nil {
		panic(err)
	}
	return c
}

func syncProducer() sarama.SyncProducer {
	p, err := sarama.NewSyncProducerFromClient(newClient())
	if err != nil {
		panic(err)
	}
	return p
}

func asyncProducer() sarama.AsyncProducer {
	p, err := sarama.NewAsyncProducerFromClient(newClient())
	if err != nil {
		panic(err)
	}
	go func() {
		for {
			select {
			case err := <-p.Errors():
				log.Println("Failed to write log entry:", err)
			case msg := <-p.Successes():
				key, _ := msg.Key.Encode()
				val, _ := msg.Value.Encode()
				log.Printf("Success to write log entry: key(%v) val(%v)\n", string(key), string(val))
			}
		}
	}()
	return p
}

func consumer(group string, num int, applys ...func(*sarama.Config)) func() {
	var wg sync.WaitGroup
	ctx, cancel := context.WithCancel(context.Background())
	for index := 0; index < num; index++ {
		cgroup, err := sarama.NewConsumerGroupFromClient(group, newClient(applys...))
		if err != nil {
			panic(err)
		}
		c := &Consumer{
			group: group,
			index: index,
			ready: make(chan bool),
		}
		wg.Add(1)
		pcall := func(cgroup sarama.ConsumerGroup, c *Consumer) {
			defer wg.Done()
			for {
				fmt.Println("begin consume, index:", c.index)
				err := cgroup.Consume(ctx, strings.Split(topic, ","), c)
				if err != nil {
					log.Printf("Error from consumer: %v", err)
				} else if err := ctx.Err(); err != nil {
					log.Printf("cancel consumer: %v", err)
					break
				}
				fmt.Println("closed consumer index:", c.index)
				time.Sleep(time.Second)
				c.ready = make(chan bool)
			}
			cgroup.Close()
		}
		go pcall(cgroup, c)
		// wait till the consumer has been set up
		<-c.Ready()
	}
	return func() {
		time.Sleep(time.Second) // wait for read complete
		cancel()
		wg.Wait()
	}
}

// Consumer represents a Sarama consumer group consumer
type Consumer struct {
	index int
	group string
	ready chan bool
}

// Setup is run at the beginning of a new session, before ConsumeClaim
func (c *Consumer) Setup(sarama.ConsumerGroupSession) error {
	close(c.ready)
	return nil
}

// Cleanup is run at the end of a session, once all ConsumeClaim goroutines have exited
func (c *Consumer) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

// ConsumeClaim must start a consumer loop of ConsumerGroupClaim's Messages().
func (c *Consumer) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for message := range claim.Messages() {

		log.Printf("Message claimed: group = %s, index = %d, topic = %s, timestamp = %v, value = %s",
			c.group, c.index, message.Topic, message.Timestamp.Format(time.RFC3339), string(message.Value))

		session.MarkMessage(message, "") // AutoCommit
	}
	return nil
}

func (c *Consumer) Ready() <-chan bool {
	return c.ready
}

func main() {
	group1 := consumer("group1", 1)
	fmt.Println("group1 ready")

	p := syncProducer()
	for i := 0; i < 10; i++ {
		if _, _, err := p.SendMessage(&sarama.ProducerMessage{
			Topic: topic,
			Key:   sarama.StringEncoder(fmt.Sprintf("Message Key: %d", i)),
			Value: sarama.StringEncoder(fmt.Sprintf("Message Value: %d", i)),
		}); err != nil {
			panic(err)
		}
		time.Sleep(500 * time.Millisecond)
	}

	group1()
}
