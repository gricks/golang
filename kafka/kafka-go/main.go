package main

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/segmentio/kafka-go"
)

var (
	topic   = "topic-A"
	group   = "consumer-group"
	brokers = []string{"localhost:9092"}
)

func main() {
	initialize()

	// make a new reader that consumes from topic-A
	r1 := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  brokers,
		GroupID:  group,
		Topic:    topic,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})

	fmt.Println(r1)

	// make a new reader that consumes from topic-A
	r2 := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  brokers,
		GroupID:  group,
		Topic:    topic,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})

	// make a new reader that consumes from topic-A
	r3 := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  brokers,
		GroupID:  group,
		Topic:    topic,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})

	go func() {
		for {
			m, err := r1.ReadMessage(context.Background())
			if err != nil {
				break
			}
			fmt.Printf("111 message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))
		}

		if err := r1.Close(); err != nil {
			log.Fatal("failed to close reader:", err)
		}
	}()

	go func() {
		for {
			m, err := r2.ReadMessage(context.Background())
			if err != nil {
				break
			}
			fmt.Printf("222 message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))
		}

		if err := r2.Close(); err != nil {
			log.Fatal("failed to close reader:", err)
		}

	}()

	go func() {
		for {
			m, err := r3.ReadMessage(context.Background())
			if err != nil {
				break
			}
			fmt.Printf("333 message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))
		}

		if err := r3.Close(); err != nil {
			log.Fatal("failed to close reader:", err)
		}

	}()
	select {}
}

func initialize() {
	createTopic()
	createMessage()
}

func createTopic() {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	conn, err := kafka.DialContext(ctx, "tcp", "localhost:9092")
	if err != nil {
		panic(err.Error())
	}
	defer conn.Close()

	topicConfigs := []kafka.TopicConfig{
		{
			Topic:             topic,
			NumPartitions:     2,
			ReplicationFactor: 1,
		},
	}

	err = conn.CreateTopics(topicConfigs...)
	if err != nil {
		panic(err.Error())
	}
}

func createMessage() {
	w := kafka.NewWriter(kafka.WriterConfig{
		Brokers:   brokers,
		Topic:     topic,
		Balancer:  &kafka.LeastBytes{},
		BatchSize: 1,
	})

	for i := 0; i < 10; i++ {
		a := strconv.Itoa(i)
		err := w.WriteMessages(context.Background(),
			kafka.Message{
				Key:   []byte(a),
				Value: []byte(a),
			},
		)
		if err != nil {
			panic(err)
		}
	}
	err := w.Close()
	if err != nil {
		panic(err)
	}
}
