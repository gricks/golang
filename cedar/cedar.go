package cedar

import (
	"errors"
	"reflect"
	"unsafe"
)

const valueGuard = int(^uint(0) >> 1)

var (
	ErrInvalidValue = errors.New("cedar: invalid value")
	ErrInvalidKey   = errors.New("cedar: invalid key")
	ErrNoPath       = errors.New("cedar: no path")
	ErrNoValue      = errors.New("cedar: no value")
)

// Cedar infomation
type Cedar struct {
	array      []node
	infos      []info
	blocks     []block
	reject     [257]int
	full, open int
	closed     int
	capacity   int
	size       int
	trial      int
}

// New return cedar
func New() *Cedar {
	da := new(Cedar)
	da.init()
	return da
}

// Key returns the key of the node with the given `id`.
// It will return ErrNoPath, if the node does not exist.
func (da *Cedar) Key(id int) (string, error) {
	key := make([]byte, 0, 16)
	for id > 0 && id < len(da.array) {
		from := da.array[id].check
		if from < 0 {
			return "", ErrNoPath
		}
		if char := byte(da.array[from].base() ^ id); char != 0 {
			key = append(key, char)
		}
		id = from
	}
	if id != 0 || len(key) == 0 {
		return "", ErrInvalidKey
	}
	for i := 0; i < len(key)/2; i++ {
		key[i], key[len(key)-i-1] =
			key[len(key)-i-1], key[i]
	}
	return bytes2str(key), nil
}

// Value returns the value of the node with the given `id`.
// It will return ErrNoValue, if the node does not have a value.
func (da *Cedar) Value(id int) (value int, err error) {
	value = da.array[id].value
	if value >= 0 {
		return value, nil
	}
	to := da.array[id].base()
	if da.array[to].check == id && da.array[to].value >= 0 {
		return da.array[to].value, nil
	}
	return 0, ErrNoValue
}

// Jump travels from a node `from` to another node `to` by following the path `path`.
// For example, if the following keys were inserted:
//	id	key
//	19	abc
//	23	ab
//	37	abcd
// then
//	Jump("ab", 0)  = 23, nil		// reach "ab" from root
//	Jump("c", 23)  = 19, nil		// reach "abc" from "ab"
//	Jump("cd", 23) = 37, nil		// reach "abcd" from "ab"
func (da *Cedar) Jump(path []byte, from int) (to int, err error) {
	for _, b := range path {
		if da.array[from].value >= 0 {
			return from, ErrNoPath
		}
		to = da.array[from].base() ^ int(b)
		if da.array[to].check != from {
			return from, ErrNoPath
		}
		from = to
	}
	return to, nil
}

// Exist returns the value of the node with the given `id`.
// It will return ErrNoValue, if the node does not have a value.
func (da *Cedar) Exist(id int) bool {
	value := da.array[id].value
	if value >= 0 {
		return true
	}
	to := da.array[id].base()
	if da.array[to].check == id && da.array[to].value >= 0 {
		return true
	}
	return false
}

// Insert adds a key-value pair into the cedar.
// It will return ErrInvalidValue, if value < 0 or >= valueGuard.
func (da *Cedar) Insert(key string, value int) error {
	if value < 0 || value >= valueGuard {
		return ErrInvalidValue
	}
	from, sbyte := 0, str2bytes(key)
	for _, b := range sbyte {
		value := da.array[from].value
		if value >= 0 && value != valueGuard {
			to := da.follow(from, 0)
			da.array[to].value = value
		}
		from = da.follow(from, b)
	}
	to := from
	if da.array[from].value < 0 {
		to = da.follow(from, 0)
	}
	da.array[to].value = value
	return nil
}

// Delete removes a key-value pair from the cedar.
func (da *Cedar) Delete(key string) {
	// if the path does not exist or the end is not a leaf
	from, err := da.Jump(str2bytes(key), 0)
	if err != nil || !da.Exist(from) {
		return
	}
	e := from
	if da.array[from].value < 0 {
		e = da.array[from].base() ^ 0
	}
	from = da.array[e].check
	var flag bool
	for !flag {
		n := da.array[from]
		flag = da.infos[n.base()^int(da.infos[from].child)].sibling != 0
		if flag {
			da.popSibling(from, n.base(), byte(n.base()^e))
		}
		da.pushEnode(e)
		e = from
		from = da.array[from].check
	}
	return
}

// PrefixMatch returns a list of at most `num` nodes which match the prefix of the key.
// If `num` is 0, it returns all matches.
// For example, if the following keys were inserted:
//	id	key
//	19	abc
//	23	ab
//	37	abcd
// then
//	PrefixMatch("abc", 1)  = [ 23 ]				// match ["ab"]
//	PrefixMatch("abcd", 0) = [ 23, 19, 37 ]		// match ["ab", "abc", "abcd"]
func (da *Cedar) PrefixMatch(key string, num int) (ids []int) {
	sbyte := str2bytes(key)
	for from, i := 0, 0; i < len(sbyte); i++ {
		to, err := da.Jump(sbyte[i:i+1], from)
		if err != nil {
			break
		}
		if da.Exist(to) {
			ids = append(ids, to)
			num--
			if num == 0 {
				return
			}
		}
		from = to
	}
	return
}

// PrefixPredict returns a list of at most `num` nodes which has the key as their prefix.
// These nodes are ordered by their keys.
// If `num` is 0, it returns all matches.
// For example, if the following keys were inserted:
//	id	key
//	19	abc
//	23	ab
//	37	abcd
// then
//	PrefixPredict("ab", 2) = [ 23, 19 ]			// predict ["ab", "abc"]
//	PrefixPredict("ab", 0) = [ 23, 19, 37 ]		// predict ["ab", "abc", "abcd"]
func (da *Cedar) PrefixPredict(key string, num int) (ids []int) {
	root, err := da.Jump(str2bytes(key), 0)
	if err != nil {
		return
	}
	for from, err := da.begin(root); err == nil; from, err = da.next(from, root) {
		ids = append(ids, from)
		num--
		if num == 0 {
			return
		}
	}
	return
}

// initlize cedar trie
func (da *Cedar) init() {
	da.capacity = 256
	da.size = 256
	da.trial = 1
	da.array = make([]node, 256)
	for i := 1; i < 256; i++ {
		da.array[i] = node{-(i - 1), -(i + 1)}
	}
	da.array[0] = node{-1, -1}
	da.array[1].value = -255
	da.array[255].check = -1
	da.infos = make([]info, 256)
	da.blocks = make([]block, 1)
	da.blocks[0].head = 1
	da.blocks[0].init()
	for i := 0; i <= 256; i++ {
		da.reject[i] = i + 1
	}
}

type node struct {
	value, check int
}

func (n *node) base() int {
	return -(n.value + 1)
}

type info struct {
	sibling, child byte
}

type block struct {
	prev   int
	next   int
	num    int
	reject int
	trial  int
	head   int
}

func (b *block) init() {
	b.num, b.reject = 256, 257
}

// follow or create trie edge
func (da *Cedar) follow(from int, label byte) int {
	base := da.array[from].base()
	to := base ^ int(label)
	if base < 0 || da.array[to].check < 0 {
		to = da.popEnode(base, label, from)
		da.pushSibling(from, to^int(label), label, base >= 0)
	} else if da.array[to].check != from {
		to = da.resolve(from, base, label)
	}
	return to
}

// pop block from trie
func (da *Cedar) popBlock(bi int, in *int, last bool) {
	if last {
		*in = 0 // last one poped
	} else {
		b := &da.blocks[bi]
		da.blocks[b.prev].next = b.next
		da.blocks[b.next].prev = b.prev
		if bi == *in {
			*in = b.next
		}
	}
}

// push block into trie
func (da *Cedar) pushBlock(bi int, out *int, empty bool) {
	b := &da.blocks[bi]
	if empty {
		*out, b.prev, b.next = bi, bi, bi
	} else {
		tail := &da.blocks[*out].prev
		b.prev = *tail
		b.next = *out
		*out, *tail, da.blocks[*tail].next = bi, bi, bi
	}
}

func (da *Cedar) addBlock() int {
	if da.size == da.capacity {
		da.capacity *= 2
		_array := da.array
		da.array = make([]node, da.capacity)
		copy(da.array, _array)
		_infos := da.infos
		da.infos = make([]info, da.capacity)
		copy(da.infos, _infos)
		_blocks := da.blocks
		da.blocks = make([]block, da.capacity>>8)
		copy(da.blocks, _blocks)
	}
	da.blocks[da.size>>8].init()
	da.blocks[da.size>>8].head = da.size
	da.array[da.size] = node{-(da.size + 255), -(da.size + 1)}
	for i := da.size + 1; i < da.size+255; i++ {
		da.array[i] = node{-(i - 1), -(i + 1)}
	}
	da.array[da.size+255] = node{-(da.size + 254), -da.size}
	da.pushBlock(da.size>>8, &da.open, da.open == 0)
	da.size += 256
	return da.size>>8 - 1
}

func (da *Cedar) transferBlock(bi int, in, out *int) {
	da.popBlock(bi, in, bi == da.blocks[bi].next)
	da.pushBlock(bi, out, *out == 0 && da.blocks[bi].num != 0)
}

func (da *Cedar) popEnode(base int, label byte, from int) int {
	e := base ^ int(label)
	if base < 0 {
		e = da.findPlace()
	}
	bi := e >> 8
	n := &da.array[e]
	b := &da.blocks[bi]
	b.num--
	if b.num == 0 {
		if bi != 0 {
			da.transferBlock(bi, &da.closed, &da.full)
		}
	} else {
		da.array[-n.value].check = n.check
		da.array[-n.check].value = n.value
		if e == b.head {
			b.head = -n.check
		}
		if bi != 0 && b.num == 1 && b.trial != da.trial {
			da.transferBlock(bi, &da.open, &da.closed)
		}
	}
	n.value = valueGuard
	n.check = from
	if base < 0 {
		da.array[from].value = -(e ^ int(label)) - 1
	}
	return e
}

func (da *Cedar) pushEnode(e int) {
	bi := e >> 8
	b := &da.blocks[bi]
	b.num++
	if b.num == 1 {
		b.head = e
		da.array[e] = node{-e, -e}
		if bi != 0 {
			da.transferBlock(bi, &da.full, &da.closed)
		}
	} else {
		prev := b.head
		next := -da.array[prev].check
		da.array[e] = node{-prev, -next}
		da.array[prev].check = -e
		da.array[next].value = -e
		if b.num == 2 || b.trial == da.trial {
			if bi != 0 {
				da.transferBlock(bi, &da.closed, &da.open)
			}
		}
		b.trial = 0
	}
	if b.reject < da.reject[b.num] {
		b.reject = da.reject[b.num]
	}
	da.infos[e] = info{}
}

func (da *Cedar) pushSibling(from, base int, label byte, hasChild bool) {
	c := &da.infos[from].child
	keepOrder := label > *c
	if hasChild && keepOrder {
		c = &da.infos[base^int(*c)].sibling
		for *c != 0 && *c < label {
			c = &da.infos[base^int(*c)].sibling
		}
	}
	da.infos[base^int(label)].sibling = *c
	*c = label
}

func (da *Cedar) popSibling(from, base int, label byte) {
	c := &da.infos[from].child
	for *c != label {
		c = &da.infos[base^int(*c)].sibling
	}
	*c = da.infos[base^int(*c)].sibling
}

func (da *Cedar) consult(base_n, base_p int, c_n, c_p byte) bool {
	c_n = da.infos[base_n^int(c_n)].sibling
	c_p = da.infos[base_p^int(c_p)].sibling
	for c_n != 0 && c_p != 0 {
		c_n = da.infos[base_n^int(c_n)].sibling
		c_p = da.infos[base_p^int(c_p)].sibling
	}
	return c_p != 0
}

func (da *Cedar) setChild(base int, c byte, label byte, flag bool) []byte {
	child := make([]byte, 0, 257)
	if c == 0 {
		child = append(child, c)
		c = da.infos[base^int(c)].sibling
	}
	for c != 0 && c <= label {
		child = append(child, c)
		c = da.infos[base^int(c)].sibling
	}
	if flag {
		child = append(child, label)
	}
	for c != 0 {
		child = append(child, c)
		c = da.infos[base^int(c)].sibling
	}
	return child
}

func (da *Cedar) findPlace() int {
	if da.closed != 0 {
		return da.blocks[da.closed].head
	}
	if da.open != 0 {
		return da.blocks[da.open].head
	}
	return da.addBlock() << 8
}

func (da *Cedar) findPlaces(child []byte) int {
	bi := da.open
	if bi != 0 {
		bz := da.blocks[da.open].prev
		nc := len(child)
		for {
			b := &da.blocks[bi]
			if b.num >= nc && nc < b.reject {
				for e := b.head; ; {
					base := e ^ int(child[0])
					for i := 0; da.array[base^int(child[i])].check < 0; i++ {
						if i == len(child)-1 {
							b.head = e
							return e
						}
					}
					e = -da.array[e].check
					if e == b.head {
						break
					}
				}
			}
			b.reject = nc
			if b.reject < da.reject[b.num] {
				da.reject[b.num] = b.reject
			}
			bi_ := b.next
			b.trial++
			if b.trial == da.trial {
				da.transferBlock(bi, &da.open, &da.closed)
			}
			if bi == bz {
				break
			}
			bi = bi_
		}
	}
	return da.addBlock() << 8
}

func (da *Cedar) resolve(from_n, base_n int, label_n byte) int {
	to_pn := base_n ^ int(label_n)
	from_p := da.array[to_pn].check
	base_p := da.array[from_p].base()

	flag := da.consult(base_n, base_p, da.infos[from_n].child, da.infos[from_p].child)
	var children []byte
	if flag {
		children = da.setChild(base_n, da.infos[from_n].child, label_n, true)
	} else {
		children = da.setChild(base_p, da.infos[from_p].child, 255, false)
	}
	var base int
	if len(children) == 1 {
		base = da.findPlace()
	} else {
		base = da.findPlaces(children)
	}
	base ^= int(children[0])
	var from int
	var base_ int
	if flag {
		from = from_n
		base_ = base_n
	} else {
		from = from_p
		base_ = base_p
	}
	if flag && children[0] == label_n {
		da.infos[from].child = label_n
	}
	da.array[from].value = -base - 1
	for i := 0; i < len(children); i++ {
		to := da.popEnode(base, children[i], from)
		to_ := base_ ^ int(children[i])
		if i == len(children)-1 {
			da.infos[to].sibling = 0
		} else {
			da.infos[to].sibling = children[i+1]
		}
		if flag && to_ == to_pn { // new node has no child
			continue
		}
		n := &da.array[to]
		n_ := &da.array[to_]
		n.value = n_.value
		if n.value < 0 && children[i] != 0 {
			// this node has children, fix their check
			c := da.infos[to_].child
			da.infos[to].child = c
			da.array[n.base()^int(c)].check = to
			c = da.infos[n.base()^int(c)].sibling
			for c != 0 {
				da.array[n.base()^int(c)].check = to
				c = da.infos[n.base()^int(c)].sibling
			}
		}
		if !flag && to_ == from_n { // parent node moved
			from_n = to
		}
		if !flag && to_ == to_pn {
			da.pushSibling(from_n, to_pn^int(label_n), label_n, true)
			da.infos[to_].child = 0
			n_.value = valueGuard
			n_.check = from_n
		} else {
			da.pushEnode(to_)
		}
	}
	if flag {
		return base ^ int(label_n)
	}
	return to_pn
}

func (da *Cedar) begin(from int) (to int, err error) {
	for c := da.infos[from].child; c != 0; {
		to = da.array[from].base() ^ int(c)
		c = da.infos[to].child
		from = to
	}
	if da.array[from].base() > 0 {
		return da.array[from].base(), nil
	}
	return from, nil
}

func (da *Cedar) next(from int, root int) (to int, err error) {
	c := da.infos[from].sibling
	for c == 0 && from != root && da.array[from].check >= 0 {
		from = da.array[from].check
		c = da.infos[from].sibling
	}
	if from == root {
		return 0, ErrNoPath
	}
	from = da.array[da.array[from].check].base() ^ int(c)
	return da.begin(from)
}

func bytes2str(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

func str2bytes(s string) []byte {
	p := unsafe.Pointer((*reflect.StringHeader)(unsafe.Pointer(&s)).Data)
	var b []byte
	hdr := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	hdr.Data = uintptr(p)
	hdr.Cap = len(s)
	hdr.Len = len(s)
	return b
}
