module cedar

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/google/gofuzz v1.2.0
	github.com/smartystreets/goconvey v1.7.2
)
