# cedar

Package `cedar` implementes double-array trie.

It is a [Golang](https://golang.org/) port of [cedar](http://www.tkl.iis.u-tokyo.ac.jp/~ynaga/cedar) which is written in C++ by Naoki Yoshinaga. `cedar` currently implements the `reduced` verion of cedar. 
