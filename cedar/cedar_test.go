package cedar

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"sort"
	"strings"
	"testing"
	"time"

	fuzz "github.com/google/gofuzz"

	. "github.com/smartystreets/goconvey/convey"
)

func TestCedar(t *testing.T) {
	Convey("Cedar", t, func() {
		trie := New()
		// Insert key-value pairs.
		trie.Insert("How many loved your moments of glad grace", 3)
		trie.Insert("How many loved your moments", 2)
		trie.Insert("How many loved", 1)
		trie.Insert("How many", 0)

		trie.Insert("姑苏", 0)
		trie.Insert("姑苏城外", 1)
		trie.Insert("姑苏城外寒山寺", 2)

		fmt.Println("\nPrefixMatch\nid\tkey:", "How many loved your moments of glad grace")
		for _, id := range trie.PrefixMatch("How many loved your moments of glad grace", 0) {
			key, _ := trie.Key(id)
			value, _ := trie.Value(id)
			fmt.Printf("%d\t%d:%s\n", id, value, key)
		}

		fmt.Println("\nPrefixPredict\nid\tkey:", "姑苏")
		for _, id := range trie.PrefixPredict("姑苏", 0) {
			key, _ := trie.Key(id)
			value, _ := trie.Value(id)
			fmt.Printf("%d\t%d:%s\n", id, value, key)
		}

		// key no exist
		var err error
		_, err = trie.Key(10000)
		So(err, ShouldEqual, ErrInvalidKey)
		_, err = trie.Key(10)
		So(err, ShouldEqual, ErrNoPath)
		_, err = trie.Value(10)
		So(err, ShouldEqual, ErrNoValue)

		_, err = trie.Jump([]byte("xx"), 0)
		So(err, ShouldEqual, ErrNoPath)
		_, err = trie.Jump([]byte("xx"), 10)
		So(err, ShouldEqual, ErrNoPath)

		err = trie.Insert("x", -1)
		So(err, ShouldEqual, ErrInvalidValue)

		trie.Delete("xx") // delete not exist
	})
}

func TestCedarFuzz(t *testing.T) {
	Convey("Cedar fuzz", t, func() {
		seed := time.Now().UnixNano()
		t.Log("seed:", seed)
		fuzzer := fuzz.NewWithSeed(seed).
			Funcs(func(v *string, c fuzz.Continue) {
				for {
					str := c.RandString()
					if str != "" {
						*v = str
						return
					}
				}
			})

		trie := New()
		for i := 0; i < 100000; i++ {
			for _, n := range []int{1, 2, 5, 10, 50} {
				strs := make([]string, 0)
				for k := 0; k < n; k++ {
					var str string
					fuzzer.Fuzz(&str)
					strs = append(strs, str)
					// insert
					err := trie.Insert(str, i)
					if err != nil {
						t.Errorf("str: %s", str)
						return
					}

					var ids []int
					fword := string([]rune(str)[0])
					rpredict, rmatches := rawStringMatch(strs, str, fword)
					rpredict = rawStringUnique(rpredict)
					rmatches = rawStringUnique(rmatches)
					contains := func(strs []string, str string) bool {
						for _, vstr := range strs {
							if vstr == str {
								return true
							}
						}
						return false
					}

					// prefix predict
					ids = trie.PrefixPredict(fword, 0)
					if len(ids) < 0 || len(ids) != len(rpredict) {
						t.Errorf("str:(%s) fword:(%v) rpredict(%v)", str, []byte(fword), rpredict)
						return
					}
					for _, id := range ids {
						pstr, err := trie.Key(id)
						if err != nil || !contains(rpredict, pstr) {
							t.Errorf("str: (%s) (%s)", str, pstr)
						}
					}

					// prefix matches
					ids = trie.PrefixMatch(str, 0)
					if len(ids) < 0 || len(ids) != len(rmatches) {
						t.Errorf("str:(%s) strs:(%v) rmatches(%v)", str, strs, rmatches)
						return
					}
					for _, id := range ids {
						mstr, err := trie.Key(id)
						if err != nil || !contains(rmatches, mstr) {
							t.Errorf("str: (%s) (%s)", str, mstr)
							return
						}
					}
				}

				// cleanup
				for _, str := range strs {
					trie.Delete(str)
				}

				// cleanup twice
				for _, str := range strs {
					trie.Delete(str)
				}
			}
		}
	})
}

func TestCedarDict(t *testing.T) {
	Convey("Cedar", t, func() {
		f, err := os.Open("./testdata/dict.txt")
		So(err, ShouldBeNil)
		defer f.Close()

		terms := make([]string, 0, 1024)
		scanner := bufio.NewScanner(f)
		for scanner.Scan() {
			terms = append(terms,
				scanner.Text())
		}

		trie := New()

		// insert
		for idx, term := range terms {
			trie.Insert(term, idx)
		}

		// reinsert
		for idx := 1; idx < len(terms); idx = idx + 2 {
			trie.Delete(terms[idx])
			trie.Insert(terms[idx], idx)
		}

		// find
		for i := 0; i < 100; i++ {
			index := rand.Intn(len(terms))
			rterm := terms[index]
			fword := string([]rune(rterm)[0])
			rpredict, rmatches := rawStringMatch(terms, rterm, fword)
			spredict, smatches := make([]string, 0), make([]string, 0)
			for _, id := range trie.PrefixPredict(fword, 0) {
				k, err := trie.Key(id)
				if err != nil {
					panic(err)
				}
				spredict = append(spredict, string(k))
			}
			sort.Strings(spredict)

			for _, id := range trie.PrefixMatch(rterm, 0) {
				k, err := trie.Key(id)
				if err != nil {
					panic(err)
				}
				smatches = append(smatches, string(k))
			}
			sort.Strings(smatches)

			So(spredict, ShouldResemble, rpredict)
			So(smatches, ShouldResemble, rmatches)
		}
	})
}

func rawStringMatch(terms []string, rterm string, fword string) ([]string, []string) {
	predict, matches := make([]string, 0), make([]string, 0)
	for _, term := range terms {
		if strings.HasPrefix(term, fword) {
			predict = append(predict, term)
		}
		if strings.HasPrefix(rterm, term) {
			matches = append(matches, term)
		}
	}
	sort.Strings(predict)
	sort.Strings(matches)
	return predict, matches
}

func rawStringUnique(terms []string) []string {
	uterms := make([]string, 0, len(terms))
	for _, term := range terms {
		var flag bool
		for _, uterm := range uterms {
			if term == uterm {
				flag = true
				break
			}
		}
		if !flag {
			uterms = append(uterms, term)
		}
	}
	return uterms
}

func BenchmarkCedar(b *testing.B) {
	f, err := os.Open("./testdata/dict.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	trie := New()
	scanner := bufio.NewScanner(f)
	for i := 0; scanner.Scan(); i++ {
		trie.Insert(scanner.Text(), i)
	}

	b.Run("Insert", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			err := trie.Insert("姑苏城外寒山寺上", 0)
			if err != nil {
				panic(err)
			}
		}
	})

	b.Run("Delete", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			trie.Delete("姑苏城外寒山寺上")
		}
	})

	b.Run("PrefixMatch", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			ids := trie.PrefixMatch("姑苏城外寒山寺", 10)
			if len(ids) == 0 {
				panic(ids)
			}
			for _, id := range ids {
				_, err := trie.Key(id)
				if err != nil {
					panic(err)
				}
			}
		}
	})

	b.Run("PrefixPredict", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			ids := trie.PrefixPredict("军", 10)
			if len(ids) == 0 {
				panic(ids)
			}
			for _, id := range ids {
				_, err := trie.Key(id)
				if err != nil {
					panic(err)
				}
			}
		}
	})
}
