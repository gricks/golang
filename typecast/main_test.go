package main

import "testing"

func BenchmarkTypecast(b *testing.B) {
	a := []int32{}
	for i := int32(0); i < 10; i++ {
		a = append(a, i)
	}

	b.Run("Typecast", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			Int32Slice(a).Exist(1)
			Int32Slice(a).Exist(12)
		}
	})

	b.Run("Without Typecast", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			Exist(a, 1)
			Exist(a, 12)
		}
	})
}
