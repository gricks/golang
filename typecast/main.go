package main

import "fmt"

type Int32Slice []int32

func (this Int32Slice) Exist(v int32) bool {
	for _, val := range this {
		if val == v {
			return true
		}
	}
	return false
}

func Exist(a []int32, v int32) bool {
	for _, val := range a {
		if val == v {
			return true
		}
	}
	return false
}

func main() {
	a := []int32{}
	for i := int32(0); i < 10; i++ {
		a = append(a, i)
	}

	fmt.Println(Int32Slice(a).Exist(1))
	fmt.Println(Int32Slice(a).Exist(12))
	fmt.Println(Exist(a, 1))
	fmt.Println(Exist(a, 12))
}
