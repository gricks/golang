package main

import (
	"context"
	"time"

	"github.com/go-kit/kit/log"
)

type loggingMiddleware struct {
	log.Logger

	next StringService
}

func (this loggingMiddleware) Uppercase(ctx context.Context, s string) (o string, err error) {
	defer func(begin time.Time) {
		this.Log(
			"method", "uppercase",
			"in", s,
			"out", o,
			"err", err,
			"elapse", time.Since(begin),
		)
	}(time.Now())
	o, err = this.next.Uppercase(ctx, s)
	return
}

func (this loggingMiddleware) Lowercase(ctx context.Context, s string) (o string, err error) {
	defer func(begin time.Time) {
		this.Log(
			"method", "lowercase",
			"in", s,
			"out", o,
			"err", err,
			"elapse", time.Since(begin),
		)
	}(time.Now())

	o, err = this.next.Lowercase(ctx, s)
	return
}
