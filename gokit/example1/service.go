package main

import (
	"context"
	"strings"
)

type StringService interface {
	Uppercase(context.Context, string) (string, error)
	Lowercase(context.Context, string) (string, error)
}

type stringService struct{}

func (this *stringService) Uppercase(ctx context.Context, s string) (string, error) {
	return strings.ToUpper(s), nil
}

func (this *stringService) Lowercase(ctx context.Context, s string) (string, error) {
	return strings.ToLower(s), nil
}
