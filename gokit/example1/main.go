package main

import (
	"net/http"
	"os"

	"github.com/go-kit/kit/log"
	transport "github.com/go-kit/kit/transport/http"
)

func main() {
	logger := log.NewLogfmtLogger(os.Stderr)

	var svc StringService
	svc = &stringService{}
	svc = loggingMiddleware{logger, svc}

	uppercaseHandle := transport.NewServer(
		makeUppercaseEndpoint(svc),
		decodeRequest, encodeResponse,
	)

	lowercaseHandle := transport.NewServer(
		makeLowercaseEndpoint(svc),
		decodeRequest, encodeResponse,
	)

	http.Handle("/uppercase", uppercaseHandle)
	http.Handle("/lowercase", lowercaseHandle)

	logger.Log(http.ListenAndServe(":39111", nil))
}
