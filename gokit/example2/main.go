package main

import (
	"net"
	"os"

	"google.golang.org/grpc"

	"github.com/go-kit/kit/log"
)

func main() {
	logger := log.NewLogfmtLogger(os.Stderr)

	var svc StringService
	svc = &stringService{}
	svc = loggingMiddleware{logger, svc}

	lis, err := net.Listen("tcp", ":39000")
	if err != nil {
		panic(err)
	}

	s := grpc.NewServer()
	RegisterGRPCStringServer(s, NewGRPCStringService(
		makeUppercaseEndpoint(svc),
		makeLowercaseEndpoint(svc),
	))

	logger.Log(s.Serve(lis))
}
