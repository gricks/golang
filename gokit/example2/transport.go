package main

import (
	"context"

	"github.com/go-kit/kit/endpoint"
)

////////////////////////////////////////////////////////////////////
/////////////// Request & Response

type request struct {
	S string `json:"s"`
}

type response struct {
	S   string `json:"s"`
	Err string `json:"err,omitempty"`
}

////////////////////////////////////////////////////////////////////
/////////////// Decode & Encode

func decodeRequest(ctx context.Context, r interface{}) (interface{}, error) {
	req := r.(*StringRequest)
	return &request{
		S: req.S,
	}, nil
}

func encodeResponse(ctx context.Context, r interface{}) (interface{}, error) {
	rsp := r.(*response)
	return &StringResponse{
		S: rsp.S,
	}, nil
}

////////////////////////////////////////////////////////////////////
/////////////// Endpoint

// An endpoint represents a single RPC

func makeUppercaseEndpoint(svc StringService) endpoint.Endpoint {
	return func(ctx context.Context, r interface{}) (interface{}, error) {
		req := r.(*request)
		s, err := svc.Uppercase(ctx, req.S)
		if err != nil {
			return &response{s, err.Error()}, nil
		}
		return &response{s, ""}, nil
	}
}

func makeLowercaseEndpoint(svc StringService) endpoint.Endpoint {
	return func(ctx context.Context, r interface{}) (interface{}, error) {
		req := r.(*request)
		s, err := svc.Lowercase(ctx, req.S)
		if err != nil {
			return &response{s, err.Error()}, nil
		}
		return &response{s, ""}, nil
	}
}
