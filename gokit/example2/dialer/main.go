package main

import (
	"context"
	"log"

	"google.golang.org/grpc"
)

const host = "localhost:39000"

func main() {
	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	c := NewGRPCStringClient(conn)
	// Uppercase
	{
		req := &StringRequest{
			S: "Hey, Man!",
		}
		log.Println("Send:", req)
		r, err := c.Uppercase(context.Background(), req)
		if err != nil {
			log.Fatal(err)
		}
		log.Println("Recv:", r)
	}

	//Lowercase
	{
		req := &StringRequest{
			S: "Hey, Man!",
		}
		log.Println("Send:", req)
		r, err := c.Lowercase(context.Background(), req)
		if err != nil {
			log.Fatal(err)
		}
		log.Println("Recv:", r)
	}
}
