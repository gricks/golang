package main_test

import (
	"context"
	"io"
	"log/slog"
	"testing"
)

func BenchmarkSlog(b *testing.B) {
	ctx := context.Background()
	b.Run("slog.Text.Info", func(b *testing.B) {
		logger := slog.New(slog.NewTextHandler(io.Discard, nil))
		for i := 0; i < b.N; i++ {
			logger.Info("ok",
				slog.Float64("key1", 1.0),
				slog.String("key2", "eee=ccc=aaaaaaaaaaaaaaaaaaaaaa"),
			)
		}
	})

	b.Run("slog.JSON.Info", func(b *testing.B) {
		logger := slog.New(slog.NewJSONHandler(io.Discard, nil))
		for i := 0; i < b.N; i++ {
			logger.Info("ok",
				slog.Float64("key1", 1.0),
				slog.String("key2", "eee=ccc=aaaaaaaaaaaaaaaaaaaaaa"),
			)
		}
	})

	b.Run("slog.Text.LogAttrs", func(b *testing.B) {
		logger := slog.New(slog.NewTextHandler(io.Discard, nil))
		for i := 0; i < b.N; i++ {
			logger.LogAttrs(ctx, slog.LevelInfo, "ok",
				slog.Float64("key1", 1.0),
				slog.String("key2", "eee=ccc=aaaaaaaaaaaaaaaaaaaaaa"),
			)
		}
	})

	b.Run("slog.JSON.LogAttrs", func(b *testing.B) {
		logger := slog.New(slog.NewJSONHandler(io.Discard, nil))
		for i := 0; i < b.N; i++ {
			logger.LogAttrs(ctx, slog.LevelInfo, "ok",
				slog.Float64("key1", 1.0),
				slog.String("key2", "eee=ccc=aaaaaaaaaaaaaaaaaaaaaa"),
			)
		}
	})

	b.Run("slog.Text.LogAttrs.Source", func(b *testing.B) {
		logger := slog.New(slog.NewTextHandler(io.Discard,
			&slog.HandlerOptions{
				AddSource: true,
			}))
		for i := 0; i < b.N; i++ {
			logger.LogAttrs(ctx, slog.LevelInfo, "ok",
				slog.Float64("key1", 1.0),
				slog.String("key2", "eee=ccc=aaaaaaaaaaaaaaaaaaaaaa"),
			)
		}
	})

	b.Run("slog.JSON.LogAttrs.Source", func(b *testing.B) {
		logger := slog.New(slog.NewJSONHandler(io.Discard,
			&slog.HandlerOptions{
				AddSource: true,
			}))
		for i := 0; i < b.N; i++ {
			logger.LogAttrs(ctx, slog.LevelInfo, "ok",
				slog.Float64("key1", 1.0),
				slog.String("key2", "eee=ccc=aaaaaaaaaaaaaaaaaaaaaa"),
			)
		}
	})

	b.Run("slog.Text.LogAttrs.Group", func(b *testing.B) {
		logger := slog.New(slog.NewTextHandler(io.Discard,
			&slog.HandlerOptions{
				AddSource: true,
			}))
		for i := 0; i < b.N; i++ {
			logger.LogAttrs(ctx, slog.LevelInfo, "ok",
				slog.String("key1", "value1\naaa\nccc\naaaaaaaaaaaaaaaaaaaaaaaaaa"),
				slog.Group(
					"group1",
					slog.Float64("gkey1", 1.0),
					slog.String("gkey2", "eee=ccc"),
				),
			)
		}
	})

	b.Run("slog.JSON.LogAttrs.Group", func(b *testing.B) {
		logger := slog.New(slog.NewJSONHandler(io.Discard,
			&slog.HandlerOptions{
				AddSource: true,
			}))
		for i := 0; i < b.N; i++ {
			logger.LogAttrs(ctx, slog.LevelInfo, "ok",
				slog.String("key1", "value1\naaa\nccc\naaaaaaaaaaaaaaaaaaaaaaaaaa"),
				slog.Group(
					"group1",
					slog.Float64("gkey1", 1.0),
					slog.String("gkey2", "eee=ccc"),
				),
			)
		}
	})
}
