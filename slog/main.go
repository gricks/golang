package main

import (
	"log/slog"
	"os"
)

func main() {
	logger := slog.New(slog.NewJSONHandler(os.Stdout,
		&slog.HandlerOptions{
			AddSource: true,
		}))
	slog.SetDefault(logger)
	slog.LogAttrs(nil, slog.LevelInfo, "ok", slog.String("key1", "value1\naaa\nccc"))
	slog.LogAttrs(nil, slog.LevelInfo, "ok",
		slog.String("key1", "value1\naaa\nccc"),
		slog.Group(
			"group1",
			slog.Float64("gkey1", 1.0),
			slog.String("gkey2", "eee"),
		),
	)
}
