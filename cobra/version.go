package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

func init() {
	var ver string
	versionCmd := &cobra.Command{
		Use:   "version",
		Short: "print cobra example version",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("version: " + ver)
		},
	}
	versionCmd.Flags().StringVarP(&ver, "string", "s", "", "version to print")
	versionCmd.MarkFlagRequired("string")

	rootCmd.AddCommand(versionCmd)
}
