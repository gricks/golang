package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

var (
	sig string
	ver bool
)

var rootCmd = &cobra.Command{
	Use: "cobra",
	Run: func(cmd *cobra.Command, args []string) {
		if ver {
			fmt.Println("print version")
			return
		}
		switch sig {
		case "":
		case "stop":
			fmt.Println("stop")
			return
		}
	},
}

func init() {
	rootCmd.Flags().StringVarP(&sig, "signal", "s", "", "signal")
	rootCmd.Flags().BoolVarP(&ver, "version", "v", false, "signal")
}

func main() {
	rootCmd.Execute()
}
