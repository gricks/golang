# Golang
It's userful example for golang best practice.

# Path
It's useful to mkdir `~/.go` as `gopath` for install package and tools, Don't forget add `~/.go/bin` to $PATH.

# Proxy
```
go env -w GOPROXY="https://goproxy.cn,direct"
go env -w GONOPROXY="gitlab.company.com,gitee.com"
go env -w GONOSUMDB="gitlab.company.com"
```
