package main

import (
	"context"
	"testing"

	"golang.org/x/time/rate"
)

func call() {
	for i := 0; i < 100000; i++ {
	}
}

func BenchmarkRate(b *testing.B) {
	limiter := rate.NewLimiter(1000000, 100)
	for i := 0; i < b.N; i++ {
		limiter.Wait(context.TODO())
	}
}

func BenchmarkRateParallel(b *testing.B) {
	limiter := rate.NewLimiter(1000000, 100)
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			limiter.Wait(context.TODO())
			call()
		}
	})
}

func BenchmarkRateCall(b *testing.B) {
	b.Run("Without Rate", func(tb *testing.B) {
		for i := 0; i < tb.N; i++ {
			call()
		}
	})

	b.Run("With Rate", func(tb *testing.B) {
		limiter := rate.NewLimiter(1000000, 100)
		for i := 0; i < tb.N; i++ {
			limiter.Wait(context.TODO())
			call()
		}
	})
}
