package main

import (
	"context"
	"fmt"
	"time"

	"golang.org/x/time/rate"
)

// if u take a token from limiter it will return to limiter after Limit Duration

func main() {
	// rate 速率 burst 容量
	// 当使用 Allow 时:
	// - burst 表示迸发量, rate 表示速率
	// - 当 burst 大于 rate 时。表示迸发量可以超过最大速率,
	// - 当迸发量被消耗完之后，后续的请求可以被限制在 rate 之内
	// - 当一段时间的请求量下降之后, burst 的容量又可以恢复。

	// every 1 action per second, burst 1
	{
		limiter := rate.NewLimiter(1, 1)
		for i := 0; i < 5; i++ {
			if limiter.Allow() {
				fmt.Println("action start")
			} else {
				fmt.Println("action not allow")
			}
			time.Sleep(500 * time.Millisecond)
		}
	}

	fmt.Println("----------------------")

	// rate 速率 burst 容量
	// 当使用 Reserve 时:
	// - burst 表示迸发量, rate 表示速率.
	// - 当迸发量未被消耗完时, delay 返回 0.
	// - 当迸发量被消耗完完后, delay 返回根据 rate 均匀分布的延迟时间.
	// 注意这时已经消耗了一个未来的 token, 可以通过 Cancel 方法将 token 放回.

	// every 2 action per second, burst 1
	{
		limiter := rate.NewLimiter(2, 1)
		for i := 0; i < 10; i++ {
			rv := limiter.Reserve()
			fmt.Println("delay:", rv.Delay())
		}
	}

	fmt.Println("----------------------")

	// rate 速率 burst 容量
	// 当使用 Wait 时:
	// - burst 表示迸发量, rate 表示速率.
	// - 当迸发量未被消耗完时, wait 会立刻返回.
	// - 当迸发量被消耗完完后, wait 会根据 rate 均匀分布的延迟时间返回.
	// 当 wait Context 超时后会自动 Cancel.

	// every 1 action per 100 millisecond, burst 5
	{
		limiter := rate.NewLimiter(rate.Every(100*time.Millisecond), 3)
		for i := 0; i < 6; i++ {
			t := time.Now().Truncate(time.Millisecond)
			err := limiter.Wait(context.TODO())
			if err != nil {
				panic(err)
			}
			fmt.Println("action wait cost: ", time.Now().Truncate(time.Millisecond).Sub(t))
		}

		time.Sleep(400 * time.Millisecond)

		for i := 0; i < 6; i++ {
			t := time.Now().Truncate(time.Millisecond)
			err := limiter.Wait(context.TODO())
			if err != nil {
				panic(err)
			}
			fmt.Println("action wait cost: ", time.Now().Truncate(time.Millisecond).Sub(t))
		}
	}
}
