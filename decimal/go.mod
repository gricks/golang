module decimal

go 1.20

require (
	github.com/cockroachdb/apd v1.1.0
	github.com/ericlagergren/decimal v0.0.0-20221120152707-495c53812d05
	github.com/shopspring/decimal v1.3.1
)

require (
	github.com/lib/pq v1.10.9 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
