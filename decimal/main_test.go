package main_test

import (
	"testing"

	decimal1 "github.com/cockroachdb/apd"
	decimal2 "github.com/ericlagergren/decimal"
	decimal3 "github.com/shopspring/decimal"
)

func BenchmarkDecimal(b *testing.B) {
	b.Run("decimal1", func(b *testing.B) {
		ctx := decimal1.BaseContext.WithPrecision(3)
		for i := 0; i < b.N; i++ {
			d1 := decimal1.New(0, 0)
			d2 := decimal1.New(0, 0)
			ctx.Quo(d1, decimal1.New(1, 0), decimal1.New(3, 0)) // 0.333
			ctx.Quo(d2, decimal1.New(1, 0), decimal1.New(3, 0)) // 0.333
			d3 := decimal1.New(0, 0)
			ctx.Sub(d3, decimal1.New(1, 0), d1) // 1 - 0.333 - 0.333 should be 0.334
			ctx.Sub(d3, d3, d2)
		}
	})

	b.Run("decimal2", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			d1 := decimal2.WithPrecision(3).Quo(decimal2.New(1, 0), decimal2.New(3, 0)) // 0.333
			d2 := decimal2.WithPrecision(3).Quo(decimal2.New(1, 0), decimal2.New(3, 0)) // 0.333
			d3 := decimal2.WithPrecision(3).Sub(decimal2.New(1, 0), d1)
			d3 = d3.Sub(d3, d2) // 1 - 0.333 - 0.333 should be 0.334
		}
	})

	decimal3.DivisionPrecision = 3
	b.Run("decimal3", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			d1 := decimal3.NewFromInt(1).Div(decimal3.NewFromInt(3)) // 0.333
			d2 := decimal3.NewFromInt(1).Div(decimal3.NewFromInt(3)) // 0.333
			decimal3.NewFromInt(1).Sub(d1).Sub(d2)                   // 1 - 0.333 - 0.333 should be 0.334
		}
	})
}
