package main

import (
	"fmt"
	"math"

	decimal1 "github.com/cockroachdb/apd"
	decimal2 "github.com/ericlagergren/decimal"
	decimal3 "github.com/shopspring/decimal"
)

func AlmostEqual(f1, f2 float64, p float64) bool {
	return math.Abs(f1-f2) <= p
}

func main() {
	{
		f1 := 1.0 / 3.0
		f2 := 1.0 / 3.0
		f3 := 1 - f1 - f2
		fmt.Printf("%.3f %.3f %.3f\n", f1, f2, f3) // lost 0.001
		fmt.Println(AlmostEqual(f1, f2, 0.001))
		fmt.Println(AlmostEqual(f2, f1, 0.001))
		fmt.Println(AlmostEqual(f1, f3, 0.001))
		fmt.Println(AlmostEqual(f3, f1, 0.001))
	}

	{
		ctx := decimal1.BaseContext.WithPrecision(3)
		d1 := decimal1.New(0, 0)
		d2 := decimal1.New(0, 0)
		ctx.Quo(d1, decimal1.New(1, 0), decimal1.New(3, 0)) // 0.333
		ctx.Quo(d2, decimal1.New(1, 0), decimal1.New(3, 0)) // 0.333
		d3 := decimal1.New(0, 0)
		ctx.Sub(d3, decimal1.New(1, 0), d1) // 1 - 0.333 - 0.333 should be 0.334
		ctx.Sub(d3, d3, d2)
		fmt.Println(d1, d2, d3)
	}

	{
		d1 := decimal2.WithPrecision(3).Quo(decimal2.New(1, 0), decimal2.New(3, 0)) // 0.333
		d2 := decimal2.WithPrecision(3).Quo(decimal2.New(1, 0), decimal2.New(3, 0)) // 0.333
		d3 := decimal2.WithPrecision(3).Sub(decimal2.New(1, 0), d1)
		d3 = d3.Sub(d3, d2) // 1 - 0.333 - 0.333 should be 0.334
		fmt.Println(d1, d2, d3)
	}

	{
		decimal3.DivisionPrecision = 3
		d1 := decimal3.NewFromInt(1).Div(decimal3.NewFromInt(3)) // 0.333
		d2 := decimal3.NewFromInt(1).Div(decimal3.NewFromInt(3)) // 0.333
		d3 := decimal3.NewFromInt(1).Sub(d1).Sub(d2)             // 1 - 0.333 - 0.333 should be 0.334
		fmt.Println(d1, d2, d3)
	}
}
