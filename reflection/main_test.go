package main

import (
	"reflect"
	"testing"
)

func NewI1() *I1 {
	return new(I1)
}

func BenchmarkReflectNew(b *testing.B) {
	var ii Interface
	ii = (*I1)(nil)
	for i := 0; i < b.N; i++ {
		reflect.New(reflect.TypeOf(ii).Elem()).Interface()
	}
}

func BenchmarkFunctionNew(b *testing.B) {
	for i := 0; i < b.N; i++ {
		NewI1()
	}
}
