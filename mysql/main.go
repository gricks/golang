package main

import (
	"database/sql"
	"flag"
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

var info string

/*
DSN System Variables, Any other parameters are interpreted as system variables:

time_zone=%27Europe%2FParis%27 : SET time_zone='Europe/Paris'

Rules:
The values for string variables must be quoted with '.
The values must also be url.QueryEscape'ed! (which implies values of string variables must be wrapped with %27).
*/

func init() {
	flag.StringVar(&info, "info", "user:pwd@(host:port)/database?timeout=30s&parseTime=true&loc=Local&charset=utf8", "database info")
}

func main() {
	flag.Parse()
	if flag.NFlag() < 1 {
		flag.Usage()
		os.Exit(-1)
	}

	mysql, err := sql.Open("mysql", info)
	if err != nil {
		panic(err.Error())
	}
	defer mysql.Close()

	strSql := "SELECT id FROM mail WHERE title like '%雾迷城 新月之%' and hasitems='0';"
	rows, err := mysql.Query(strSql)
	if err != nil {
		panic(err.Error())
	}

	// Get column names
	columns, err := rows.Columns()
	if err != nil {
		panic(err.Error())
	}

	values := make([]sql.RawBytes, len(columns))
	args := make([]interface{}, len(values))
	for i := range values {
		args[i] = &values[i]
	}

	// Fetch rows
	for rows.Next() {
		err = rows.Scan(args...)
		if err != nil {
			panic(err.Error())
		}
		for _, col := range values {
			// Here we can check if the value is nil (NULL value)
			fmt.Println(string(col))
		}
	}

	if err = rows.Err(); err != nil {
		panic(err.Error())
	}
}
