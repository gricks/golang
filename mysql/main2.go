package main

import (
	"database/sql"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

func main2() {
	mysql, err := sql.Open("mysql", "root:nikki@tcp(127.0.0.1:3306)/")
	if err != nil {
		panic(err)
	}

	mysql.SetMaxOpenConns(20)
	mysql.SetMaxIdleConns(10)

	wg := sync.WaitGroup{}
	for i := 0; i < 200; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			for i := 0; i < 300; i++ {
				// NOTE: 如果 MaxOpen 不等于 MaxIdle 有可能会引起 TIME_WAIT 过多的问题
				// time.Sleep(100 * time.Millisecond)
				v := 0
				row := mysql.QueryRow("SELECT 1")
				err := row.Scan(&v)
				if err != nil {
					panic(err)
				}
			}
		}()
	}

	wg.Wait()
}
