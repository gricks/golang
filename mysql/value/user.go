package main

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"

	"github.com/davecgh/go-spew/spew"
)

type UserSet struct {
	U1 *User `gorm:"size:128"`
	U2 *User `gorm:"text"`
	U3 *User `gorm:"blob"`
}

type User struct {
	A int
	B string
}

// INSERT INTO user_sets(u1,u2,u3) VALUES(null,null,null),VALUES("","",""),('{"A":1,"B":"B"}','{"A":1,"B":"B"}','{"A":1,"B":"B"}');

func main() {
	d, err := gorm.Open(mysql.Open("root:nikki@(127.0.0.1:3306)/test?timeout=5s&parseTime=true&loc=Local&charset=utf8"))
	if err != nil {
		panic(err)
	}
	d.AutoMigrate(new(UserSet))

	err = d.Create(&UserSet{}).Error
	if err != nil {
		panic(err)
	}

	err = d.Create(&UserSet{
		U1: &User{A: 1, B: "B1"},
		U2: &User{A: 2, B: "B2"},
		U3: &User{A: 3, B: "B3"},
	}).Error
	if err != nil {
		panic(err)
	}

	uss := []*UserSet{}
	err = d.Find(&uss).Error
	if err != nil {
		panic(err)
	}
	fmt.Println(spew.Sdump(uss))
}
