package main

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"reflect"
)

func (x *User) Value() (driver.Value, error) {
	b, err := json.Marshal(x)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func (x *User) Scan(data interface{}) error {
	b, ok := data.([]byte)
	if !ok {
		return fmt.Errorf("invalid type(%s) Scan `*User`",
			reflect.TypeOf(data))
	}
	if len(b) == 0 {
		return nil
	}
	return json.Unmarshal(b, x)
}
