package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/alipay/fury/go/fury"
)

func main() {
	data, err := os.ReadFile("data.json")
	if err != nil {
		panic(err)
	}
	tweet := new(Tweet)
	err = json.Unmarshal(data, tweet)
	if err != nil {
		panic(err)
	}

	text, err := json.MarshalIndent(tweet, "", "  ")
	if err != nil {
		panic(err)
	}

	fmt.Println(string(text))

	f := fury.NewFury(true)
	f.RegisterTagType("ma.User", User{})

	data, err = os.ReadFile("user.json")
	if err != nil {
		panic(err)
	}

	user1 := new(User)
	err = json.Unmarshal(data, user1)
	if err != nil {
		panic(err)
	}

	data, err = f.Marshal(*user1)
	if err != nil {
		panic(err)
	}

	user2 := new(User)
	err = f.Unmarshal(data, user2)
	if err != nil {
		panic(err)
	}

	data2, err := f.Marshal(*user2)
	if err != nil {
		panic(err)
	}

	user3 := new(User)
	err = f.Unmarshal(data2, user3)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v\n", user1)
	fmt.Printf("%+v\n", user2)
	fmt.Printf("%+v\n", user3)
}

type Tweet struct {
	Statuses []struct {
		Metadata struct {
			ResultType      string `json:"result_type"`
			IsoLanguageCode string `json:"iso_language_code"`
		} `json:"metadata"`
		//CreatedAt            time.Time `json:"created_at"`
		ID                   int64  `json:"id"`
		IDStr                string `json:"id_str"`
		Text                 string `json:"text"`
		Source               string `json:"source"`
		Truncated            bool   `json:"truncated"`
		InReplyToStatusID    int64  `json:"in_reply_to_status_id"`
		InReplyToStatusIDStr string `json:"in_reply_to_status_id_str"`
		InReplyToUserID      int64  `json:"in_reply_to_user_id"`
		InReplyToUserIDStr   string `json:"in_reply_to_user_id_str"`
		InReplyToScreenName  string `json:"in_reply_to_screen_name"`
		User                 struct {
			ID          int64  `json:"id"`
			IDStr       string `json:"id_str"`
			Name        string `json:"name"`
			ScreenName  string `json:"screen_name"`
			Location    string `json:"location"`
			Description string `json:"description"`
			URL         string `json:"url"`
			Entities    struct {
				Description struct {
					Urls []string `json:"urls"`
				} `json:"description"`
			} `json:"entities"`
			Protected      bool  `json:"protected"`
			FollowersCount int64 `json:"followers_count"`
			FriendsCount   int64 `json:"friends_count"`
			ListedCount    int64 `json:"listed_count"`
			//CreatedAt       time.Time `json:"created_at"`
			FavouritesCount int64  `json:"favourites_count"`
			UtcOffset       int    `json:"utc_offset"`
			TimeZone        int    `json:"time_zone"`
			GeoEnabled      bool   `json:"geo_enabled"`
			Verified        bool   `json:"verified"`
			StatusesCount   int64  `json:"statuses_count"`
			Lang            string `json:"lang"`
		} `json:"user"`
	} `json:"statuses"`
}

type User struct {
	ID              int64  `json:"id"`
	IDStr           string `json:"id_str"`
	Name            string `json:"name"`
	ScreenName      string `json:"screen_name"`
	Location        string `json:"location"`
	Description     string `json:"description"`
	URL             string `json:"url"`
	Protected       bool   `json:"protected"`
	FollowersCount  int64  `json:"followers_count"`
	FriendsCount    int64  `json:"friends_count"`
	ListedCount     int64  `json:"listed_count"`
	FavouritesCount int64  `json:"favourites_count"`
	UtcOffset       int    `json:"utc_offset"`
	TimeZone        int    `json:"time_zone"`
	GeoEnabled      bool   `json:"geo_enabled"`
	Verified        bool   `json:"verified"`
	StatusesCount   int64  `json:"statuses_count"`
	Lang            string `json:"lang"`
}
