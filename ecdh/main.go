package main

import (
	"crypto/elliptic"
	"crypto/rand"
	"fmt"
)

func main() {
	curve := elliptic.P224()

	Apriv, Ax, Ay, err := elliptic.GenerateKey(curve, rand.Reader)
	if err != nil {
		panic(err)
	}

	Bpriv, Bx, By, err := elliptic.GenerateKey(curve, rand.Reader)
	if err != nil {
		panic(err)
	}

	s1, _ := curve.ScalarMult(Bx, By, Apriv)
	fmt.Println(s1.Text(16))

	s2, _ := curve.ScalarMult(Ax, Ay, Bpriv)
	fmt.Println(s2.Text(16))
}
