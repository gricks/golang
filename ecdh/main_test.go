package main

import (
	"testing"

	"crypto/elliptic"
	"crypto/rand"
)

func TestECDH(t *testing.T) {
	curve := elliptic.P224()
	testcase := func(tt *testing.T) {
		tt.Parallel()
		Apriv, Ax, Ay, err := elliptic.GenerateKey(curve, rand.Reader)
		if err != nil {
			t.Fatal("unexpected", err)
		}
		Bpriv, Bx, By, err := elliptic.GenerateKey(curve, rand.Reader)
		if err != nil {
			t.Fatal("unexpected", err)
		}
		s1, _ := curve.ScalarMult(Ax, Ay, Bpriv)
		s2, _ := curve.ScalarMult(Bx, By, Apriv)
		if s1.Text(10) != s2.Text(10) {
			t.Fatal("unexpected")
		}
	}
	t.Run("TestECDH1", testcase)
	t.Run("TestECDH2", testcase)
}

func BenchmarkECDH(b *testing.B) {
	curve := elliptic.P224()
	for i := 0; i < b.N; i++ {
		Apriv, Ax, Ay, err := elliptic.GenerateKey(curve, rand.Reader)
		if err != nil {
			b.Fatal("unexpected", err)
		}
		Bpriv, Bx, By, err := elliptic.GenerateKey(curve, rand.Reader)
		if err != nil {
			b.Fatal("unexpected", err)
		}
		curve.ScalarMult(Ax, Ay, Bpriv)
		curve.ScalarMult(Bx, By, Apriv)
	}
}

func BenchmarkECDHParallel(b *testing.B) {
	curve := elliptic.P224()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			Apriv, Ax, Ay, err := elliptic.GenerateKey(curve, rand.Reader)
			if err != nil {
				b.Fatal("unexpected", err)
			}
			Bpriv, Bx, By, err := elliptic.GenerateKey(curve, rand.Reader)
			if err != nil {
				b.Fatal("unexpected", err)
			}
			curve.ScalarMult(Ax, Ay, Bpriv)
			curve.ScalarMult(Bx, By, Apriv)
		}
	})
}
