package main

import (
	"bytes"
	"testing"
)

var x = bytes.Repeat([]byte{'x'}, 1023)
var y = bytes.Repeat([]byte{'y'}, 1023)

func BenchmarkStringConcate1(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = (" " + string(x) + string(y))[1:]
	}
}

func BenchmarkStringConcate2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = string(x) + string(y)
	}
}
