package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"reflect"
)

func main() {
	s := `{"a":9007199254740993}`
	fmt.Println(s)
	{ // raw
		var v map[string]interface{}
		err := json.Unmarshal([]byte(s), &v)
		if err != nil {
			panic(err)
		}
		// lost precision
		fmt.Println(v["a"], reflect.TypeOf(v["a"]))
		b, err := json.Marshal(v)
		if err != nil {
			panic(err)
		}
		fmt.Println(string(b))
	}
	{ // use number
		var v map[string]interface{}
		enc := json.NewDecoder(bytes.NewBuffer([]byte(s)))
		enc.UseNumber()
		err := enc.Decode(&v)
		if err != nil {
			panic(err)
		}
		fmt.Println(v["a"], reflect.TypeOf(v["a"]))
		b, err := json.Marshal(v)
		if err != nil {
			panic(err)
		}
		fmt.Println(string(b))
	}
}
