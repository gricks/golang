package main

import (
	"bufio"
	"fmt"
	"go/parser"
	"go/token"
	"os"

	"github.com/davecgh/go-spew/spew"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	for {
		scanner.Scan()
		source := scanner.Text()
		fmt.Printf("source: %s\n", source)
		fset := token.NewFileSet()
		expr, err := parser.ParseExprFrom(
			fset, "", source, 0)
		if err != nil {
			fmt.Printf("invalid expr(%s) with error(%s)\n", source, expr)
			continue
		}
		fmt.Println(spew.Sdump(expr))
	}
}
