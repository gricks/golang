package main

import "fmt"

func Goto(c int) {
	fmt.Println("case:", c)
	switch c {
	case 1:
		fmt.Println("begin goto _EXIT")
		goto _EXIT
		fmt.Println("end goto _EXIT")
	default:
	}
	fmt.Println("begin _EXIT")
_EXIT:
	fmt.Println("end _EXIT")
}

func main() {
	Goto(0)
	Goto(1)
}
