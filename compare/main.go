package main

import (
	"encoding/json"
	"fmt"
	"unicode"
	"unicode/utf8"

	"github.com/google/go-cmp/cmp"
)

type User struct {
	Id    int
	Name  string
	Vals  []int
	Attrs map[string]interface{}

	unexport int
}

func main() {
	u1 := &User{
		Id:   1,
		Name: "bob",
		Vals: []int{1, 2, 3},
		Attrs: map[string]interface{}{
			"attr1": 1.0,
			"attr2": "a",
		},
		unexport: 1,
	}

	b, err := json.Marshal(u1)
	if err != nil {
		panic(err)
	}

	u2 := new(User)
	err = json.Unmarshal(b, u2)
	if err != nil {
		panic(err)
	}

	u3 := new(User)
	*u3 = *u1
	u3.Id = 2

	var v1 interface{}
	err = json.Unmarshal(b, &v1)
	if err != nil {
		panic(err)
	}

	var v2 interface{}
	err = json.Unmarshal(b, &v2)
	if err != nil {
		panic(err)
	}

	fmt.Printf("u1: %+v\n", u1)
	fmt.Printf("u2: %+v\n", u2)
	fmt.Printf("v1: %+v\n", v1)

	fmt.Println("u1 equal u2: ", cmp.Equal(u1, u2,
		cmp.FilterPath(func(p cmp.Path) bool {
			s := p.String()
			if len(s) > 0 {
				r, _ := utf8.DecodeRuneInString(s)
				if !unicode.IsUpper(r) {
					return true
				}
			}
			return false
		}, cmp.Ignore()),
	))

	fmt.Println("u1 equal u3: ", cmp.Equal(u1, u3,
		cmp.FilterPath(func(p cmp.Path) bool {
			s := p.String()
			if len(s) > 0 {
				r, _ := utf8.DecodeRuneInString(s)
				if !unicode.IsUpper(r) {
					return true
				}
			}
			return false
		}, cmp.Ignore()),
	))

	fmt.Println("u1 equal v1: ", cmp.Equal(u1, v1))
	fmt.Println("v1 equal v2: ", cmp.Equal(v1, v2))
}
