package tpkg

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func Test_Add(t *testing.T) {
	Convey("add two number", t, func() {
		So(Add(1, 2), ShouldEqual, 3)
	})
}

func Test_Sub(t *testing.T) {
	Convey("sub two number", t, func() {
		So(Sub(1, 2), ShouldEqual, -1)
	})
}

func Test_Mul(t *testing.T) {
	Convey("multiply two number", t, func() {
		So(Mul(3, 2), ShouldEqual, 6)
	})
}

func Test_Div(t *testing.T) {
	Convey("division two number", t, func() {
		Convey("divisor 0", func() {
			_, err := Div(10, 0)
			So(err, ShouldNotBeNil)
		})
		Convey("divisor not 0", func() {
			num, err := Div(10, 2)
			So(err, ShouldBeNil)
			So(num, ShouldEqual, 5)
		})
	})
}
