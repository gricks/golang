package main

import (
	"fmt"
	"testing"
)

//go:noinline
func VaArgs(a ...string) int {
	c := a[0]
	return int([]byte(c)[0])
}

//go:noinline
func ArrArgs(a []string) int {
	c := a[0]
	return int([]byte(c)[0])
}

func BenchmarkVaArgs(b *testing.B) {
	as := make([]string, 1024)
	for i := range as {
		as[i] = fmt.Sprint(i)
	}

	b.Run("VaArgs", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			VaArgs(as...)
		}
	})

	b.Run("ArrArgs", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			ArrArgs(as)
		}
	})
}
