package main

import (
	"fmt"

	"github.com/dgrijalva/jwt-go"
)

var secret = []byte("secret")

func main() {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"name": "gricks",
	})
	tokenStr, err := token.SignedString(secret)
	fmt.Println(tokenStr, err)

	token, err = jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return secret, nil
	})

	if err != nil {
		panic(err)
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		panic("unexpected")
	}

	fmt.Printf("Header: %+v\n", token.Header)
	fmt.Printf("Claims: %+v\n", claims)
}
