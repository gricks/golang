package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func main() {
	r := strings.NewReader("Go is a general-purpose language designed with systems programming in mind.")
	b, err := ioutil.ReadAll(r)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s", b)

	dir, err := ioutil.ReadDir("../")
	if err != nil {
		log.Fatal(err)
	}
	for _, file := range dir {
		fmt.Println(file.Name())
	}

	b, err = ioutil.ReadFile("./main.go")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s", b)
}
