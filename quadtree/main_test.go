package main

import (
	"math/rand"
	"testing"
)

func makeObjs(n, boundWidth, squareWidthMin, squareWidthMax int) []*Rect {
	objs := []*Rect{}
	for i := 0; i < n; i++ {
		width := rand.Intn(squareWidthMax-squareWidthMin+1) + squareWidthMin
		objs = append(objs, &Rect{
			X: rand.Intn(boundWidth),
			Y: rand.Intn(boundWidth),
			W: width,
			H: width,
		})
	}
	return objs
}

func AABB(r1, r2 *Rect) bool {
	return !(r1.X > r2.X+r2.W ||
		r1.X+r1.W < r2.X ||
		r1.Y > r2.Y+r2.H ||
		r1.Y+r1.H < r2.Y)
}

func TestQuadTree(t *testing.T) {
	nk := 100
	n := 10000
	boundWidth := 300
	squareWidthMin := 5
	squareWidthMax := 10
	quadTree := NewQuadTree(1, &Rect{0, 0, boundWidth, boundWidth})
	{
		objs := makeObjs(n, boundWidth, squareWidthMin, squareWidthMax)
		for i := 0; i < len(objs); i++ {
			quadTree.Insert(objs[i])
		}
	}

	for k := 0; k < nk; k++ {
		quadTree.Clear()
		objs := makeObjs(n, boundWidth, squareWidthMin, squareWidthMax)
		for i := 0; i < len(objs); i++ {
			quadTree.Insert(objs[i])
		}

		target := objs[0]

		cobjs1 := []*Rect{}
		for i := 0; i < len(objs); i++ {
			if AABB(target, objs[i]) {
				cobjs1 = append(cobjs1, objs[i])
			}
		}

		cobjs2 := quadTree.Retrieve(objs[0])
		for i := 0; i < len(cobjs2); {
			if !AABB(target, cobjs2[i]) {
				cobjs2[i] = cobjs2[len(cobjs2)-1]
				cobjs2 = cobjs2[:len(cobjs2)-1]
			} else {
				i++
			}
		}

		t.Log("cobjs1:", len(cobjs1))
		t.Log("cobjs2:", len(cobjs1))

		if len(cobjs1) != len(cobjs2) {
			t.Fatal("Unexpected")
		}
	}
}

func retrieve(quadTree *QuadTree, rect *Rect) {
	for _, obj := range quadTree.Objects {
		AABB(rect, obj)
	}
	q := quadTree.getQuadrant(rect)
	if !quadTree.Leaf {
		if q != 0 {
			quadTree.Children[q-1].Retrieve(rect)
		} else {
			for i := 0; i < len(quadTree.Children); i++ {
				quadTree.Children[i].Retrieve(rect)
			}
		}
	}
}

func Benchmark_N_1000_Width_100(b *testing.B) {
	benchmark(1000, 100, b)
}

func Benchmark_N_1000_Width_1000(b *testing.B) {
	benchmark(1000, 1000, b)
}

func Benchmark_N_1000_Width_10000(b *testing.B) {
	benchmark(1000, 10000, b)
}

func Benchmark_N_1000_Width_100000(b *testing.B) {
	benchmark(1000, 100000, b)
}

func Benchmark_N_10000_Width_100000(b *testing.B) {
	benchmark(10000, 100000, b)
}

func benchmark(n, boundWidth int, b *testing.B) {
	squareWidthMin := 5
	squareWidthMax := 10
	quadTree := NewQuadTree(1, &Rect{0, 0, boundWidth, boundWidth})
	objs := makeObjs(n, boundWidth, squareWidthMin, squareWidthMax)
	for i := 0; i < len(objs); i++ {
		quadTree.Insert(objs[i])
	}
	quadTree.Clear()

	b.ResetTimer()
	b.Run("Normal", func(tb *testing.B) {
		for c := 0; c < tb.N; c++ {
			for i := 0; i < len(objs); i++ {
				for j := 0; j < len(objs); j++ {
					AABB(objs[i], objs[j])
				}
			}
		}
	})

	b.Run("QuadTree", func(tb *testing.B) {
		for c := 0; c < tb.N; c++ {
			quadTree.Clear()
			for i := 0; i < len(objs); i++ {
				quadTree.Insert(objs[i])
			}
			for i := 0; i < len(objs); i++ {
				retrieve(quadTree, objs[i])
			}
		}
	})
}
