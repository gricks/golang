# oetlcol-contrib
https://github.com/open-telemetry/opentelemetry-collector-releases

```
otelcol-contrib --config=./conf.yaml
```

# jaeger
https://www.jaegertracing.io/docs/1.37/getting-started/

```
docker run -d --name jaeger \
  -e COLLECTOR_OTLP_ENABLED=true \
  -p 16686:16686 \
  -p 4317:4317 \
  -p 14250:14250 \
  -p 14268:14268 \
  -p 14269:14269 \
  jaegertracing/all-in-one:1.37
```

# sentry
https://docs.sentry.io/platforms/go/

```
exporters:
  sentry:
    dsn: http://a2b6f911221e4fdb86c675b526555a61@127.0.0.1:9000/2
```

