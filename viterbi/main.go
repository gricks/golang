package main

import "fmt"

// https://www.zhihu.com/question/20136144

// 注意本例中的 DAG 并不是篱笆型的图, 但是只要 DAG 的遍历顺序和篱笆型图一致.
// 同样可以使用 viterbi 算法. 因为 viterbi 算法的本质就是动态规划.
// 可以通过增加虚拟节点的方式补全成篱笆型图.
// state[s] 表示从 A 到 s 的最短路径长度
// state[G] 表示从 A 到 G 的最短路径长度, 即问题的求解.

type edge struct {
	in     string
	weight int
}

func main() {
	graph := map[string][]*edge{
		"B": {{"A", 4}},
		"C": {{"A", 1}},
		"D": {{"A", 5}, {"B", 2}},
		"E": {{"B", 6}},
		"F": {{"E", 1}, {"D", 1}},
		"G": {{"E", 3}, {"F", 4}, {"D", 10}, {"C", 12}},
	}

	jumps := map[string]string{}
	state := map[string]int{}
	for _, v := range "BCDEFG" {
		s := string(v)
		for _, edge := range graph[s] {
			weight := state[edge.in] + edge.weight
			if state[s] == 0 || state[s] > weight {
				jumps[s] = edge.in
				state[s] = weight
			}
		}
	}
	fmt.Println("value:", state["G"])

	path := "G"
	for path[len(path)-1] != 'A' {
		path += jumps[string(path[len(path)-1])]
	}
	fmt.Println("paths:", path)
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
