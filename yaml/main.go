package main

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Student struct {
	Name  string
	Age   int
	Score []int
}

func main() {
	b, err := ioutil.ReadFile("main.yaml")
	if err != nil {
		panic(err)
	}

	s := &Student{}
	err = yaml.Unmarshal(b, s)
	if err != nil {
		panic(err)
	}

	// Name is empty because of
	// https://github.com/go-yaml/yaml/blob/v2/yaml.go#L395
	// sigh
	fmt.Println(s)

	s = &Student{"Astone", 19, []int{1, 2, 3}}
	b, err = yaml.Marshal(s)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(b))
}
