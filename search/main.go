package main

import (
	"fmt"

	"github.com/go-ego/riot"
	"github.com/go-ego/riot/types"
)

var searcher = riot.Engine{}

func main() {
	searcher.Init(types.EngineOpts{
		Using: 3,
		//	IDOnly:        true,
		GseDict:       "dictionary.txt",
		StopTokenFile: "stop_tokens.txt",
	})
	defer searcher.Close()

	text := "《复仇者联盟3：无限战争》是全片使用IMAX摄影机拍摄"
	text1 := "在IMAX影院放映时"
	text2 := "全片以上下扩展至IMAX 1.9：1的宽高比来呈现"

	// 将文档加入索引，docId 从1开始
	searcher.Index("1", types.DocData{Content: text})
	searcher.Index("2", types.DocData{Content: text1})
	searcher.Index("3", types.DocData{Content: text2})

	// 等待索引刷新完毕
	searcher.Flush()

	fmt.Printf("%+v\n", searcher.Search(types.SearchReq{Text: "复仇者"}))
	fmt.Printf("%+v\n", searcher.Search(types.SearchReq{Text: "影院"}))
}
