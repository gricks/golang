package main

import "github.com/astaxie/beego"

type IndexController struct {
	beego.Controller
}

func (this *IndexController) View() {
	this.TplName = "index.html"
}

func (this *IndexController) Post() {
	this.TplName = ""
	result := make(map[string]interface{})
	this.Data["json"] = result
	defer this.ServeJSON()
	_, head, _ := this.GetFile("file")
	err := this.SaveToFile("file", "static/"+head.Filename)
	if err != nil {
		result["text"] = "failed"
	} else {
		result["text"] = "ok"
	}
}

func main() {
	beego.Router("/", &IndexController{}, "*:View")
	beego.Router("/upload", &IndexController{}, "POST:Post")

	beego.SetStaticPath("/static", "static")
	beego.Run()
}
