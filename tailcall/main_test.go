package main

import "testing"

func BenchmarkFibonacci(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fibonacci(30)
	}
}

func BenchmarkFibonacciTailCall(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fibonacci_tail_call(30)
	}
}

func BenchmarkFibonacciDynamic(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fibonacci_dynamic(30)
	}
}
