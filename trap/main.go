package main

import (
	"fmt"
	"sync"
	"time"
	"unsafe"
)

func trap1() (result int) {
	defer func() { result++ }()
	return 2
}

func trap2() (result int) {
	t := 5
	defer func() { t += 5 }()
	return t
}

func trap3() (result int) {
	defer func(r int) { r += 5 }(result)
	return 1
}

//NOTE: DO NOT USE
// go func(){
// }()
//
// USING
//
// func1() {
// }
// go func1()
//
// This trap is frequent appearances
func trap4() {
	for i := 0; i < 2; i++ {
		go func() {
			fmt.Println(i)
		}()
	}
	time.Sleep(1 * time.Second)

	for i := 0; i < 2; i++ {
		go func(i int) {
			fmt.Println(i)
		}(i)
	}
	time.Sleep(1 * time.Second)
}

var done chan struct{}

func trap5() {
	fmt.Println("done")
	done <- struct{}{}
}

func trap6() {
	var (
		s string = "cc"
	)
	{
		b := []byte(s)
		fmt.Printf("%p: %v, %p: %v\n", b, b, &s, s)
		b[0], b[1] = 'a', 'a'
		fmt.Printf("%p: %v, %p: %v\n", b, b, &s, s)
		b = append(b, "dd"...)
		fmt.Printf("%p: %v, %p: %v\n", b, b, &s, s)
	}
	{
		b := (*[]byte)(unsafe.Pointer(&s))
		fmt.Printf("%p: %v, %p: %v\n", *b, *b, &s, s)
		// string address is const address
		//(*b)[0], (*b)[1] = 'a', 'a'
		//fmt.Printf("%p: %v, %p: %v\n", *b, *b, &s, s)
		//*b = append(*b, "dd"...)
	}
}

// panic defer unlock
func trap7() {
	flag := false
	var lock sync.Mutex
	defer func() {
		if err := recover(); err != nil {
			if !flag {
				fmt.Println("i'm die without unlock")
			} else {
				fmt.Println("i'm die but unlock success")
			}
		}
	}()

	func() {
		fmt.Println("i'm lock")
		lock.Lock()
		defer func() {
			flag = true
			fmt.Println("i'm unlock")
			lock.Unlock()
		}()

		panic("i'm panic")

		// lock.Unlock() // missed unlock
	}()
}

// goroutine panic defer
func trap8() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("i'm die, but recovered.", err)
		}
	}()

	fmt.Println("i'm running")

	go func() {
		// panic("i'm panic") // it's will panic, and cann't be recover by trap8() defer recover().
	}()
	time.Sleep(10 * time.Millisecond)
}

func trap9() {
	{
		a1 := make([]int, 1, 1)
		a2 := append(a1, 1)
		fmt.Println(a1, a2)
		a3 := append(a1, 2)
		fmt.Println(a1, a2, a3)
	}
	{
		a1 := make([]int, 1, 2)
		a2 := append(a1, 1)
		fmt.Println(a1, a2)
		a3 := append(a1, 2)
		fmt.Println(a1, a2, a3) // !!! a2 unexpected changed
	}
	{
		a1 := make([]int, 1, 2)
		t1 := a1
		a2 := append(t1, 1)
		fmt.Println(a1, a2)
		t2 := a1
		a3 := append(t2, 2)
		fmt.Println(a1, a2, a3) // !!! a2 unexpected changed
	}
	{
		a1 := make([]int, 0)
		fmt.Printf("%p %d %d\n", a1, len(a1), cap(a1))
		a1 = append(a1, 1)
		fmt.Printf("%p %d %d\n", a1, len(a1), cap(a1))
		a1 = append(a1, 1)
		fmt.Printf("%p %d %d\n", a1, len(a1), cap(a1))
		a1 = append(a1, 1)
		fmt.Printf("%p %d %d\n", a1, len(a1), cap(a1))
		a1 = append(a1, 1)
		fmt.Printf("%p %d %d\n", a1, len(a1), cap(a1))
		a1 = append(a1, 1)
		fmt.Printf("%p %d %d\n", a1, len(a1), cap(a1))
	}
}

func trap10() {
	{
		tformat := "2006-01-02 15:04:05"
		now := time.Now().Truncate(time.Second)
		sNow := now.Format(tformat)
		fmt.Println("time.Format:", sNow)
		t, _ := time.Parse(tformat, sNow)
		fmt.Println("time.Parse Sub Now:", now.Sub(t).Seconds())
		t, _ = time.ParseInLocation(tformat, sNow, time.Local)
		fmt.Println("time.ParseInLocation Sub Now:", now.Sub(t).Seconds())
	}

	fmt.Println()

	{
		tformat := time.RFC3339
		now := time.Now().Truncate(time.Second)
		sNow := now.Format(tformat)
		fmt.Println("time.Format:", sNow)
		t, _ := time.Parse(tformat, sNow)
		fmt.Println("time.Parse Sub Now:", now.Sub(t).Seconds())
		t, _ = time.ParseInLocation(tformat, sNow, time.Local)
		fmt.Println("time.ParseInLocation Sub Now:", now.Sub(t).Seconds())
	}
}

func trap11() {
	a := []int{1, 2, 3}

	{
		ref := []*int{}
		for _, v := range a {
			ref = append(ref, &v) // !!! NOTE: Don't use range value address
		}
		for _, v := range ref {
			fmt.Printf("%d ", *v)
		}
		fmt.Println()
	}
	{
		// The right way
		ref := []*int{}
		for i := range a {
			ref = append(ref, &a[i])
		}
		for _, v := range ref {
			fmt.Printf("%d ", *v)
		}
		fmt.Println()
	}
}

func trap12() int {
	// NOTE: IMPORTANT !!!
	timeout := 1 * time.Second
	ch := make(chan int)
	// ch := make(chan int, 1) // right way
	go func() {
		// goroutine leak
		fmt.Println("start goroutine")
		defer fmt.Println("end goroutine")

		result := func() int {
			// a long time process
			time.Sleep(2 * time.Second)
			return 1
		}()
		ch <- result // blocks
	}()

	select {
	case result := <-ch: // process success
		return result
	case <-time.After(timeout): // process timeout
		return -1
	}
}

func trap13() {
	v := 1
	defer func(v int) {
		fmt.Println(v) // 1
	}(v)

	defer func() {
		fmt.Println(v) // 2
	}()
	v = 2
	// out:
	// 2
	// 1
}

// struct value range

type trap14State struct {
	val int
}

func (s *trap14State) Reset() {
	s.val = 0
}

func trap14() {
	// It's trip me again 2022/03/09.
	// !!! NOTE: Don't use range value address
	{
		states := []trap14State{
			{1}, {2}, {3},
		}
		for _, state := range states {
			state.Reset()
		}
		for i, state := range states {
			fmt.Println(i, state)
		}
	}
	fmt.Println("----")
	{
		states := []*trap14State{
			{1}, {2}, {3},
		}
		for _, state := range states {
			state.Reset()
		}
		for i, state := range states {
			fmt.Println(i, state)
		}
	}
}

type object struct {
	v int
}

func (obj *object) Ref() {
	fmt.Println("object:", obj.v)
}

func (obj object) NonRef() {
	fmt.Println("object:", obj.v)
}

func trap15() {
	// The arguments to defer are evaluated immediately,
	// but the execution only happens later

	// defer o1.Ref() is the syntactic sugar of defer (*object).Ref(&o1)
	// &o1 has been evaluated, but the value of o1 has been modified.
	o1 := object{v: 1}
	defer o1.Ref() // print 2
	o1 = object{v: 2}

	o2 := object{v: 3}
	defer o2.NonRef() // print 3
	o2 = object{v: 4}

	o3 := object{v: 5}
	defer func() { o3.Ref() }() // print 6
	o3 = object{v: 6}

	o4 := object{v: 7}
	defer func() { o4.NonRef() }() // print 8
	o4 = object{v: 8}

	// defer o1.Ref() is the syntactic sugar of defer (*object).Ref(p1)
	// p1 has been evaluated, but the value of p1 hasn't modified.
	p1 := &object{v: 1}
	defer p1.Ref() // print 1
	p1 = &object{v: 2}

	p2 := &object{v: 3}
	defer p2.NonRef() // print 3
	p2 = &object{v: 4}

	p3 := &object{v: 5}
	defer func() { p3.Ref() }() // print 6
	p3 = &object{v: 6}

	p4 := &object{v: 7}
	defer func() { p4.NonRef() }() // print 8
	p4 = &object{v: 8}
}

func main() {
	fmt.Println("------------trap1-------------")
	fmt.Println(trap1())
	fmt.Println("------------trap2-------------")
	fmt.Println(trap2())
	fmt.Println("------------trap3-------------")
	fmt.Println(trap3())
	fmt.Println("------------trap4-------------")
	trap4()
	fmt.Println("------------trap5-------------")
	// trap5
	// deadlock, done is a new variable, not the global variable
	//done := make(chan struct{})
	done = make(chan struct{})
	go trap5()
	<-done
	fmt.Println("------------trap6-------------")
	trap6()
	fmt.Println("------------trap7-------------")
	trap7()
	fmt.Println("------------trap8-------------")
	trap8()
	fmt.Println("------------trap9-------------")
	trap9()
	fmt.Println("------------trap10-------------")
	trap10()
	fmt.Println("------------trap11-------------")
	trap11()
	fmt.Println("------------trap12-------------")
	trap12()
	fmt.Println("------------trap13-------------")
	trap13()
	fmt.Println("------------trap14-------------")
	trap14()
	fmt.Println("------------trap15-------------")
	trap15()
}
