package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/facebookgo/grace/gracehttp"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(10 * time.Second)
		fmt.Fprintf(w, "grace Welcome\n")
	})

	srv := &http.Server{
		Addr:         ":19882",
		Handler:      http.DefaultServeMux,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	pid := os.Getpid()
	ioutil.WriteFile("grace.pid", []byte(fmt.Sprintf("%d", pid)), 0644)

	gracehttp.Serve(srv)
}
