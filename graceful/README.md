# Nginx
[https://serverfault.com/questions/140990/nginx-automatic-failover-load-balancing](https://serverfault.com/questions/140990/nginx-automatic-failover-load-balancing)
```
proxy_next_upstream error;     
proxy_next_upstream_tries 1;
```

# Fork
[https://github.com/facebookarchive/grace](https://github.com/facebookarchive/grace)

# SO\_REUSEADDR
[https://github.com/cloudflare/tableflip](https://github.com/cloudflare/tableflip)
