package main

import (
	"fmt"

	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/load"
	"github.com/shirou/gopsutil/mem"
)

var bunits = [...]string{"", "Ki", "Mi", "Gi", "Ti"}

func ShortenByte(bytes int) string {
	i := 0
	f := float32(bytes)
	for ; f > 100 && i < 4; i++ {
		f /= 1024
	}
	return fmt.Sprintf("%.2f%sB", f, bunits[i])
}

func main() {
	v, err := mem.VirtualMemory()
	if err != nil {
		panic(err)
	}

	fmt.Printf("MemTotal: %v, MemUsed: %v, MemUsedPercent:%.2f%%\n", v.Total, v.Used, v.UsedPercent)

	cpuinfo, err := cpu.Info()

	fmt.Printf("Cores: %v\n", len(cpuinfo))

	avg, err := load.Avg()
	if err != nil {
		panic(err)
	}

	fmt.Printf("Load1: %.2f%%, Load5: %.2f%%, Load15: %.2f%%\n", avg.Load1*100, avg.Load5*100, avg.Load15*100)

	host, err := host.Info()
	if err != nil {
		panic(err)
	}

	fmt.Printf("Uptime: %v\n", host.Uptime)
}
