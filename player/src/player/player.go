package main

import "golang/player/src/lib"

func main() {
	manager := lib.NewMusicManager()
	manager.Add(&lib.Music{
		"1", "My Heart Will Go On",
		"Celion Dion",
		"http://qbox.me/24501234",
		"Pop",
	})

	manager.Add(&lib.Music{
		"2", "Five Hours",
		"Deorro",
		"http://qbox.me/24501235",
		"Original Mix",
	})

	music := manager.Find("2")
	if music != nil {
		lib.Play(music.Name, "MP3")
	}
}
