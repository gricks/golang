package lib

type Music struct {
	ID     string
	Name   string
	Artist string
	Source string
	Type   string
}

type MusicManager struct {
	musics map[string]*Music
}

func NewMusicManager() *MusicManager {
	return &MusicManager{make(map[string]*Music)}
}

func (m *MusicManager) Len() int {
	return len(m.musics)
}

func (m *MusicManager) Add(music *Music) {
	m.musics[music.ID] = music
}

func (m *MusicManager) Del(ID string) {
	delete(m.musics, ID)
}

func (m *MusicManager) Find(ID string) *Music {
	music, ok := m.musics[ID]
	if ok {
		return music
	}
	return nil
}
