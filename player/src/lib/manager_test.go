package lib

import "testing"

func TestManager1(t *testing.T) {
	manager := NewMusicManager()

	if manager == nil {
		t.Error("NewMusicManager failed")
	}

	if manager.Len() != 0 {
		t.Error("NewMusicManager failed, manager not empty")
	}

	/*
		music := &Music{
			ID:     "1",
			Name:   "My Heart Will Go On",
			Artist: "Celion Dion",
			Source: "http://qbox.me/24501234",
			Type:   "Pop",
		}
	*/

	music := &Music{
		"1",
		"My Heart Will Go On",
		"Celion Dion",
		"http://qbox.me/24501234",
		"Pop",
	}

	manager.Add(music)
	if manager.Len() != 1 {
		t.Error("Add failed")
	}
	if manager.Find("1") == nil {
		t.Error("Find failed")
	}

	manager.Del("1")
	if manager.Len() != 0 {
		t.Error("Del failed")
	}
	if manager.Find("1") != nil {
		t.Error("Del failed")
	}

}
