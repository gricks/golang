package lib

import "fmt"

type Player interface {
	play(source string)
}

type MP3Player struct{}

func (player *MP3Player) play(source string) {
	fmt.Println("play mp3", source)
}

type WAVPlayer struct{}

func (player *WAVPlayer) play(source string) {
	fmt.Println("play wav", source)
}

func Play(source string, mtype string) {
	var player Player
	switch mtype {
	case "MP3":
		player = &MP3Player{}
	case "WAV":
		player = &WAVPlayer{}
	default:
		fmt.Println("unknow mtype:", mtype)
		return
	}
	player.play(source)
}
