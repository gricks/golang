package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Controller interface {
	// Prepare()
	// Finish()
	Handle(c *gin.Context)
}

func RegisterHandle(path string, ctrl Controller) {
	route.Any(path, func() gin.HandlerFunc {
		return func(c *gin.Context) {
			// ctrl.Prepare()
			ctrl.Handle(c)
			// ctrl.Finish()
		}
	}())
}

//////////////////////////////////////////////////////

type IndexRequest struct {
	Name string
	Age  int
}

type IndexControl struct{}

func (this *IndexControl) Handle(c *gin.Context) {
	r := &IndexRequest{}
	if err := c.BindJSON(r); err != nil {
		return
	}
	//time.Sleep(5 * time.Second)
	c.JSON(http.StatusOK, r)
}

//////////////////////////////////////////////////////

type GetUserRequest struct {
	Ids1 []int `query:"ids1"`
	Ids2 []int `query:"ids2[]"`
}

type GetUserControl struct{}

func (this *GetUserControl) Handle(c *gin.Context) {
	request := new(GetUserControl)
	err := c.ShouldBindQuery(&request)
	if err != nil {
		panic(err)
	}
	c.JSON(http.StatusOK, request)
}

//////////////////////////////////////////////////////

type GetRoleRequest struct {
	Id   int   `form:"id"`
	Ids1 []int `form:"ids1"`
	Ids2 []int `form:"ids2[]"`
}

type GetRoleControl struct{}

func (this *GetRoleControl) Handle(c *gin.Context) {
	request := new(GetRoleRequest)
	err := c.ShouldBindQuery(&request)
	if err != nil {
		panic(err)
	}
	c.JSON(http.StatusOK, request)
}
