package main

import "gitee.com/gricks/logrus"

type Option interface {
	apply()
}

type funcOption struct {
	f func()
}

func (this *funcOption) apply() {
	this.f()
}

func newFuncOption(f func()) *funcOption {
	return &funcOption{
		f: f,
	}
}

func WithLogger(l *logrus.Logger) Option {
	return newFuncOption(func() {
		logger = l
	})
}
