package main

import "gitee.com/gricks/logrus"

var (
	logger *logrus.Logger
)

func SetLogger(l *logrus.Logger) {
	logger = l
}

func GetLogger() *logrus.Entry {
	return logger.GetEntry()
}
