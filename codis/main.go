package main

import (
	"bytes"
	"fmt"
	"hash/crc32"
)

const MaxSlots = 1024

func Hash(key []byte) uint32 {
	const (
		TagBeg = '{'
		TagEnd = '}'
	)
	if beg := bytes.IndexByte(key, TagBeg); beg >= 0 {
		if end := bytes.IndexByte(key[beg+1:], TagEnd); end >= 0 {
			key = key[beg+1 : beg+1+end]
		}
	}
	return crc32.ChecksumIEEE(key)
}

func GetKeyHash(key string) uint32 {
	return Hash([]byte(key)) % MaxSlots
}

func main() {
	fmt.Println(GetKeyHash("user:1001:base"))
}
