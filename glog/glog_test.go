package glog

import "testing"

func Test1(t *testing.T) {
	log := NewGlogger("../glog/logic", DEBUG)
	log.Debug("%s", "say debug")
	log.Info("%s", "say info")
	log.Warn("%s", "say warn")
	log.Error("%s", "say error")
}

func BenchmarkWrapperLogger(b *testing.B) {
	log := NewGlogger("logic", DEBUG)
	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			log.Warn("%s", "say warn")
		}
	})
}
