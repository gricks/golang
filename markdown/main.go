package main

import (
	"bytes"
	"fmt"
	"text/template"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/parser"
)

type WarnningInfo struct {
	Role  string
	Value string
}

var tpl = `
## 报警明细

| Role | 监控指标 |
| :---:| :--- |
{{range $WarnningInfo := .WarnningInfos}}|{{$WarnningInfo.Role}}|{{$WarnningInfo.Value}}|
{{end}}
`

func main() {
	WarnningInfos := []*WarnningInfo{
		{
			Role:  "100001",
			Value: "10",
		},
		{
			Role:  "100002",
			Value: "100",
		},
		{
			Role:  "100003",
			Value: "200",
		},
	}

	d := make(map[string]interface{})
	d["WarnningInfos"] = WarnningInfos

	t, err := template.New("tpl").Parse(tpl)
	if err != nil {
		panic(err)
	}

	b := &bytes.Buffer{}
	err = t.Execute(b, d)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(b.Bytes()))

	// parse markdown

	parse := parser.NewWithExtensions(parser.CommonExtensions | parser.AutoHeadingIDs)
	html := markdown.ToHTML(b.Bytes(), parse, nil)

	fmt.Println(string(html))
}
