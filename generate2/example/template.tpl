package {{.Package}}
// NOTE: THIS FILE WAS PRODUCED BY
// CODE GENERATION TOOL
// DO NOT EDIT
{{$length := len .Imports}}{{if ne $length 0}}
import (
    {{range .Imports}} "{{.}}" {{end}}
)
{{end}}

func init() { {{range $Struct := .Structs}}
    RegisterHandle(CC_{{$Struct.Name}}, {{$Struct.Name}}Invoke, func() interface{} { return new({{$Struct.Name}}Request) }) {{end}}
}

{{range $Struct := .Structs}}
func {{$Struct.Name}}Invoke(r interface{}) (int, interface{}) {
    return {{$Struct.Name}}Handle(r.(*{{$Struct.Name}}Request))
}
{{end}}
