package main

import "fmt"

func main() {
	var (
		h       *Handler
		ret     int
		rsp     interface{}
		body    []byte
		command int
	)

	command = CC_Register
	body = []byte(`{"Name":"Astone", "Age":18}`)
	h = GetHandler(command)
	if h == nil {
		panic("unexpected")
	}
	fmt.Println("Invoke CC_Register")
	ret, rsp = h.Invoke(body)
	fmt.Println("Response", ret, rsp)

	/////
	fmt.Println("-----")

	command = CC_Calculate1
	body = []byte(`{"Value1":1, "Value2":2, "Expr":"+"}`)
	h = GetHandler(command)
	if h == nil {
		panic("unexpected")
	}
	fmt.Println("Invoke CC_Calculate1")
	ret, rsp = h.Invoke(body)
	fmt.Println("Response", ret, rsp)

	/////
	fmt.Println("-----")

	command = CC_Calculate2
	body = []byte(`{"Value1":3, "Value2":1, "Expr":"-"}`)
	h = GetHandler(command)
	if h == nil {
		panic("unexpected")
	}
	fmt.Println("Invoke CC_Calculate2")
	ret, rsp = h.Invoke(body)
	fmt.Println("Response", ret, rsp)
}
