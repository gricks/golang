package main

//////////

type RegisterRequest struct {
	Name string
	Age  int
}

type RegisterResponse struct {
	Name string
	Age  int
}

//////////

type Calculate1Request struct {
	Value1 int
	Value2 int
	Expr   string
}

type Calculate1Response struct {
	Value int
}

//////////

type Calculate2Request struct {
	Value1 int
	Value2 int
	Expr   string
}

type Calculate2Response struct {
	Value int
}
