package main

import (
	"bytes"
	"fmt"
	"go/parser"
	"go/printer"
	"go/token"
	"io/ioutil"
	"path"
	"strings"
	"text/template"
)

const strtpl = `package {{.Package}}
// NOTE: THIS FILE WAS PRODUCED BY
// CODE GENERATION TOOL
// DO NOT EDIT
{{$length := len .Imports}}{{if ne $length 0}}
import (
    {{range .Imports}} "{{.}}" {{end}}
)
{{end}}

{{range $Struct := .Structs}}
func (self *{{$Struct.Name}}) Info() string {
    var info string {{range $Field := $Struct.Fields}}
	info += "name:{{$Field.Name}}, type:{{$Field.Type}} tag:{{$Field.TagRaw}}"{{end}}
    return info
}
{{end}}
`

func (p *Parser) Print() error {
	b, err := ioutil.ReadFile(p.source.Template)
	if err != nil {
		b = []byte(strtpl)
	}

	tpl, err := template.New("tpl").Parse(string(b))
	if err != nil {
		return err
	}
	var buffer bytes.Buffer
	if err := tpl.Execute(&buffer, p.source); err != nil {
		return err
	}

	fset := token.NewFileSet()
	file, err := parser.ParseFile(fset, "", &buffer, parser.ParseComments)
	if err != nil {
		fmt.Println()
		fmt.Println(string(buffer.Bytes()))
		return err
	}
	buffer.Reset()

	pp := printer.Config{Mode: printer.UseSpaces, Tabwidth: 4}
	err = pp.Fprint(&buffer, fset, file)
	if err != nil {
		fmt.Println()
		fmt.Println(string(buffer.Bytes()))
		return err
	}

	filename := strings.TrimSuffix(
		path.Join(p.source.Filedir, p.source.Filename), ".go") +
		p.source.Suffix

	return ioutil.WriteFile(filename, buffer.Bytes(), 0644)
}
