package main

import (
	"fmt"

	"github.com/RoaringBitmap/roaring"
)

func main() {
	rb := roaring.BitmapOf(0, 1, 2, 3, 4, 5, 100, 1000)
	fmt.Println(rb.String())
	fmt.Println(rb.ToBase64())

	fmt.Println("Cardinality: ", rb.GetCardinality())
	fmt.Println("Contains 0: ", rb.Contains(0))
	fmt.Println("Contains 3: ", rb.Contains(3))
	fmt.Println("Contains 6: ", rb.Contains(6))
	fmt.Println("Contains 100: ", rb.Contains(100))

	// empty
	{
		fmt.Println("emtpy")
		rb := roaring.BitmapOf()
		fmt.Println(rb.String())
		fmt.Println(rb.ToBase64())
	}
	{
		fmt.Println("1024")
		a := make([]uint32, 0, 1024)
		for i := uint32(0); i < 1024; i++ {
			a = append(a, i)
		}
		rb := roaring.BitmapOf(a...)
		fmt.Println(rb.String())
		fmt.Println(rb.ToBase64())
		b, err := rb.ToBytes()
		fmt.Println(len(b), err)
	}
}
