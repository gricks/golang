package main

import "testing"

func sliceAppend(b *testing.B, n int) {
	rst := []int{}
	num := make([]int, n)
	for i := 0; i < n; i++ {
		num[i] = i
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		rst = append([]int{}, num...)
		rst[0] = 1 // copy-on-write
		rst = rst[:0]
	}
}

func sliceCopy(b *testing.B, n int) {
	rst := []int{}
	num := make([]int, n)
	for i := 0; i < n; i++ {
		num[i] = i
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		rst = make([]int, n)
		copy(rst, num)
		rst[0] = 1 // copy-on-write
		rst = rst[:0]
	}
}

func BenchmarkAppend10(b *testing.B) {
	sliceAppend(b, 10)
}

func BenchmarkCopy10(b *testing.B) {
	sliceCopy(b, 10)
}

func BenchmarkAppend100(b *testing.B) {
	sliceAppend(b, 100)
}

func BenchmarkCopy100(b *testing.B) {
	sliceCopy(b, 100)
}

func BenchmarkAppend1000(b *testing.B) {
	sliceAppend(b, 1000)
}

func BenchmarkCopy1000(b *testing.B) {
	sliceCopy(b, 1000)
}

func BenchmarkAppend100000(b *testing.B) {
	sliceAppend(b, 100000)
}

func BenchmarkCopy100000(b *testing.B) {
	sliceCopy(b, 100000)
}
