# Trimpath

```
go build
./trimpath
file:/root/go/src/golang/trimpath/main.go:11
file:/root/.go/pkg/mod/golang.org/x/sync@v0.3.0/errgroup/errgroup.go:75
```

```
go build -trimpath
./trimpath
file:trimpath/main.go:11
file:golang.org/x/sync@v0.3.0/errgroup/errgroup.go:75
```
