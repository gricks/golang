package main

import (
	"fmt"
	"runtime"

	"golang.org/x/sync/errgroup"
)

func info() {
	_, file, line, ok := runtime.Caller(0)
	if ok {
		fmt.Printf("file:%s:%d\n", file, line)
	}

	eg := new(errgroup.Group)
	eg.Go(func() error {
		_, file, line, ok := runtime.Caller(1)
		if ok {
			fmt.Printf("file:%s:%d\n", file, line)
		}
		return nil
	})
	eg.Wait()
}

func main() {
	info()
}
