package main

import (
	"fmt"
	"time"

	"github.com/gomodule/redigo/redis"
)

func Close(con redis.Conn) {
	time.Sleep(time.Second)
	con.Close()
}

func init() {
	RedisPool := &redis.Pool{
		MaxIdle:   1,
		MaxActive: 1,
		Dial: func() (redis.Conn, error) {
			conn, err := redis.Dial("tcp", "127.0.0.1:6379")
			if err != nil {
				return nil, err
			}
			return conn, nil
		},
	}

	for i := 0; i < 3; i++ {
		con := RedisPool.Get()
		if con.Err() != nil {
			fmt.Println("Get Redis Connection Failed:", con.Err())
		} else {
			fmt.Println("Get Redis Connection Success")
		}
		go Close(con)
	}

	// block get when exhausted
	RedisPool.Wait = true

	for i := 0; i < 3; i++ {
		con := RedisPool.Get() // block here
		if con.Err() != nil {
			fmt.Println("Get Redis Connection Failed:", con.Err())
		} else {
			fmt.Println("Get Redis Connection Success")
		}
		go Close(con)
	}
}
