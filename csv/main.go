package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/gocarina/gocsv"
)

type AchieveConf struct {
	ID         string `csv:"id"`
	Name       string `csv:"nameutf"`
	Clothes    string `csv:"clothes"`
	ClothesVec []int  `csv:"-"`
}

func main() {
	b, err := ioutil.ReadFile("data.csv")
	if err != nil {
		panic(err)
	}

	achs := []*AchieveConf{}
	if err := gocsv.UnmarshalBytes(b, &achs); err != nil {
		panic(err)
	}

	var tachs []*AchieveConf
	for _, ach := range achs {
		if ach.Clothes != "" {
			tachs = append(tachs, ach)
			if err := json.Unmarshal([]byte(ach.Clothes), &ach.ClothesVec); err != nil {
				panic(err)
			}
		}
	}

	achs = tachs
	for _, ach := range achs {
		fmt.Println(ach)
	}
}
