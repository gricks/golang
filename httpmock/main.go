package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/go-resty/resty/v2"
	"github.com/jarcoal/httpmock"
)

func main() {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	// our database of articles
	articles := make(map[string]interface{}, 0)
	articles["book1"] = "book1"
	articles["book2"] = "book2"

	url := "https://api.mybiz.com/articles"

	// mock to list out the articles
	httpmock.RegisterResponder("GET", url,
		func(req *http.Request) (*http.Response, error) {
			resp, err := httpmock.NewJsonResponse(200, articles)
			if err != nil {
				return httpmock.NewStringResponse(500, ""), nil
			}
			return resp, nil
		},
	)

	{ // stdhttp
		resp, err := http.Get(url)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}

		fmt.Println(string(b))
	}

	{ // resty
		clt := resty.New()
		httpmock.ActivateNonDefault(clt.GetClient())

		articlesObj := make(map[string]interface{}, 0)
		_, err := clt.NewRequest().SetResult(&articlesObj).Get(url)
		if err != nil {
			panic(err)
		}

		b, _ := json.Marshal(articlesObj)
		fmt.Println(string(b))
	}
}
