package main

import "mathes/internal"

//export add
func add(x, y uint64) uint64 {
	return x + y
}

//export timestamp
func timestamp() uint64 {
	return internal.Timestamp()
}

func main() {}
