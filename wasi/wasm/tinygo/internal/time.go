package internal

import "time"

func Timestamp() uint64 {
	return uint64(time.Now().Unix())
}
