package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"os"

	_ "embed"

	"github.com/tetratelabs/wazero"
	"github.com/tetratelabs/wazero/imports/wasi_snapshot_preview1"
)

//go:embed wasm/bin/logs.wasm
var logswasm []byte

//go:embed wasm/bin/math.wasm
var mathwasm []byte

func main() {
	ctx := context.Background()

	r := wazero.NewRuntime(ctx)

	defer r.Close(ctx)

	_, err := r.NewHostModuleBuilder("env").
		NewFunctionBuilder().
		WithFunc(func(v uint32) {
			fmt.Println("log_i32 >>", v)
		}).
		Export("log_i32").
		Instantiate(ctx)
	if err != nil {
		panic(err)
	}

	wasi_snapshot_preview1.MustInstantiate(ctx, r)

	{
		conf := wazero.NewModuleConfig().
			WithStdin(os.Stdout).
			WithStderr(os.Stderr).
			WithSysNanotime().
			WithSysWalltime()

		mod, err := r.InstantiateWithConfig(ctx, mathwasm, conf)
		if err != nil {
			log.Panicln(err)
		}

		add := mod.ExportedFunction("add")
		if add == nil {
			panic("not found")
		}

		for i := 0; i < 3; i++ {
			x, y := rand.Intn(9), rand.Intn(9)
			v, err := add.Call(ctx, uint64(x), uint64(y))
			if err != nil {
				panic(err)
			}
			fmt.Printf("add.Call(%d, %d) = %d\n", x, y, v[0])
		}

		timestamp := mod.ExportedFunction("timestamp")
		val, err := timestamp.Call(ctx)
		if err != nil {
			panic(err)
		}

		fmt.Printf("Timestamp:%+v\n", val)
	}
	{
		conf := wazero.NewModuleConfig().
			WithSysNanotime().
			WithSysWalltime()

		_, err := r.InstantiateWithConfig(ctx, logswasm, conf)
		if err != nil {
			log.Panicln(err)
		}
	}
}
