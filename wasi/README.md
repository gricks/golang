# What is WebAssembly ?
https://www.webassembly.guide/webassembly-guide/webassembly/what-is-webassembly

# WebAssembly binary toolkit
https://github.com/WebAssembly/wabt

# Golang wasm runtime
https://github.com/tetratelabs/wazero

# Why go build can't export wasm function ?
https://github.com/golang/go/issues/42372
