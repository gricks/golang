package main

import (
	"encoding/json"
	"fmt"

	dynamicstruct "github.com/Ompluscator/dynamic-struct"
)

type Metadata struct {
	Name     string
	Type     interface{}
	Tag      string
	Typename string
	Parent   *Metadata `json:"-"`
	Object   []*Metadata
}

type User struct {
	Name  string `json:"name"`
	Age   int    `json:"age"`
	Extra []int  `json:"extra"`
}

func main() {
	metadatas := []*Metadata{
		{Name: "Name", Type: "", Tag: `json:"name"`},
		{Name: "Age", Type: 0, Tag: `json:"age"`},
		{Name: "Extra", Type: []int{}, Tag: `json:"extra"`},
	}

	ins := dynamicstruct.NewStruct()
	for _, metadata := range metadatas {
		ins.AddField(metadata.Name, metadata.Type, metadata.Tag)
	}

	u := &User{
		Name:  "mozart",
		Age:   18,
		Extra: []int{1, 2, 3},
	}

	b, err := json.Marshal(u)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%s\n", string(b))

	v := ins.Build().New()
	if err := json.Unmarshal(b, v); err != nil {
		panic(err)
	}

	b, err = json.Marshal(v)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%s\n", string(b))
}
