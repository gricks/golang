package main

func MakeUserKey(Gid uint32, Uid uint32) uint64 {
	return (uint64(Uid) | (uint64(Gid) << 32))
}
