package main

import "time"

const (
	GC_Host = ":12323"

	GC_SignalCache = 4

	GC_UserCount    = 3
	GC_MessageCount = 100
	GC_ReadBuffer   = 1024
	GC_WriteBuffer  = 1024

	GC_StatusWait    = 0
	GC_StatusRunning = 1
	GC_StatusClosed  = 2

	GC_ReadyTimeout = 3 * time.Minute
	GC_IdleTimeout  = 1 * time.Minute
)
