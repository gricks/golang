package main

const (
	CC_Auth      = 10001
	CC_Ready     = 10002
	CC_Begin     = 10003
	CC_Forward   = 10004
	CC_Broadcast = 10005
	CC_End       = 10006
)
