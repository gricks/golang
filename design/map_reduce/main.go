package main

import "fmt"

func main() {
	mr := new(MapReduce).Set([]int{1, 2, 3, 4, 5})
	fmt.Println(mr)

	mr = mr.Map(func(v int) int { return v * 3 })
	fmt.Println(mr)

	mr = mr.Filter(func(v int) bool { return v%2 != 0 })
	fmt.Println(mr)

	fmt.Println(mr.Reduce(func(out int, val int) int {
		fmt.Println(out, val)
		out += val
		return out
	}, 0))
}
