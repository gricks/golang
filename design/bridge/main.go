package main

import "fmt"

// https://refactoringguru.cn/design-patterns/bridge

type Shape interface {
	Draw()
}

////////////

type Circle struct {
	color Color
}

func NewCircle(color Color) *Circle {
	return &Circle{color: color}
}

func (s *Circle) Draw() {
	fmt.Println("Draw circle with color", s.color.String())
}

////////////

type Square struct {
	color Color
}

func NewSquare(color Color) *Square {
	return &Square{color: color}
}

func (s *Square) Draw() {
	fmt.Println("Draw square with color", s.color.String())
}

////////////

type Color interface {
	String() string
}

type Red struct{}

func (Red) String() string { return "red" }

////////////

type Blue struct{}

func (Blue) String() string { return "blue" }

func main() {
	var shape Shape

	shape = NewSquare(new(Red))
	shape.Draw()

	shape = NewCircle(new(Blue))
	shape.Draw()
}
