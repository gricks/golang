package main

import (
	"context"
	"fmt"
	"runtime"
	"time"

	"net/http"
	_ "net/http/pprof"
)

func main() {
	func() {
		for i := 0; i < 1000000; i++ {
			_, _ = context.WithTimeout(context.Background(), 15*time.Second)
		}
		fmt.Println("Create Done")
	}()

	time.AfterFunc(15*time.Second, func() {
		fmt.Println("Timeout")
	})

	go func() {
		for {
			runtime.GC()
			time.Sleep(time.Second)
		}
	}()

	http.ListenAndServe(":80", nil)
}
