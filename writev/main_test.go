package main

import (
	"net"
	"strings"
	"testing"
	"time"
	"unsafe"
)

func BenchmarkWrite_Writev(b *testing.B) {
	conn, err := net.Dial("tcp", "127.0.0.1:19889")
	if err != nil {
		panic(err)
	}
	length, context := []byte("32|"), []byte(strings.Repeat("a", 32))
	buffers := make(net.Buffers, 0, 2)
	b.Run("escape", func(b *testing.B) {
		b.ReportAllocs()
		for i := 0; i < b.N; i++ {
			vbuffers := append(buffers, length, context)
			_, err = vbuffers.WriteTo(conn)
			if err != nil {
				panic(err)
			}
		}
	})
	b.Run("noescape", func(b *testing.B) {
		b.ReportAllocs()
		for i := 0; i < b.N; i++ {
			vbuffers := append(buffers, length, context)
			pbuffers := (*net.Buffers)(noescape(unsafe.Pointer(&vbuffers)))
			_, err = pbuffers.WriteTo(conn)
			if err != nil {
				panic(err)
			}
		}
	})
}

func init() {
	go Server()
	time.Sleep(time.Second)
}

//go:nosplit
func noescape(p unsafe.Pointer) unsafe.Pointer {
	x := uintptr(p)
	return unsafe.Pointer(x ^ 0) //nolint
}

func Server() {
	lis, err := net.Listen("tcp", ":19889")
	if err != nil {
		panic(err)
	}
	defer lis.Close()
	for {
		conn, err := lis.Accept()
		if err != nil {
			panic(err)
		}
		go func() {
			defer conn.Close()
			buffer := make([]byte, 1024)
			for {
				_, err = conn.Read(buffer)
				if err != nil {
					break
				}
			}
		}()
	}
}
