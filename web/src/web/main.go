package main

import (
	"log"

	"net/http"
	_ "net/http/pprof"

	"golang/web/src/method"
)

type RoutingMapType map[string]func(http.ResponseWriter, *http.Request)

func init() {
	RoutingMap := make(RoutingMapType, 10)
	RoutingMap["/view"] = method.ViewHandler
	RoutingMap["/list"] = method.ListHandler
	RoutingMap["/upload"] = method.UploadHandler

	for key, val := range RoutingMap {
		http.HandleFunc(key, val)
	}
}

func main() {
	err := http.ListenAndServe(":18080", nil)
	if err != nil {
		log.Fatal("ListenAndServe:", err.Error())
	}
}
