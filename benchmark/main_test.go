package main

import (
	"bytes"
	"fmt"
	"strings"
	"testing"
)

var d = []string{"this", "is", "a", "Benchmark"}

func BenchmarkString_Sprintf(b *testing.B) {
	b.StopTimer()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		s := fmt.Sprintf("%s|%s|%s|%s", d[0], d[1], d[2], d[3])
		_ = s
	}
}

func BenchmarkString_Add(b *testing.B) {
	b.StopTimer()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		s := ""
		s += d[0] + "|"
		s += d[1] + "|"
		s += d[2] + "|"
		s += d[3]
		_ = s
	}
}

func BenchmarkString_Join(b *testing.B) {
	b.StopTimer()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		s := strings.Join(d, "|")
		_ = s
	}
}

func BenchmarkString_ByteWrite(b *testing.B) {
	b.StopTimer()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		b := bytes.Buffer{}
		b.WriteString(d[0])
		b.WriteString("|")
		b.WriteString(d[1])
		b.WriteString("|")
		b.WriteString(d[2])
		b.WriteString("|")
		b.WriteString(d[3])
		_ = b.String()
	}
}
