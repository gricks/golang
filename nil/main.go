package main

import "fmt"

type User struct {
	Info *Info
}

type Info struct{}

func (this *Info) Nil() bool {
	if this == nil {
		return true
	}
	return false
}

func funcs(s string) interface{} {
	switch s {
	case "nil":
		return nil
	case "var":
		var p *int
		if p != nil {
			panic(p)
		}
		return p
	case "var_nil":
		var p *int
		p = nil
		if p != nil {
			panic(p)
		}
		return p
	case "var_interface":
		var p interface{}
		if p != nil {
			panic(p)
		}
		return p
	case "var_interface_nil":
		var p interface{}
		p = nil
		if p != nil {
			panic(p)
		}
		return p
	case "func_nil":
		return funcs("nil")
	}
	return nil
}

func main() {
	u := &User{}
	fmt.Println(u.Info.Nil())
	u = (*User)(nil)
	// fmt.Println(u.Info.Nil()) // panic u is nil

	fmt.Println("-----------")
	fmt.Println(funcs("nil") == nil)               // true
	fmt.Println(funcs("var") == nil)               // false
	fmt.Println(funcs("var_nil") == nil)           // false
	fmt.Println(funcs("var_interface") == nil)     // true
	fmt.Println(funcs("var_interface_nil") == nil) // true
	fmt.Println(funcs("func_nil") == nil)          // true
}
