package main

import (
	"github.com/fatih/color"
)

func main() {
	color.Cyan("Prints text in cyan")
	color.Green("Prints text in green")
	color.Yellow("Prints text in yellow")
	color.Magenta("Prints text in magenta")
}
