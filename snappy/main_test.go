package main

import (
	"io/ioutil"
	"testing"

	"github.com/golang/snappy"
	jsoniter "github.com/json-iterator/go"
)

type dataStruct struct {
	ID   int
	Zone int
	NPC  int
}

func BenchmarkSnappyEncode(b *testing.B) {
	in, _ := ioutil.ReadFile("text")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		snappy.Encode(nil, in)
	}
}

func BenchmarkSnappyDecode(b *testing.B) {
	in, _ := ioutil.ReadFile("text")
	out := snappy.Encode(nil, in)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := snappy.Decode(nil, out)
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkJsonMarshal(b *testing.B) {
	in, _ := ioutil.ReadFile("text")
	d := []*dataStruct{}
	err := jsoniter.Unmarshal(in, &d)
	if err != nil {
		b.Fatal(err)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		jsoniter.Marshal(d)
	}
}

func BenchmarkJsonUnmarshal(b *testing.B) {
	in, _ := ioutil.ReadFile("text")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d := []*dataStruct{}
		err := jsoniter.Unmarshal(in, &d)
		if err != nil {
			b.Fatal(err)
		}
	}
}
