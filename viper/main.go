package main

import (
	"fmt"
	"time"

	"github.com/spf13/viper"
)

type Config struct {
	Value1   string
	Value2   int
	Value3   map[string]string
	Interval time.Duration
}

func main() {
	viper.SetConfigFile("conf.yaml")
	{
		if err := viper.ReadInConfig(); err != nil {
			panic(err)
		}

		fmt.Println("Using config file:", viper.ConfigFileUsed())

		conf := &Config{}
		if err := viper.Unmarshal(conf); err != nil {
			panic(err)
		}

		fmt.Printf("Conf:%+v\n", conf)

		viper.Set("value2", 1000)
		if err := viper.WriteConfig(); err != nil {
			panic(err)
		}
	}

	{
		if err := viper.ReadInConfig(); err != nil {
			panic(err)
		}

		fmt.Println("Using config file:", viper.ConfigFileUsed())

		conf := &Config{}
		if err := viper.Unmarshal(conf); err != nil {
			panic(err)
		}

		fmt.Printf("Conf:%+v\n", conf)

		viper.Set("value2", 10)
		if err := viper.WriteConfig(); err != nil {
			panic(err)
		}
	}
}
