package localizer

import (
	"golang.org/x/text/language"
	"golang.org/x/text/message"

	_ "i18n/internal/translations"
)

var tags = []language.Tag{
	language.AmericanEnglish, // The first language is used as fallback.
	language.SimplifiedChinese,
	language.TraditionalChinese,
}

var (
	matcher = language.NewMatcher(tags)
	locales = map[language.Tag]*Localizer{}
)

func init() {
	for _, tag := range tags {
		locales[tag] = &Localizer{printer: message.NewPrinter(tag)}
	}
}

type Localizer struct {
	printer *message.Printer
}

func Get(langs ...string) *Localizer {
	_, idx := language.MatchStrings(matcher, langs...)
	if idx < 0 || idx >= len(tags) {
		idx = 0
	}
	locale, ok := locales[tags[idx]]
	if !ok {
		panic("unexpected")
	}
	return locale
}

func (l Localizer) T(key message.Reference, args ...interface{}) string {
	return l.printer.Sprintf(key, args...)
}
