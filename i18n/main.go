package main

import (
	"fmt"
	"net/http"

	"i18n/internal/localizer"
)

func main() {
	http.HandleFunc("/wellcome", func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseForm()
		if err != nil {
			panic(err)
		}
		locale := localizer.Get(r.Form.Get("lang"), r.Header.Get("Accept-Language"))
		fmt.Fprintln(w, locale.T("wellcome"))
	})

	http.ListenAndServe(":8080", nil)
}
