# Install
# go install golang.org/x/text/cmd/gotext@latest

curl 'http://localhost:8080/wellcome'
curl 'http://localhost:8080/wellcome?lang=en'
curl 'http://localhost:8080/wellcome?lang=en-US'
curl 'http://localhost:8080/wellcome?lang=zh'
curl 'http://localhost:8080/wellcome?lang=zh-CN'
curl 'http://localhost:8080/wellcome?lang=zh-TW'
curl 'http://localhost:8080/wellcome?lang=zh-Hans'
curl 'http://localhost:8080/wellcome?lang=zh-Hant'
curl 'http://localhost:8080/wellcome?lang=123'

