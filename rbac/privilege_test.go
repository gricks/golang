package main

import "testing"

func TestPrivilege(t *testing.T) {
	p := NewPrivilege()
	grants := []*Grant{
		&Grant{
			Action: "r",
			Object: "cat",
		},
		&Grant{
			Action: "w",
			Object: "cat",
		},
	}

	role := "bob"
	p.Add(role, grants...)
	if !p.Auth(role, &Grant{Action: "r", Object: "cat"}) {
		t.Fatal("unexpected")
	}

	grants = p.Get(role)
	if len(grants) != 2 {
		t.Fatal("unexpected")
	}

	roles := p.GetRoles()
	if len(roles) != 1 {
		t.Fatal("unexpected")
	}

	// grant not exist
	if p.Auth(role, &Grant{Action: "r", Object: "dog"}) {
		t.Fatal("unexpected")
	}

	// grant later
	p.Add(role, &Grant{Action: "r", Object: "dog"})
	if !p.Auth(role, &Grant{Action: "r", Object: "dog"}) {
		t.Fatal("unexpected")
	}

	// grant deleted
	p.Del(role, &Grant{Action: "r", Object: "dog"})
	if p.Auth(role, &Grant{Action: "r", Object: "dog"}) {
		t.Fatal("unexpected")
	}

}
