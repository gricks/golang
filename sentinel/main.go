package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/alibaba/sentinel-golang/api"
	"github.com/alibaba/sentinel-golang/core/base"
	"github.com/alibaba/sentinel-golang/core/config"
	"github.com/alibaba/sentinel-golang/core/flow"
	"github.com/alibaba/sentinel-golang/util"
)

func main() {
	os.Setenv(config.LogDirEnvKey, "./logs")

	// We should initialize Sentinel first.
	err := api.InitDefault()
	if err != nil {
		log.Fatalf("Unexpected error: %+v", err)
	}

	if _, err := flow.LoadRules([]*flow.FlowRule{
		{
			Resource:        "some-test",
			MetricType:      flow.QPS,
			Count:           10,
			ControlBehavior: flow.Reject,
		},
	}); err != nil {
		log.Fatalf("Unexpected error: %+v", err)
		return
	}

	for i := 0; i < 20; i++ {
		go func() {
			for {
				e, b := api.Entry("some-test", api.WithTrafficType(base.Inbound))
				if b != nil {
					// Blocked. We could get the block reason from the BlockError.
					time.Sleep(time.Duration(rand.Uint64()%10) * time.Millisecond)
				} else {
					// Passed, wrap the logic here.
					fmt.Println(util.CurrentTimeMillis(), "passed")
					// Be sure the entry is exited finally.
					e.Exit()
				}
			}
		}()
	}

	select {}
}
