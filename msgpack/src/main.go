package main

import (
	"fmt"
	"log"

	"golang/msgpack/src/proto"
)

func main() {
	student1 := proto.Student{1000, 23, "Astone"}
	data, err := student1.MarshalMsg(nil)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(student1)
	fmt.Println(data, len(data), student1.Msgsize())

	student2 := proto.Student{}
	data, err = student2.UnmarshalMsg(data)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(student2)
}
