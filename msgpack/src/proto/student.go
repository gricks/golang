package proto

//go:generate msgp
type Student struct {
	ID   int32  `msg:"id"`
	Age  int32  `msg:"age"`
	Name string `msg:"name"`
}
