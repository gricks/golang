package proto

//go:generate msgp
type Message struct {
	ID  int32  `msg:id`
	Msg string `msg:msg`
}

type Protocol struct {
	ID   int32  `msg:id`
	Desc string `msg:desc`
}
