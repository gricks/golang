package main

import (
	"fmt"
	"sort"
)

type Struct struct {
	V int
	S string
}

type StructSlice_SortV []Struct

func (self StructSlice_SortV) Len() int           { return len(self) }
func (self StructSlice_SortV) Less(i, j int) bool { return self[i].V < self[j].V }
func (self StructSlice_SortV) Swap(i, j int)      { self[i], self[j] = self[j], self[i] }

type StructSlice_SortS []Struct

func (self StructSlice_SortS) Len() int           { return len(self) }
func (self StructSlice_SortS) Less(i, j int) bool { return self[i].S < self[j].S }
func (self StructSlice_SortS) Swap(i, j int)      { self[i], self[j] = self[j], self[i] }

func main() {
	{
		a := []int{1, 3, 2, 4, 5}
		sort.IntSlice(a).Sort()
		fmt.Println(a)
	}
	{
		a := []int{1, 3, 2, 4, 5}
		sort.Sort(sort.Reverse(sort.IntSlice(a)))
		fmt.Println(a)
		sort.Ints(a)
		fmt.Println(a)
	}
	{
		a := []string{"Tomas", "Astone", "Jack"}
		sort.Strings(a)
		fmt.Println(a)
		sort.Sort(sort.Reverse(sort.StringSlice(a)))
		fmt.Println(a)
	}
	{
		a := []Struct{{2, "Tomas"}, {3, "Astone"}, {1, "Jack"}}
		sort.Sort(StructSlice_SortV(a))
		fmt.Println(a)
		sort.Sort(StructSlice_SortS(a))
		fmt.Println(a)
	}
}
