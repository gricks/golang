```
go test -test.bench=".*" -benchmem
goos: linux
goarch: amd64
pkg: write
cpu: Intel Xeon Processor (Skylake, IBRS)
BenchmarkWrite_WriteTwice-4            92254         13076 ns/op           0 B/op          0 allocs/op
BenchmarkWrite_WriteOnceCopy-4        192986          6600 ns/op          80 B/op          1 allocs/op
BenchmarkWrite_Writev-4               164373          6713 ns/op          24 B/op          1 allocs/op
BenchmarkWrite_Write-4                224497          6495 ns/op           0 B/op          0 allocs/op
PASS
ok      write   23.589s
```
