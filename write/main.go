package main

import (
	"net"
)

func main() {
	Server()
}

func Server() {
	lis, err := net.Listen("tcp", ":19889")
	if err != nil {
		panic(err)
	}
	defer lis.Close()

	for {
		conn, err := lis.Accept()
		if err != nil {
			panic(err)
		}
		go func() {
			defer conn.Close()
			buffer := make([]byte, 1024)
			for {
				_, err = conn.Read(buffer)
				if err != nil {
					break
				}
			}
		}()
	}
}
