package main

import (
	"fmt"
	"net"
	"os"
	"testing"
	"time"
)

var (
	context = []byte("lkasdjflkasdjfaskdfj;laskjdfl;askdjf;laksjdflkasjdfl;kasjdfasldfjaslkdf")
	length  = []byte(fmt.Sprintf("%d|", len(context)))
)

func init() {
	go Server()
	time.Sleep(100 * time.Millisecond)
}

func BenchmarkWrite_WriteTwice(b *testing.B) {
	host := os.Getenv("BENCH_HOST")
	if host == "" {
		host = "127.0.0.1:19889"
	}
	conn, err := net.Dial("tcp", host)
	if err != nil {
		panic(err)
	}
	time.Sleep(1 * time.Second)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err = conn.Write(length)
		if err != nil {
			panic(err)
		}
		_, err = conn.Write(context)
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkWrite_WriteOnceCopy(b *testing.B) {
	host := os.Getenv("BENCH_HOST")
	if host == "" {
		host = "127.0.0.1:19889"
	}
	l := len(length) + len(context)
	conn, err := net.Dial("tcp", host)
	if err != nil {
		panic(err)
	}
	time.Sleep(1 * time.Second)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buffer := make([]byte, len(length)+len(context))
		copy(buffer[0:], length)
		copy(buffer[2:], context)
		_, err = conn.Write(buffer[0:l])
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkWrite_Writev(b *testing.B) {
	host := os.Getenv("BENCH_HOST")
	if host == "" {
		host = "127.0.0.1:19889"
	}
	conn, err := net.Dial("tcp", host)
	if err != nil {
		panic(err)
	}
	time.Sleep(1 * time.Second)
	b.ResetTimer()
	buffers := make(net.Buffers, 0, 2)
	for i := 0; i < b.N; i++ {
		vbuffers := append(buffers, length, context)
		_, err = vbuffers.WriteTo(conn)
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkWrite_Write(b *testing.B) {
	host := os.Getenv("BENCH_HOST")
	if host == "" {
		host = "127.0.0.1:19889"
	}
	conn, err := net.Dial("tcp", host)
	if err != nil {
		panic(err)
	}
	time.Sleep(1 * time.Second)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err = conn.Write(context)
		if err != nil {
			panic(err)
		}
	}
}
