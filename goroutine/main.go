package main

import "fmt"

func Calc(a []int, c chan int) {
	ret := 0
	for _, v := range a {
		ret += v
	}
	c <- ret
}

const (
	CORE  = 10
	SIZE  = 100
	SLICE = SIZE / CORE
)

func main() {
	c := make(chan int, CORE)
	a := make([]int, SIZE)
	for k := range a {
		a[k] = k
	}

	for i := 0; i < CORE; i++ {
		go Calc(a[i*SLICE:(i+1)*SLICE], c)
	}

	r := 0
	for i := 0; i < CORE; i++ {
		r += <-c
	}

	fmt.Println(r)
	fmt.Println((0 + SIZE - 1) * SIZE / 2)
}
