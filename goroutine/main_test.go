package main

import (
	"context"
	"testing"
)

func BenchmarkGoroutine(b *testing.B) {
	n := 10000
	b.Run("direct", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_ = calc(n)
		}
	})
	b.Run("go", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_ = parallel(context.Background(), n)
		}
	})
}

func parallel(ctx context.Context, n int) int {
	done := make(chan struct{})
	go func() {
		_ = calc(n)
		close(done)
	}()
	select {
	case <-ctx.Done():
	case <-done:
	}
	return 0
}

func calc(n int) int {
	k := 0
	for i := 0; i < n; i++ {
		k += i
	}
	return k
}
