package main

import (
	"fmt"
)

type Weekday int

const (
	Sunday Weekday = iota
	Monday
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday
)

var days = [...]string{
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
}

func (d Weekday) Int() int {
	return int(d)
}

func (d Weekday) String() string {
	return days[d]
}

func main() {
	fmt.Println(Monday.Int(), Monday.String())
}
