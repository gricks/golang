package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"

	"github.com/davecgh/go-spew/spew"
)

func main() {
	fset := token.NewFileSet()
	source := "((*Version)(((*sparrow/pkg.Version)(main.V)).C)).GoVersion"
	expr, err := parser.ParseExprFrom(fset, "", source, 0)
	if err != nil {
		panic(err)
	}
	fmt.Println(spew.Sdump(expr))

	expr = expr.(*ast.SelectorExpr).X.(*ast.ParenExpr)
	beg, end := fset.Position(expr.Pos()), fset.Position(expr.End())
	src := source[beg.Offset:end.Offset]
	fmt.Println(src)
}
