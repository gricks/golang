package main

import (
	"bytes"
	"fmt"
)

func main() {
	buf := bytes.NewBuffer([]byte("hello"))
	fmt.Println(buf.Len(), buf.Cap(), buf.Bytes(), buf.String())
	buf.Write([]byte(" world"))
	fmt.Println(buf.Len(), buf.Cap(), buf.Bytes(), buf.String())
	buf.Reset()
	fmt.Println(buf.Len(), buf.Cap(), buf.Bytes(), buf.String())
}
