package main

import (
	"fmt"
	"math/rand"
	"time"
)

type Status struct {
	Name string
	Exec func(args ...interface{}) string
}

type Action struct {
	Name string
	Next string
	Exec func(args ...interface{}) string
}

type FSM struct {
	status map[string]*Status
	action map[string]*Action
}

func New() *FSM {
	return &FSM{
		status: make(map[string]*Status),
		action: make(map[string]*Action),
	}
}

func (f *FSM) AddStatus(status *Status) *FSM { f.status[status.Name] = status; return f }
func (f *FSM) Status(status string, args ...interface{}) string {
	return f.Action(f.status[status].Exec(args...), args...)
}

func (f *FSM) AddAction(action *Action) *FSM { f.action[action.Name] = action; return f }
func (f *FSM) Action(action string, args ...interface{}) string {
	a := f.action[action]
	if a.Exec == nil {
		return a.Next
	}
	return a.Exec(args...)
}

func main() {
	lv, hp := 1, 9

	f := New()
	f.AddStatus(&Status{
		Name: "fighting",
		Exec: func(args ...interface{}) string {
			fmt.Println("开始战斗")
			switch rand.Intn(2) {
			case 0: // fail
				hp--
				fmt.Println("战斗失败", "hp:", hp, "lv:", lv)
				return "action_fighting_fail"
			default:
				lv++
				fmt.Println("战斗成功", "hp:", hp, "lv:", lv)
				return "action_fighting_succ"
			}
		},
	}).AddStatus(&Status{
		Name: "upgrade",
		Exec: func(args ...interface{}) string {
			fmt.Println("开始升级")
			switch rand.Intn(2) {
			case 0: // fail
				fmt.Println("升级失败")
				return "action_upgrade_fail"
			default:
				fmt.Println("升级成功")
				return "action_upgrade_succ"
			}
		},
	}).AddAction(&Action{
		Name: "action_fighting_fail",
		Exec: func(args ...interface{}) string {
			if hp <= 0 {
				fmt.Println("死亡")
				return "dead"
			}
			return "upgrade"
		},
	}).AddAction(&Action{
		Name: "action_fighting_succ",
		Exec: func(args ...interface{}) string {
			if lv >= 9 {
				fmt.Println("完结")
				return "done"
			}
			return "fighting"
		},
	}).AddAction(&Action{
		Name: "action_upgrade_fail",
		Exec: func(args ...interface{}) string {
			return "upgrade"
		},
	}).AddAction(&Action{
		Name: "action_upgrade_succ",
		Exec: func(args ...interface{}) string {
			return "fighting"
		},
	})

	status := "fighting"
	for status != "dead" && status != "done" {
		status = f.Status(status)
		time.Sleep(time.Second)
	}
}
