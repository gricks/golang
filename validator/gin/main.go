package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

func init() {
	// register custom tag
	validate := binding.Validator.Engine().(*validator.Validate)
	validate.RegisterValidation("gender", func(fl validator.FieldLevel) bool {
		switch fl.Field().String() {
		case "male", "female":
			return true
		default:
			return false
		}
	})
}

//see: https://github.com/gin-gonic/gin/blob/master/binding/default_validator.go
type Request struct {
	Uid    int    `form:"uid"    binding:"required,gt=10"`
	Gid    int    `form:"gid"    binding:"required,gt=0,lt=100"`
	Gender string `form:"gender" binding:"required,gender"`
	Text   string `form:"text"   binding:"required,len=3"`
}

func query(c *gin.Context) {
	request := new(Request)
	if err := c.ShouldBindQuery(request); err != nil {
		c.Abort()
		c.String(http.StatusOK, "%s", err)
		return
	}
	c.JSON(http.StatusOK, request)
}

func curl(p string) {
	urls := "http://127.0.0.1:9933/query?" + p
	fmt.Println("request:", urls)
	resp, err := http.Get(urls)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println("response:", string(b))
	fmt.Println()
}

func main() {
	go func() {
		time.Sleep(time.Second)
		curl("uid=0")
		curl("uid=1&gid=30")
		curl("uid=20&gid=0")
		curl("uid=20&gid=30")
		curl("uid=20&gid=30&gender=12")
		curl("uid=20&gid=30&gender=male")
		curl("uid=20&gid=30&gender=male&text=12")
		curl("uid=20&gid=30&gender=male&text=123")
		time.Sleep(time.Second)
		os.Exit(0)
	}()

	gin.SetMode(gin.ReleaseMode)

	eng := gin.New()
	eng.Any("/query", query)
	eng.Run(":9933")
}
