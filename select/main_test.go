package main

import "testing"

func BenchmarkNoSelectDefault(b *testing.B) {
	b.StopTimer()
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		c1 := make(chan int, 10000)
		for i := 0; i < 10000; i++ {
			c1 <- i
		}
	}
}

func BenchmarkSelectDefault(b *testing.B) {
	b.StopTimer()
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		c1 := make(chan int, 10000)
		for i := 0; i < 10000; i++ {
			select {
			case c1 <- i:
			default:
			}
		}
	}
}
