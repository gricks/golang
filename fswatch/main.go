package main

import (
	"fmt"

	"gitee.com/gricks/signal"
	"github.com/elastic/beats/libbeat/beat"
	"golang/fswatch/beater"
)

type Pipeline struct {
	ackHandler beat.PipelineACKHandler
}

func (p *Pipeline) Publish(e beat.Event) {
	fmt.Printf("%+v\n", e)
	ack := make([]interface{}, 0)
	ack = append(ack, e.Private)
	p.ackHandler.ACKEvents(ack)
}

func (p *Pipeline) SetACKHandler(ackHandler beat.PipelineACKHandler) {
	p.ackHandler = ackHandler
}

func (p *Pipeline) Close() error {
	return nil
}

func main() {
	s := &signal.Signal{}
	s.Init(nil)

	b, err := beater.New(new(Pipeline))
	if err != nil {
		panic(err)
	}

	go func() {
		err = b.Run()
		if err != nil {
			panic(err)
		}
	}()

	s.SignalTerm = func() {
		b.Stop()
		fmt.Println("Stoped")
	}

	s.Handle()
}
