package beater

import (
	"github.com/elastic/beats/libbeat/beat"
)

type Pipeline interface {
	Publish(beat.Event)
	SetACKHandler(beat beat.PipelineACKHandler)
	Close() error
}

type PipelineFactory struct {
	p Pipeline
}

func NewPipelineFactory(p Pipeline) *PipelineFactory {
	return &PipelineFactory{p: p}
}

func (f *PipelineFactory) SetACKHandler(ackHandler beat.PipelineACKHandler) error {
	f.p.SetACKHandler(ackHandler)
	return nil
}

func (f *PipelineFactory) Connect() (beat.Client, error) {
	return f, nil
}

func (f *PipelineFactory) ConnectWith(c beat.ClientConfig) (beat.Client, error) {
	return f, nil
}

func (f *PipelineFactory) Publish(e beat.Event) {
	f.p.Publish(e)
}

func (f *PipelineFactory) PublishAll(es []beat.Event) {
}

func (f *PipelineFactory) Close() error {
	return f.p.Close()
}
