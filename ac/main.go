package main

import "fmt"

func main() {
	p := []string{"he", "she", "his", "hers"}
	s1 := "he took the gold crown from his head, and placed it on hers, and asked her name, and if she would be his wife."

	fmt.Println(s1)

	ac := ACAutomaton{}
	ac.BuildACFSM(p)

	fmt.Println(ac.Contain(s1))
	s1 = ac.Replace(s1, rune('*'))
	fmt.Println(s1)

	s2 := "a person whom one knows and with whom one has a bond of mutual affection"
	fmt.Println(ac.Contain(s1))
	fmt.Println(ac.Contain(s2))
}
