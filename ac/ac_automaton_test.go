package main

import (
	"testing"
)

func BenchmarkACAutomatonReplace(b *testing.B) {
	b.StopTimer()

	p := []string{"he", "she", "his", "hers"}
	s := "he took the gold crown from his head, and placed it on hers, and asked her name, and if she would be his wife."

	ac := ACAutomaton{}
	ac.BuildACFSM(p)

	r := rune('*')

	b.StartTimer()

	for i := 0; i < b.N; i++ {
		ac.Replace(s, r)
	}
}

func BenchmarkACAutomatonContain(b *testing.B) {
	b.StopTimer()

	p := []string{"he", "she", "his", "hers"}
	s := "a person whom one knows and with whom one has a bond of mutual affection"

	ac := ACAutomaton{}
	ac.BuildACFSM(p)

	b.StartTimer()

	for i := 0; i < b.N; i++ {
		ac.Contain(s)
	}
}
