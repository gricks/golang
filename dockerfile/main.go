package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/",
		func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "Welcome Dockerfile\n")
			fmt.Println("Welcome Dockerfile")
		})

	http.ListenAndServe(":8081", nil)
}
