local function split3(str)
  local r, p = {0,0,0}, 1
  if not str or str:len() == 0 then
      return r
  end
  for i = 1,3,1 do
    local k = str:find(',', p, true)
    if not k then
      r[i] = str:sub(p)
      break
    end
    r[i] = str:sub(p, k - 1)
    p = k + 1
  end
  return r
end

local function split(str, sep)
  if not str or str:len() == 0 then
    return {}
  end
  local r, p, i = {}, 1, 1
  while true do
    local k = str:find(',', p, true)
    if not k then
      r[i] = str:sub(p)
      break
    end
    r[i] = str:sub(p, k - 1)
    p = k + 1
    i = i + 1
  end
  return r
end

local function printtable(t) 
  print("print")
  for k, v in ipairs(t) do
    print(k, v)
  end
end

printtable(split3(nil))
printtable(split3(""))
printtable(split3("1"))
printtable(split3("1,2"))
printtable(split3("1,2,3"))
printtable(split3("4,2,3"))

print("---")

printtable(split(nil, ","))
printtable(split("", ","))
printtable(split("a", ","))
printtable(split("a,b,c", ","))

