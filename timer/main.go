package main

import (
	"fmt"
	"runtime"
	"time"

	"golang/timer/timer"
)

func print1() { fmt.Println("I'm print 1") }
func print2() { fmt.Println("I'm print 2") }

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func main() {
	Timer := timer.NewTimer(100)
	Task1 := timer.NewTimerTask(print1, 500*time.Millisecond)
	fmt.Println(Timer.Add(Task1))
	Task2 := timer.NewTimerTask(print2, 1000*time.Millisecond)
	fmt.Println(Timer.Add(Task2))

	Timer.Start()
	time.Sleep(5 * time.Second)
	Timer.Stop()
	fmt.Println("timer stoped")
	time.Sleep(5 * time.Second)
}
