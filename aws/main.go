package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type Config struct {
	Filename     string `json:"filename"`
	Bucket       string `json:"bucket"`
	Endpoint     string `json:"endpoint"`
	Region       string `json:"region"`
	AccessKeyID  string `json:"access_id"`
	AccessSecret string `json:"access_secret"`
}

func main() {
	var conf Config
	bconf, err := ioutil.ReadFile("conf")
	if err != nil {
		panic(err)
	}

	if err := json.Unmarshal(bconf, &conf); err != nil {
		panic(err)
	}

	fname := conf.Filename
	fcontent, err := ioutil.ReadFile(fname)
	if err != nil {
		panic(err)
	}

	sess, err := session.NewSession(&aws.Config{
		Region:   aws.String(conf.Region),
		Endpoint: aws.String(conf.Endpoint),
		Credentials: credentials.NewStaticCredentials(
			conf.AccessKeyID,
			conf.AccessSecret, "",
		),
	})

	if err != nil {
		panic(err)
	}

	s3obj := s3.New(sess)
	_, err = s3obj.PutObject(&s3.PutObjectInput{
		ACL:    aws.String("public-read"),
		Bucket: aws.String(conf.Bucket),
		Key:    aws.String(fname),
		Body:   bytes.NewReader(fcontent),
	})

	if err != nil {
		panic(err)
	}

	fmt.Println("Success")
}
