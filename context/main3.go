package main

import (
	"context"
	"fmt"
)

func Wait(parent context.Context, parentCancel func()) {

}

func main3() {
	fmt.Println("-------------------------")

	{
		ctxParent, ctxParentCancel := context.WithCancel(context.Background())
		defer ctxParentCancel()
		ctx, cancel := context.WithCancel(ctxParent)
		cancel()
		<-ctx.Done()
		fmt.Println(ctx.Err())
		fmt.Println(ctxParent.Err())
	}

	{
		ctxParent, ctxParentCancel := context.WithCancel(context.Background())
		ctx, cancel := context.WithCancel(ctxParent)
		defer cancel()
		ctxParentCancel()
		<-ctx.Done()
		<-ctxParent.Done()
		fmt.Println(ctx.Err())
		fmt.Println(ctxParent.Err())
	}
}
