package main

import (
	"fmt"

	"github.com/casbin/casbin"
	fileadapter "github.com/casbin/casbin/persist/file-adapter"
)

const rbacModel = `
[request_definition]
r = sub, obj, act

[policy_definition]
p = sub, obj, act

[role_definition]
g = _, _

[policy_effect]
e = some(where (p.eft == allow))

[matchers]
m = g(r.sub, p.sub) && r.obj == p.obj && r.act == p.act
`

func main() {
	m := casbin.NewModel(rbacModel)
	p := fileadapter.NewAdapter("policy.csv")
	e := casbin.NewEnforcer(m, p)

	fmt.Println(e.Enforce("alice", "/data1", "GET"))
	fmt.Println(e.Enforce("alice", "/data2/1", "GET"))
	fmt.Println(e.Enforce("alice", "/data1", "POST"))
	fmt.Println(e.Enforce("bob", "/data1", "GET"))
	fmt.Println(e.Enforce("alice", "/data2", "GET"))

	// NOTE: we must make sure Role must diff between User.
	// we can add a "_role_" and "_user_" prefix
	fmt.Println(e.Enforce("data_admin", "/data1", "GET"))
}
