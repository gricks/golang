package main

import (
	"fmt"

	"github.com/davecgh/go-spew/spew"
)

type Struct struct {
	M    map[string]*Struct
	Next *Struct
}

func main() {
	st := &Struct{}
	st.M = make(map[string]*Struct)
	st.M["asdf"] = &Struct{
		M:    map[string]*Struct{},
		Next: nil,
	}
	fmt.Println(spew.Sdump(st))
}
