package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
)

func main() {
	arg0, err := exec.LookPath(os.Args[0])
	if err != nil {
		panic(err)
	}

	absExecFile, err := filepath.Abs(arg0)
	if err != nil {
		panic(err)
	}

	execDir, execFile := filepath.Split(absExecFile)
	if err != nil {
		panic(err)
	}

	fmt.Println("os.Args[0]:", os.Args[0])
	fmt.Println("arg0:", arg0)
	fmt.Println("absExecFile:", absExecFile)
	fmt.Println("execDir:", execDir)
	fmt.Println("execFile:", execFile)
}
