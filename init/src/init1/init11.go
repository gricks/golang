package init1

import (
	"fmt"

	_ "golang/init/src/init2"
)

func init() {
	fmt.Println("init 11")
}
