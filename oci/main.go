package main

import (
	"archive/tar"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"strings"

	"github.com/google/go-containerregistry/pkg/name"
	"github.com/google/go-containerregistry/pkg/v1/remote"
)

func Image(ref string) {
	refname, err := name.ParseReference(ref)
	if err != nil {
		panic(err)
	}

	img, err := remote.Image(refname)
	if err != nil {
		log.Fatalf("failed to get image: %v", err)
	}

	mediaType, err := img.MediaType()
	if err != nil {
		log.Fatalf("failed to get image media type: %v", err)
	}
	fmt.Println("image media:", mediaType)

	layers, err := img.Layers()
	if err != nil {
		log.Fatalf("failed to get image layers: %v", err)
	}

	for _, layer := range layers {
		rc, err := layer.Uncompressed()
		if err != nil {
			log.Fatalf("failed to get compressed layer: %v", err)
		}
		defer rc.Close()

		mediaType, err := layer.MediaType()
		if err != nil {
			log.Fatalf("failed to get media type: %v", err)
		}

		fmt.Println("layer media:", mediaType)

		switch {
		case strings.HasPrefix(string(mediaType), "application/vnd.oci"):
			b, err := ioutil.ReadAll(rc)
			if err == nil {
				fmt.Println("body:", string(b))
			}

		case strings.HasPrefix(string(mediaType), "application/vnd.docker"):
			tr := tar.NewReader(rc)
			for {
				hdr, err := tr.Next()
				if err == io.EOF {
					break
				}
				if err == nil {
					//if strings.Contains(hdr.Name, "artifact.txt") {
					var data bytes.Buffer
					_, err := io.Copy(&data, tr)
					if err != nil {
						log.Fatalf("failed to read file: %v", err)
					}
					fmt.Printf("file %s: %s\n", hdr.Name, data.Bytes())
					//}
				}
			}
		}
	}
}

func main() {
	Image("public-registry.cn-hangzhou.cr.aliyuncs.com/public/testing/artifact:0.0.1")
	Image("public-registry.cn-hangzhou.cr.aliyuncs.com/public/testing/scratch_artifact:0.0.1")
}
