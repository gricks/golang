package main

import (
	"fmt"
	"math/bits"
)

func main() {
	fmt.Println("Len ---")

	// returns the minimum number of bits required to represent x; the result is 0 for x == 0.
	fmt.Println(1, bits.Len64(1))
	fmt.Println(2, bits.Len64(2))
	fmt.Println(3, bits.Len64(3))
	fmt.Println(4, bits.Len64(4))

	fmt.Println("OnesCount ---")

	// OnesCount returns the number of one bits ("population count") in x.
	fmt.Println(1, bits.OnesCount(1))
	fmt.Println(2, bits.OnesCount(2))
	fmt.Println(3, bits.OnesCount(3))
	fmt.Println(4, bits.OnesCount(4))

	fmt.Println("LeadingZeros32 ---")

	// LeadingZeros32 returns the number of leading zero bits in x; the result is 32 for x == 0.
	fmt.Println(1, bits.LeadingZeros32(1))
	fmt.Println(2, bits.LeadingZeros32(2))
	fmt.Println(3, bits.LeadingZeros32(3))
	fmt.Println(4, bits.LeadingZeros32(4))

	fmt.Println("TrailingZeros32 ---")

	// TrailingZeros32 returns the number of trailing zero bits in x; the result is 32 for x == 0.
	fmt.Println(1, bits.TrailingZeros32(1))
	fmt.Println(2, bits.TrailingZeros32(2))
	fmt.Println(3, bits.TrailingZeros32(3))
	fmt.Println(4, bits.TrailingZeros32(4))

	fmt.Println("Reverse32 ---")

	// Reverse32 returns the value of x with its bits in reversed order.
	fmt.Println(1, bits.Reverse32(1))
	fmt.Println(2, bits.Reverse32(2))
	fmt.Println(3, bits.Reverse32(3))
	fmt.Println(4, bits.Reverse32(4))
}
