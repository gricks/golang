package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"runtime/debug"

	_ "gitee.com/gricks/kira/pkg/misc"
)

func main() {
	info, _ := debug.ReadBuildInfo()
	b := bytes.NewBuffer(nil)
	encoder := json.NewEncoder(b)
	encoder.SetIndent("", "  ")
	encoder.Encode(info)
	fmt.Println(b.String())
}
