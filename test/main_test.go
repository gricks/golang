package main

import (
	"fmt"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("test recover") // not running
		}
	}()

	fmt.Println("start test main")
	code := m.Run()
	fmt.Println("end test main") // not running

	os.Exit(code)
}

func TestHelloWorld(t *testing.T) {
	panic("hello")
}
