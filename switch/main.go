package main

import "fmt"

func case1(flag string) {
	done := false
LABEL:
	for _, _ = fmt.Println("for init"); !done; {
		fmt.Println("begin")
		switch flag {
		case "continue":
			done = true
			continue
		case "break":
			done = false
			break LABEL
		case "goto":
			done = true
			goto LABEL
		}
		fmt.Println("end")
	}
	fmt.Println("done case 1")
}

func main() {
	case1("continue")
	// for init
	// begin
	// done case 1
	fmt.Println("------------------")
	case1("break")
	// for init
	// begin
	// done case 1
	fmt.Println("------------------")
	case1("goto")
	// for init
	// begin
	// for init
	// done case 1
}
