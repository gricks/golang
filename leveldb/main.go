package main

import (
	"fmt"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/filter"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"github.com/syndtr/goleveldb/leveldb/util"
)

func main() {
	store, err := leveldb.OpenFile("store.dat", &opt.Options{
		Filter: filter.NewBloomFilter(10),
	})
	if err != nil {
		panic(err)
	}
	defer store.Close()

	err = store.Put([]byte("key1"), []byte("val1"), nil)
	if err != nil {
		panic(err)
	}
	err = store.Put([]byte("key2"), []byte("val2"), nil)
	if err != nil {
		panic(err)
	}

	b, err := store.Get([]byte("key1"), nil)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(b))

	iter := store.NewIterator(util.BytesPrefix([]byte("key")), nil)
	for iter.Next() {
		fmt.Println(string(iter.Key()), string(iter.Value()))
	}
	iter.Release()
	err = iter.Error()
	if err != nil {
		panic(err)
	}

	batch := &leveldb.Batch{}
	batch.Put([]byte("key3"), []byte("val3"))
	batch.Put([]byte("key4"), []byte("val4"))
	err = store.Write(batch, nil)

	iter = store.NewIterator(util.BytesPrefix([]byte("key")), nil)
	for iter.Next() {
		fmt.Println(string(iter.Key()), string(iter.Value()))
	}
	iter.Release()
	err = iter.Error()
	if err != nil {
		panic(err)
	}
}
