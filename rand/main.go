package main

import (
	"fmt"
	"math/rand"
)

func main() {
	seed := int64(10)
	// global
	rand.Seed(seed)
	v1 := rand.Int()
	fmt.Println(v1)

	// standard normal distribution (mean = 0, stddev = 1)
	// NormFloat64() * stddev + mean
	fmt.Println(rand.NormFloat64())

	rander := rand.New(rand.NewSource(seed))
	v2 := rander.Int()
	fmt.Println(v2)
}
