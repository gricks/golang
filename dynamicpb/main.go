package main

import (
	"encoding/json"
	"fmt"
	"os"

	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/reflect/protodesc"
	"google.golang.org/protobuf/reflect/protoreflect"
	"google.golang.org/protobuf/reflect/protoregistry"
	"google.golang.org/protobuf/types/descriptorpb"
	"google.golang.org/protobuf/types/dynamicpb"
)

func main() {
	registry, err := LoadProtoset("shelf.protoset")
	if err != nil {
		panic(err)
	}

	message, err := Map2Proto(
		map[string]any{
			"name":        "Shelf A",
			"theme":       "classical",
			"create_time": "2022-01-01T00:00:00Z",
			"update_time": "2024-01-01T00:00:00Z",
		}, "golang.shelf.Shelf", registry)
	if err != nil {
		panic(err)
	}

	fmt.Println(message)
}

func LoadProtoset(path string) (*protoregistry.Files, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	var fdSet descriptorpb.FileDescriptorSet
	proto.Unmarshal(data, &fdSet)
	if err != nil {
		return nil, err
	}
	return protodesc.NewFiles(&fdSet)
}

func Map2Proto(data map[string]any, name string, registry *protoregistry.Files) (proto.Message, error) {
	desc, err := registry.FindDescriptorByName(
		protoreflect.FullName(name))
	if err != nil {
		return nil, fmt.Errorf(
			"failed to find descriptor for %s: %v", name, err)
	}
	b, err := json.Marshal(data)
	if err != nil {
		return nil, fmt.Errorf(
			"failed to marshal %s: %v", name, err)
	}
	mdesc := desc.(protoreflect.MessageDescriptor)
	msg := dynamicpb.NewMessage(mdesc)
	err = protojson.Unmarshal(b, msg)
	if err != nil {
		return nil, fmt.Errorf(
			"failed to unmarshal %s: %v", name, err)
	}
	return msg, nil
}
