module dynamicpb

go 1.23.3

require (
	github.com/google/gnostic v0.7.0
	gitlab.papegames.com/fringe/sparrow v1.8.19
	google.golang.org/genproto/googleapis/api v0.0.0-20250115164207-1a7da9e5054f
	google.golang.org/protobuf v1.36.3
)

require (
	github.com/google/gnostic-models v0.6.9-0.20230804172637-c7be7c783f49 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
