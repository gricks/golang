package main

import (
	"fmt"
	"time"
)

func main() {
	{
		flag := true
		ticker := time.NewTicker(100 * time.Millisecond)
		defer ticker.Stop()
		for i := 0; i < 10; i++ {
			start := time.Now()
			select {
			case <-ticker.C:
				if flag {
					time.Sleep(time.Second)
					flag = false
				}
				fmt.Println("Cost:", time.Since(start))
			}
		}
	}

	fmt.Println("next")

	{
		ticker := time.NewTicker(100 * time.Millisecond)
		defer ticker.Stop()
		for i := 0; i < 10; i++ {
			start := time.Now()
			select {
			case <-ticker.C:
				time.Sleep(50 * time.Millisecond)
				fmt.Println("Cost:", time.Since(start))
			}
		}
	}
}
