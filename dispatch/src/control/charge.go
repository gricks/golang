package control

import "fmt"

const (
	PRE_CHARGE_ITEM = 100003
	GET_CHARGE_ITEM = 100004
)

func init() {
	CommandMapping[PRE_CHARGE_ITEM] = NewChargeCommand
	CommandMapping[GET_CHARGE_ITEM] = NewChargeCommand
}

type ChargeCommand struct{}

func NewChargeCommand() ICommand { return &ChargeCommand{} }

func (self *ChargeCommand) Process(iCommand int, stParams ...interface{}) bool {
	switch iCommand {
	case PRE_CHARGE_ITEM:
		fmt.Println("Pre Charge Item Command")
	case GET_CHARGE_ITEM:
		fmt.Println("Get Charge Item Command")
	}
	return true
}
