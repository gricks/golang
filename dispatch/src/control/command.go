package control

import "fmt"

type ICommand interface {
	Process(iCommand int, stParams ...interface{}) bool
}

var CommandMapping = map[int]func() ICommand{}

type CommandBase struct {
	command   int
	commander ICommand
}

func NewCommandBase(iCommand int, iCommander ICommand) *CommandBase {
	return &CommandBase{command: iCommand, commander: iCommander}
}

func (self *CommandBase) Start() {
	fmt.Println("Process Start")
}

func (self *CommandBase) Finish() {
	fmt.Println("Process Finish")
}

func (self *CommandBase) Process() {
	self.Start()
	self.commander.Process(self.command)
	self.Finish()
}
