package control

import "fmt"

const (
	GET_USER_NAME = 100001
	SET_USER_NAME = 100002
)

func init() {
	CommandMapping[GET_USER_NAME] = NewNameCommand
	CommandMapping[SET_USER_NAME] = NewNameCommand
}

type NameCommand struct{}

func NewNameCommand() ICommand { return &NameCommand{} }

func (self *NameCommand) Process(iCommand int, stParams ...interface{}) bool {
	switch iCommand {
	case GET_USER_NAME:
		fmt.Println("Get User Name Command")
	case SET_USER_NAME:
		fmt.Println("Set User Name Command")
	}
	return true
}
