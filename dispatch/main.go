package main

import "golang/dispatch/src/control"

/*
This is a go style code, using combination
*/
func main() {
	array := []int{
		control.GET_USER_NAME,
		control.SET_USER_NAME,
		control.PRE_CHARGE_ITEM,
		control.GET_CHARGE_ITEM,
	}

	for _, command := range array {
		if creator, ok := control.CommandMapping[command]; ok {
			commander := control.NewCommandBase(command, creator())
			commander.Process()
		}
	}
}
