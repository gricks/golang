package main

import (
	"crypto/tls"
	"fmt"
	"log"
)

func client() {
	conf := &tls.Config{
		InsecureSkipVerify: true,
	}
	conn, err := tls.Dial("tcp", "127.0.0.1:443", conf)
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()

	msg, buf := "", make([]byte, 100)
	for {
		n, _ := fmt.Scanln(&msg)
		n, err := conn.Write([]byte(msg + "\n"))
		if err != nil {
			log.Println(n, err)
			return
		}
		log.Print("send: ", msg)
		n, err = conn.Read(buf[:])
		if err != nil {
			log.Println(n, err)
			return
		}
		log.Println("recv:", string(buf[:n]))
	}
}
