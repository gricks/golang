#!/bin/bash

openssl genrsa -out ./cert/cert.key 2048
openssl req -new -x509 -key ./cert/cert.key -out ./cert/cert.pem -days 3650
