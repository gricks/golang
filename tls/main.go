package main

import "flag"

var (
	s = flag.Bool("s", false, "-s")
	c = flag.Bool("c", false, "-c")
)

func init() {
	flag.Parse()
}

func main() {
	switch {
	case *s:
		server()
	case *c:
		client()
	default:
		panic("invalid flag")
	}
}
