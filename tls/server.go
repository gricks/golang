package main

import (
	"bufio"
	"crypto/tls"
	"log"
	"net"
)

func server() {
	cert, err := tls.LoadX509KeyPair(
		"./cert/cert.pem",
		"./cert/cert.key")
	if err != nil {
		log.Println(err)
		return
	}
	config := &tls.Config{
		Certificates: []tls.Certificate{cert},
	}
	ln, err := tls.Listen("tcp", ":443", config)
	if err != nil {
		log.Println(err)
		return
	}
	defer ln.Close()
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Println(err)
			continue
		}
		go handleConn(conn)
	}
}

func handleConn(conn net.Conn) {
	defer conn.Close()
	r := bufio.NewReader(conn)
	for {
		msg, err := r.ReadString('\n')
		if err != nil {
			log.Println(err)
			return
		}
		log.Print("recv: ", msg)
		n, err := conn.Write([]byte("aha\n"))
		if err != nil {
			log.Println(n, err)
			return
		}
		log.Println("send:", "aha")
	}
}
