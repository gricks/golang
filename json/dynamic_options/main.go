package main

import (
	"fmt"

	"github.com/go-json-experiment/json"
	"github.com/go-json-experiment/json/jsontext"
)

type Lang string

type Content struct {
	Text map[string]string
}

func (c Content) MarshalJSONTo(enc *jsontext.Encoder, opts json.Options) error {
	lang, _ := json.GetOption(opts,
		json.WithOption[Lang])
	switch string(lang) {
	case "*":
		return json.MarshalEncode(enc, c.Text, opts)
	default:
		return json.MarshalEncode(enc,
			map[string]string{
				string(lang): c.Text[string(lang)],
			}, opts)
	}
}

func main() {
	c := Content{
		Text: map[string]string{
			"en-US": "hello",
			"de-DE": "hallo",
		},
	}

	text1, err := json.Marshal(&c,
		json.WithOption(Lang("*")))
	if err != nil {
		panic(err)
	}
	fmt.Println(string(text1))

	text2, err := json.Marshal(&c,
		json.WithOption(Lang("en-US")))
	if err != nil {
		panic(err)
	}
	fmt.Println(string(text2))

	text3, err := json.Marshal(&c,
		json.WithOption(Lang("de-DE")))
	if err != nil {
		panic(err)
	}
	fmt.Println(string(text3))
}
