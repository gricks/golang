package main

import (
	stdjson "encoding/json"
	"strconv"
	"testing"

	"github.com/bytedance/sonic"
	"github.com/bytedance/sonic/ast"
	"github.com/goccy/go-json"
)

func BenchmarkPair(b *testing.B) {
	vv := []int{10, 50, 100}
	for _, v := range vv {
		b.Run("ast_sonic_"+strconv.Itoa(v), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				pairs := []ast.Pair{}
				for i := 0; i < v; i++ {
					pairs = append(pairs,
						ast.Pair{"id" + strconv.Itoa(i), ast.NewNumber(strconv.Itoa(i))})
				}
				obj := ast.NewObject(pairs)
				_, _ = sonic.Marshal(&obj)
			}
		})

		b.Run("ast_stdjson_"+strconv.Itoa(v), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				pairs := []ast.Pair{}
				for i := 0; i < v; i++ {
					pairs = append(pairs,
						ast.Pair{"id" + strconv.Itoa(i), ast.NewNumber(strconv.Itoa(i))})
				}
				obj := ast.NewObject(pairs)
				_, _ = stdjson.Marshal(&obj)
			}
		})

		b.Run("ast_gojson_"+strconv.Itoa(v), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				pairs := []ast.Pair{}
				for i := 0; i < v; i++ {
					pairs = append(pairs,
						ast.Pair{"id" + strconv.Itoa(i), ast.NewNumber(strconv.Itoa(i))})
				}
				obj := ast.NewObject(pairs)
				_, _ = json.Marshal(&obj)
			}
		})

		b.Run("map_sonic_"+strconv.Itoa(v), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				anys := make(map[string]any)
				for i := 0; i < v; i++ {
					anys["id"+strconv.Itoa(i)] = i
				}
				_, _ = sonic.Marshal(anys)
			}
		})

		b.Run("map_stdjson_"+strconv.Itoa(v), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				anys := make(map[string]any)
				for i := 0; i < v; i++ {
					anys["id"+strconv.Itoa(i)] = i
				}
				_, _ = stdjson.Marshal(anys)
			}
		})

		b.Run("map_gojson_"+strconv.Itoa(v), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				anys := make(map[string]any)
				for i := 0; i < v; i++ {
					anys["id"+strconv.Itoa(i)] = i
				}
				_, _ = json.Marshal(anys)
			}
		})
	}
}
