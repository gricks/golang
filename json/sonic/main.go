package main

import (
	"fmt"

	"github.com/bytedance/sonic"
	"github.com/bytedance/sonic/ast"
)

func main() {
	pairs := []ast.Pair{}
	pairs = append(pairs, ast.Pair{"id", ast.NewNumber("1")})
	obj := ast.NewObject(pairs)
	b, err := sonic.Marshal(&obj)
	fmt.Println("str", string(b), err)

	s := ast.NewSearcher(string(b))
	node, err := s.GetByPath()
	if err != nil {
		panic(err)
	}
	_, err = node.Set("id", ast.NewNumber("100"))
	if err != nil {
		panic(err)
	}
	str, err := node.Raw()
	if err != nil {
		panic(err)
	}
	fmt.Println("str", str, err)
}
