package main

import (
	"encoding/json"
	"fmt"
)

type Outer struct {
	Inner
	S string `json:"s"`
}

type Inner struct {
	I int `json:"i"`
}

func main() {
	v1 := Outer{
		Inner: Inner{
			I: 1,
		},
		S: "a",
	}

	b, err := json.Marshal(v1)
	if err != nil {
		panic(err)
	}

	var v2 Outer
	err = json.Unmarshal(b, &v2)
	if err != nil {
		panic(err)
	}

	fmt.Println("b:", string(b))
	fmt.Printf("v1:%+v\n", v1)
	fmt.Printf("v2:%+v\n", v2)
}
