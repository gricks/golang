package main

import "time"

type JsonTime time.Time

func (this JsonTime) MarshalJSON() ([]byte, error) {
	data := make([]byte, 0, 24)
	data = append(data, '"')
	data = time.Time(this).AppendFormat(data, "2006-01-02 15:04:05")
	data = append(data, '"')
	return data, nil
}

func (this *JsonTime) UnmarshalJSON(b []byte) error {
	t, err := time.Parse(`"2006-01-02 15:04:05"`, string(b))
	*this = JsonTime(t)
	return err
}

func (this JsonTime) String() string {
	return time.Time(this).Format("2006-01-02 15:04:05")
}
