package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
)

func main() {
	// compact-multiple-json
	b := bytes.NewBuffer(nil)
	for i := 0; i < 3; i++ {
		json.Compact(b,
			[]byte(fmt.Sprintf(` { "A" : "%d" } `, i)))
	}
	fmt.Println(b.String())

	// parsing-multiple-json
	decoder := json.NewDecoder(b)
	for {
		var v any
		err := decoder.Decode(&v)
		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(v)
	}
}
