package main

import (
	"fmt"
	"net"
)

func main() {
	network := "192.168.5.0/24"
	addrs := []string{
		"192.168.5.1",
		"192.168.6.1",
	}

	_, subnet, err := net.ParseCIDR(network)
	if err != nil {
		panic(err)
	}

	for _, addr := range addrs {
		if subnet.Contains(net.ParseIP(addr)) {
			fmt.Println("addr:", addr, "in subnet")
		}
	}
}
