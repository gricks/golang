package main_test

import (
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"testing"
)

func BenchmarkHexBase64(b *testing.B) {
	hash := sha256.New()
	text := hash.Sum(nil)
	hexs := hex.EncodeToString(text)
	base := base64.StdEncoding.EncodeToString(text)

	b.Run("hex-encode", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			hex.EncodeToString(text)
		}
	})

	b.Run("hex-decode", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			hex.DecodeString(hexs)
		}
	})

	b.Run("base64-encode", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			base64.StdEncoding.EncodeToString(text)
		}
	})

	b.Run("base64-decode", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			base64.StdEncoding.DecodeString(base)
		}
	})
}
