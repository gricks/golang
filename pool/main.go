package main

import (
	"fmt"
	"runtime"
	"runtime/debug"
	"sync"
)

type User struct {
	Name string
}

func main() {
	// close GC
	debug.SetGCPercent(-1)

	var u1, u2, u3 *User
	sp := &sync.Pool{New: func() interface{} { return &User{} }}
	u1 = sp.Get().(*User)
	u1.Name = "Jam"
	u2 = sp.Get().(*User)
	u2.Name = "Ray"
	u3 = sp.Get().(*User)
	u3.Name = "Tom"

	fmt.Println(u1, u2, u3)

	sp.Put(u1)
	sp.Put(u2)
	//	sp.Put(u3)
	u1, u2, u3 = nil, nil, nil
	u1 = sp.Get().(*User)
	u2 = sp.Get().(*User)
	u3 = sp.Get().(*User)
	fmt.Println(u1, u2, u3)

	sp.Put(u1)
	u1, u2, u3 = nil, nil, nil

	// run GC
	runtime.GC()

	u1 = sp.Get().(*User)
	u2 = sp.Get().(*User)
	u3 = sp.Get().(*User)
	fmt.Println(u1, u2, u3)
}
