package main

import (
	"fmt"
	"reflect"
	"unsafe"
)

var (
	_ reflect.SliceHeader
	_ reflect.StringHeader
)

func main() {
	i := 100
	fmt.Println(i)
	p := (*int)(unsafe.Pointer(&i))
	fmt.Println(*p)
	*p = 0
	fmt.Println(i)
	fmt.Println(*p)

	fmt.Println("point:", unsafe.Sizeof(p))
	fmt.Println("int:", unsafe.Sizeof((int)(0)))
	fmt.Println("int32:", unsafe.Sizeof((int32)(0)))
	fmt.Println("int64:", unsafe.Sizeof((int64)(0)))
	fmt.Println("string:", unsafe.Sizeof(""))
	fmt.Println("slice:", unsafe.Sizeof([]int{1}))
}
