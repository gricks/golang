# Benchstat
```
go get golang.org/x/perf/cmd/benchstat
```

# Use case
```
go test -c -o pad.golden
go test -c -o pad.optimize
./pad.golden -test.bench=.* -test.count=10 > old.txt
./pad.optimize -test.bench=.* -test.count=10 > new.txt
benchstat old.txt new.txt
```
