package main_test

import (
	"reflect"
	"runtime"
	"strconv"
	"testing"
)

//go:noinline
func Funcs() {}

var maps = map[uintptr]string{}

func BenchmarkT(b *testing.B) {
	f := Funcs
	for i := 0; i < 100; i++ {
		maps[uintptr(i)] = strconv.Itoa(i)
	}
	maps[reflect.ValueOf(f).Pointer()] = "Funcs"
	b.Log("FuncForPC:", runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name())
	b.Log("maps:", maps[reflect.ValueOf(f).Pointer()])

	b.Run("FuncForPC", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_ = runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name()
		}
	})

	b.Run("maps", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_ = maps[reflect.ValueOf(f).Pointer()]
		}
	})
}
