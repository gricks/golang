# API 签名认证

## 签名生成

API 调用方需要在调用 API 之前获取已经授权的 API 签名秘钥对 `APP Key` `APP Secret`

### 原始请求

```
POST /v1/http2test?param1=value1 HTTP/1.1
Host: graph.sparrow.com
Accept: application/json; charset=utf-8
Content-Type: application/x-www-form-urlencoded; charset=utf-8
Content-Length: 33
username=sparrow&password=1234567
```

### 提取内容

```
X-Ca-Key: 1001
X-Ca-Timestamp: 1639632143
X-Ca-Nonce: c9f15cbf-f4ac-4a6c-b54d-f51abf4b5b44
Path: /v1/http2test?param1=value1
BodyDigst: 2itXCHRQ5in7PrlGp6s/8wH7bevYjF+TvgdTZfY/ye8=
```

> 以上5个字段构成整个签名串, 字段之间使用`|`连接. 当 Body 为空时使用空字符串作为哈希函数的输入.

* X-Ca-Key: APP Key.
* X-Ca-Timestamp: 发送请求时间戳.
* X-Ca-Nonce: 短时间内的唯一ID, 可以使用 UUID 或者时间戳加随机数的方式生成.
* Path: HTTP 协议请求的 path.
* BodyDigst: HTTP 协议请求的 body 使用 Sha256 哈希函数的输入创建的 base64 字符串.

strToSign = "1001|1639632143|c9f15cbf-f4ac-4a6c-b54d-f51abf4b5b44|/v1/http2test?param1=value1|2itXCHRQ5in7PrlGp6s/8wH7bevYjF+TvgdTZfY/ye8="

使用 `HMACSha256` 生成最终签名 `X-Ca-Signature`

### 签名请求

假设 APP Key:`1001` APP Secret:`2xEJgpPsq0` 则对应生成的签名请求如下.

```
POST /v1/http2test?param1=value1 HTTP/1.1
Host: graph.sparrow.com
Accept: application/json; charset=utf-8
Content-Type: application/x-www-form-urlencoded; charset=utf-8
Content-Length: 33
X-Ca-Key: 1001
X-Ca-Signature: EAXC1iB3yrDrt8vW5QXGxsS2NBmuBSl2VPep8jOZM/E=
X-Ca-Timestamp: 1639632143
X-Ca-Nonce: c9f15cbf-f4ac-4a6c-b54d-f51abf4b5b44
username=sparrow&password=1234567
```
