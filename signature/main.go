package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"
)

var (
	appKey    = "1001"
	appSecret = "2xEJgpPsq0"
)

func main() {
	server()

	param := url.Values{}
	param.Set("param1", "value1")

	url := "http://127.0.0.1:8899"
	path := "/v1/http2test?" + param.Encode()
	body := "username=sparrow&password=1234567"

	hash := sha256.New()
	hash.Write([]byte(body))
	digest := Digest(hash.Sum(nil))

	nonce := "c9f15cbf-f4ac-4a6c-b54d-f51abf4b5b44"
	timestamp := "1639632143"

	strToSign := strings.Join([]string{
		appKey,
		timestamp,
		nonce,
		path,
		digest,
	}, "|")

	fmt.Println("strToSign:", strToSign)

	signature := HMACSha256([]byte(appSecret), []byte(strToSign))

	request, err := http.NewRequest("POST", url+path, strings.NewReader(body))
	if err != nil {
		panic(err)
	}

	request.Header.Set("X-Ca-Key", appKey)
	request.Header.Set("X-Ca-Timestamp", "1639632143")
	request.Header.Set("X-Ca-Nonce", "c9f15cbf-f4ac-4a6c-b54d-f51abf4b5b44")
	request.Header.Set("X-Ca-Signature", signature)

	fmt.Println("request")
	fmt.Println("X-Ca-Key", appKey)
	fmt.Println("X-Ca-Timestamp", "1639632143")
	fmt.Println("X-Ca-Nonce", "c9f15cbf-f4ac-4a6c-b54d-f51abf4b5b44")
	fmt.Println("X-Ca-Signature", signature)
	fmt.Println("path", path)
	fmt.Println("body", body)
	fmt.Println("digest", digest)

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		panic(err)
	}

	if response.StatusCode != http.StatusOK {
		panic(response.StatusCode)
	}
}

func server() {
	http.HandleFunc("/v1/http2test", func(w http.ResponseWriter, r *http.Request) {
		path := r.URL.RequestURI()
		body, err := io.ReadAll(r.Body)
		if err != nil {
			panic(err)
		}

		hash := sha256.New()
		hash.Write(body)
		digest := Digest(hash.Sum(nil))

		strToSign := strings.Join([]string{
			appKey,
			r.Header.Get("X-Ca-Timestamp"),
			r.Header.Get("X-Ca-Nonce"),
			path,
			digest,
		}, "|")

		fmt.Println("\nresponse")

		signature := HMACSha256([]byte(appSecret), []byte(strToSign))
		fmt.Println("signature", signature)
		fmt.Println("X-Ca-Signature", r.Header.Get("X-Ca-Signature"))
		if signature != r.Header.Get("X-Ca-Signature") {
			w.WriteHeader(http.StatusForbidden)
		} else {
			w.WriteHeader(http.StatusOK)
		}
	})

	go http.ListenAndServe(":8899", nil)
	time.Sleep(time.Second)
}

func Digest(body []byte) string {
	return base64.StdEncoding.EncodeToString(body)
}

func HMACSha256(k, b []byte) string {
	h := hmac.New(sha256.New, k)
	h.Write(b)
	return Digest(h.Sum(nil))
}
