package main

import (
	"fmt"

	"github.com/buger/jsonparser"
)

var data = []byte(`
{
  "person": {
    "name": {
      "first": "Leonid",
      "last": "Bugaev",
      "fullName": "Leonid Bugaev"
    },
    "github": {
      "handle": "buger",
      "followers": 109
    },
    "avatars": [
      { "url": "https://avatars.com/u/14009?v=3&s=460", "value": "thumbnail1" },
      { "url": "https://avatars.com/u/14009?v=3&s=460", "value": "thumbnail2" }
    ]
  },
  "company": {
    "name": "Acme"
  }
}`)

func main() {
	val1, typ1, _, err := jsonparser.Get(data, "person", "avatars", "[0]", "value")
	if err != nil {
		panic(err)
	}
	fmt.Println("val:", string(val1))
	fmt.Println("typ:", typ1)

	val2, typ2, _, err := jsonparser.Get(data, "person", "avatars", "[1]", "value")
	if err != nil {
		panic(err)
	}
	fmt.Println("val:", string(val2))
	fmt.Println("typ:", typ2)

	val3, typ3, _, err := jsonparser.Get(data, "person", "name")
	if err != nil {
		panic(err)
	}
	fmt.Println("val:", string(val3))
	fmt.Println("typ:", typ3)

	// not exist
	_, _, _, err = jsonparser.Get(data, "person", "avatars", "[3]", "value")
	fmt.Println("err:", err)
	_, _, _, err = jsonparser.Get(data, "person_", "avatars", "[3]", "value")
	fmt.Println("err:", err)
}
