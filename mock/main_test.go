package main

import (
	"testing"

	"golang/mock/mock"

	"github.com/golang/mock/gomock"
)

func TestMain(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock.NewMockFoo(ctrl)
	m.EXPECT().Bar(gomock.Any()).Return(101)
	if m.Bar(10) != 101 {
		t.Fatal("Unexpected")
	}

	// m.Bar(10)
	// has already been called the max number of times

	m.EXPECT().Bar(gomock.Eq(11)).Return(101).MinTimes(1).MaxTimes(2)
	if m.Bar(11) != 101 {
		t.Fatal("Unexpected")
	}
	m.Bar(11)
	// m.Bar(11)
	// has already been called the max number of times
	// call AnyTimes
}
