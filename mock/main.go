package main

//go:generate mockgen -source=main.go -destination=./mock/main_mock.go -package=mock
type Foo interface {
	Bar(x int) int
}
